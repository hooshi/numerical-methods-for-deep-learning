close all
clear all

nns = load('history-nnsignle-no-u.txt','-ascii');
nnd = load('history-nndeep-no-u.txt','-ascii');

hold off, l1=semilogy(nns(:,1), nns(:,2), 'color', 'blue', 'linewidth', 2)
hold on , l2=semilogy(nnd(:,1), nnd(:,2), 'color', 'red', 'linewidth', 2)
xlim([0, 20000])
legend('single layer', 'deep')
xlabel('Iteration')
ylabel('loss')

set(gcf, 'Position', [0 0 400 200]);
saveas(gcf, 'history-no-u.svg');
