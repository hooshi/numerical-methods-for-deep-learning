classdef Pendulum
    % Pendulum
    properties
        n_links
        M
        I
        L
        Lt
        g
        c  % damping
    end
    
    methods
        function this = Pendulum(n_links, M, I, L, Lt, g, c)
            % function this = Pendulum(n_links, M, I, L, Lt)
            
            this.n_links = n_links;
            this.M = M;
            this.I = I;
            this.L = L;
            this.Lt = Lt;
            this.g = g;
            this.c = c;
            this.L(n_links+1:3) = 0;
        end
        
        function draw(this, p, origin, varargin)
            % function draw(p)
            if isempty(origin) 
                origin = [0 0];
            end
            
            l1 = this.L(1);
            l2 = this.L(2);
            l3 = this.L(3);
            
            p1 = get_entry(p, 1);
            p2 = get_entry(p, 2);
            p3 = get_entry(p, 3);
            
            delta1 = l1 * [sin(p1) cos(p1)];
            delta2 = l2 * [sin(p2+p1) cos(p2+p1)];
            delta3 = l3 * [sin(p3+p2+p1) cos(p3+p2+p1)];
            
            x0 = origin;
            x1 = x0 + delta1;
            x2 = x1 + delta2;
            x3 = x2 + delta3;
            
            xy = [x0(:) x1(:) x2(:) x3(:)];
            plot( xy(1,1:this.n_links+1), xy(2,1:this.n_links+1), '-o' , 'linewidth', 3, 'markersize', 8,  varargin{:});
            set(gca,'Ydir','reverse');
            L = l1+l2+l3;
            ylim([-L  L])
            xlim([-L L])
            daspect([1 1 1])
        end
        
            function ans = n_p(this)
                 ans = this.n_links;
            end

            function  ans = n_q(this)
                 ans = this.n_links;
            end
            
            function ans = n_dof(this)
                ans = this.n_p() + this.n_q();
            end
    end
    
end

function [out] = get_entry(vv, ii)

if (numel(vv) >= ii) out = vv(ii);
else                 out = 0;
end
end
