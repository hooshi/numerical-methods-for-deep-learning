close all
clear all

%load('/home/hooshi/control-time.mat'), t=t;
%load('/home/hooshi/control-u.mat'), Ysingle = Ynn;
%load('/home/hooshi/control-deep-u.mat'), Ydeep = Ynn;
%load('/home/hooshi/tau-deep.mat'), Udeep = Tau; % 
%load('/home/hooshi/tau.mat'), Usingle = xtau; 
%load('control-hard.mat')
%save('control-hard.mat', 'Usingle', 'Ydeep', 'Udeep', 'Ysingle', 't')
%return;

MODE = 3
%load('control-simple.mat')
load('control-hard.mat')

if(MODE == 1)
    fig = figure(1);
    clf
    k=1;
    filename = 'control-simple.gif';
    zz  = zeros(1,3);
    pdlm = Pendulum(3,zz,zz,[1,1,1],zz,zz,10);
    for k=1:numel(t)
        prange = 1:(pdlm.n_p());
        qrange  = (pdlm.n_p()+1):(pdlm.n_dof());
        p = Ysingle( k , 1:(pdlm.n_links) );
        hold off, pdlm.draw(p(:), [0 0], 'color', 'blue');
        %
        p = Ydeep( k , 1:(pdlm.n_links) );
        hold on, pdlm.draw(p(:), [0 0], 'color', 'red');
        %
        p = [-pi/2 -pi/4 -pi/4];
        hold on, pdlm.draw(p(:), [0 0],  'color', [0, 0.4470, 0.7410], ...
                           'linestyle', '-.');
        title( sprintf('t = %f ',  t(k)) );
        legend('single','deep','target')
        % ================== 
        f = getframe(fig);
        [im, cm] = rgb2ind(frame2im(f),256);
        if k == 1
            imwrite(im,cm,filename,'gif', 'DelayTime', 0,'Loopcount',inf);
        else
            imwrite(im,cm,filename,'gif','WriteMode','append', 'DelayTime', 0);
        end
    end     
elseif(MODE==2)
    
    fig = figure(1);
    clf
    k=1;
    zz  = zeros(1,3);
    %pdlm = Pendulum(3,zz,zz,[1,1,1],zz,zz,10);
    pdlm = Pendulum(3,zz,zz,[1,1.5,2],zz,zz,10);
    for k=[1, 20, 40, 60, 80, 100, 120, 140, size(Ydeep,1)]
        prange = 1:(pdlm.n_p());
        qrange  = (pdlm.n_p()+1):(pdlm.n_dof());
        p = Ysingle( k , 1:(pdlm.n_links) );
        hold off, pdlm.draw(p(:), [0 0], 'color', 'blue');
        %
        p = Ydeep( k , 1:(pdlm.n_links) );
        hold on, pdlm.draw(p(:), [0 0], 'color', 'red');
        %
        p = [-pi/2 -pi/4 -pi/4];
        hold on, pdlm.draw(p(:), [0 0],  'color', [0, 0.4470, 0.7410], ...
                           'linestyle', '-.');
        title( sprintf('t = %f ',  t(k)) );
        legend('single','deep','target')
        % saveas(fig, sprintf('control-simple-%d.svg',k))
        saveas(fig, sprintf('control-hard-%d.svg',k))

    end     

else
    clf;
    for i=1:3
        subplot(2,3,i),  hold on;
        plot(t(2:end), Ysingle(:,i),'color', 'blue', 'linestyle', '-', 'linewidth', 0.75)
        plot(t(2:end), Ydeep(:,i),'color', 'red', 'linestyle', '-', 'linewidth', 0.75)
        % xlim([0, 30])
        loc = [-pi/2 -pi/4 -pi/4];
        plot(t(end) , loc(i), 'ko', 'markersize', 3, 'linewidth',  3)
    end
    subplot(2,3,1), ylabel('\theta_1')
    subplot(2,3,2), ylabel('\theta_2')  
    subplot(2,3,3), ylabel('\theta_3') 

    for i=1:3
        subplot(2,3,3+i),  hold on;
        plot(t(2:end-1), Udeep(i:3:end), 'color', 'red', 'linestyle', '-', 'linewidth', 0.75)
        plot(t(2:end-1), Usingle(i:3:end), 'color', 'blue', ...
             'linestyle', '-', 'linewidth', 0.75)
     end

    subplot(2,3,4), ylabel('\tau_1'), xlabel('time')
    subplot(2,3,5), ylabel('\tau_2'), xlabel('time') 
    subplot(2,3,6), ylabel('\tau_3'), xlabel('time')

    set(gcf, 'Position', [0 0 900 200]);
    saveas(gcf, 'control-hard-graphs.svg');
end
