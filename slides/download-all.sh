#!/bin/bash

wget "https://sites.google.com/site/ehaberubc/home/teaching/computational-data-science-1/01-NumDNN-Introduction.pdf"

wget "https://sites.google.com/site/ehaberubc/home/teaching/computational-data-science-1/02-NumDNN-LinearModels.pdf"

wget "https://sites.google.com/site/ehaberubc/home/teaching/computational-data-science-1/03-NumDNN-Regularization.pdf"

wget "https://sites.google.com/site/ehaberubc/home/teaching/computational-data-science-1/04-NumDNN-Classification.pdf"

wget "https://sites.google.com/site/ehaberubc/home/teaching/computational-data-science-1/05-NumDNN-Newton.pdf"

wget "https://sites.google.com/site/ehaberubc/home/teaching/computational-data-science-1/07-NumDNN-RegImageClass.pdf"

wget "https://sites.google.com/site/ehaberubc/home/teaching/computational-data-science-1/08-NumDNN-OneLayerNN.pdf"

wget "https://sites.google.com/site/ehaberubc/home/teaching/computational-data-science-1/09-NumDNN-IntroNonlinearModel.pdf"

wget "https://sites.google.com/site/ehaberubc/home/teaching/computational-data-science-1/09-NumDNN-IntroParametric.pdf"

wget "https://sites.google.com/site/ehaberubc/home/teaching/computational-data-science-1/10-NumDNN-IntroDeepLearning.pdf"

wget "https://sites.google.com/site/ehaberubc/home/teaching/computational-data-science-1/10a-NumDNN-ImageSegmentation.pdf"
