close all
clear all

load('/home/hooshi/control-time.mat'), t=t;
load('/home/hooshi/control-u.mat'), Ysingle = Ynn;
load('/home/hooshi/control-deep-u.mat'), Ydeep = Ynn;
save('control-simple.mat', 'Ydeep', 'Ysingle', 't')

load('control-simple.mat')

fig = figure(1);
clf
k=1;
filename = 'control-simple.gif';
zz  = zeros(1,3);
pdlm = Pendulum(3,zz,zz,[1,1,1],zz,zz,10);
for k=1:numel(t)
  prange = 1:(pdlm.n_p());
  qrange  = (pdlm.n_p()+1):(pdlm.n_dof());
  p = Ysingle( k , 1:(pdlm.n_links) );
  hold off, pdlm.draw(p(:), [0 0], 'color', 'blue');
				%
  p = Ydeep( k , 1:(pdlm.n_links) );
  hold on, pdlm.draw(p(:), [0 0], 'color', 'red');
				%
  p = [-pi/2 -pi/4 -pi/4];
  hold on, pdlm.draw(p(:), [0 0],  'color', [0, 0.4470, 0.7410], ...
		     'linestyle', '-.');
  title( sprintf('t = %f ',  t(k)) );
  legend('single','deep','target')
				% ================== 
  f = getframe(fig);
  [im, cm] = rgb2ind(frame2im(f),256);
  if k == 1
    imwrite(im,cm,filename,'gif', 'DelayTime', 0,'Loopcount',inf);
  else
    imwrite(im,cm,filename,'gif','WriteMode','append', 'DelayTime', 0);
  end
end     
