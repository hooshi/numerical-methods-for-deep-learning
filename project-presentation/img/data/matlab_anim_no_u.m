close all
clear all

% load('/home/hooshi/anim-no-u-nndeep.mat'), Ydeep = Ynn; 
% load('/home/hooshi/anim-no-u-nnsingle.mat'), Ysingle = Ynn;
% load('/home/hooshi/anim-no-u-ode.mat'), Ye = Yode;
% load('/home/hooshi/anim-no-u-ode2.mat'), Ye2 = Yode;
% load('/home/hooshi/anim-no-u-time.mat'), t = t;
% save('anim-no-u.mat', 'Ydeep', 'Ysingle', 'Ye', 't')
% norm(Ye - Ye2)

load('anim-no-u.mat')

PLOT1=0
PLOT2=1

if(PLOT1)
    clf;
    for i=1:3
        subplot(3,1,i),  hold on;
        plot(t, Ye(:,i),'color', 'black', 'linestyle', '-', 'linewidth', 1)
        plot(t(2:end), Ysingle(:,i),'color', 'blue', 'linestyle', '-', 'linewidth', 0.75)
        plot(t(2:end), Ydeep(:,i),'color', 'red', 'linestyle', '-', 'linewidth', 0.75)
        xlim([0, 30])
    end
    subplot(3,1,3), xlabel('time')
    subplot(3,1,1), ylabel('\theta_1')
    legend('rk','single','deep')
    subplot(3,1,2), ylabel('\theta_2')
    subplot(3,1,3), ylabel('\theta_3')


    set(gcf, 'Position', [0 0 200 400]);
    saveas(gcf, 'anim-no-u.svg');
end
if(PLOT2)
    fig = figure(1);
    clf
    k=1;
    filename = 'anim-no-u.gif';
    zz  = zeros(1,3);
    pdlm = Pendulum(3,zz,zz,[1,1,1],zz,zz,10);
    for k=1:numel(t)
        prange = 1:(pdlm.n_p());
        qrange  = (pdlm.n_p()+1):(pdlm.n_dof());
        p = Ye( k , 1:(pdlm.n_links) );
        hold off, pdlm.draw(p(:), [0 0], 'color', 'black');
        %
        p = Ysingle( k , 1:(pdlm.n_links) );
        hold on, pdlm.draw(p(:), [0 0], 'color', 'blue');
        %
        p = Ydeep( k , 1:(pdlm.n_links) );
        hold on, pdlm.draw(p(:), [0 0], 'color', 'red');
        %
        title( sprintf('t = %f ',  t(k)) );
        legend('rk','single','deep')
        % ================== 
        f = getframe(fig);
        [im, cm] = rgb2ind(frame2im(f),256);
        if k == 1
            imwrite(im,cm,filename,'gif', 'DelayTime', 0,'Loopcount',inf);
        else
            imwrite(im,cm,filename,'gif','WriteMode','append', 'DelayTime', 0);
        end
        if(k==20)
            saveas(gcf, 'demo-no-u.svg');
        end
    end     
end