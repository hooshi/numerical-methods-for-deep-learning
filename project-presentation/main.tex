% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice. 

\documentclass[xcolor={usenames,dvipsnames}]{beamer}
\beamertemplatenavigationsymbolsempty

% path to graphics

% write code
\usepackage{listings}
\usepackage{caption}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{bm}

% links on images
\usepackage{graphicx}
\usepackage{hyperref}


\definecolor{mblue}{RGB}{0, 0, 125}
\definecolor{mgreen}{RGB}{0, 100, 0}
\definecolor{mred}{RGB}{200, 0, 0}
\definecolor{morange}{RGB}{217,83,25}
\definecolor{mlightblue}{RGB}{0, 114, 189}
\definecolor{mdarkgreen}{RGB}{0,98,7}

% THEME
\usetheme{Frankfurt}
\usecolortheme{seagull}

%% Show the page number
\setbeamertemplate{footline}[frame number]
\setbeamercolor{footline}{fg=black}
%% No header
\addtobeamertemplate{frametitle}{\vspace*{-0.9\baselineskip}}{}

\title{Physics Integration via Regression}

% A subtitle is optional and this may be deleted
%\subtitle{}

\author{Shayan Hoshyari, Chenxi Liu}

\institute[University of British Columbia]{}

\date{April, 2018}

% subject for pdf file
\subject{EOSC550}

% add some logo and stuff
% \pgfdeclareimage[height=1cm]{remesherlogo}{../image/logo.jpg}
% \logo{\pgfuseimage{remesherlogo}}

\newcommand{\vvec}[1]{\boldsymbol{\mathbf{#1}}}
\newcommand{\mmat}[1]{\bm{#1}}

% Let's get started
\begin{document}

% ****************************************************************************
% *                             Title page
% ****************************************************************************
\begin{frame}[plain]
  \titlepage
\end{frame}


% ****************************************************************************
% *                         Introduce pendulum
% ****************************************************************************
\begin{frame}{Motivation} 
  Physics simulations can sometimes be cast as regression problems.
  \begin{itemize}
  \item<2-> Speed.    
  \item<3-> Only data is available.   
  \end{itemize}

  \vspace{1cm}
  \begin{overlayarea}{\linewidth}{3cm}
    \only<2>{
      \centering
      \includegraphics[width=\linewidth]{img/fluid-sim.png} \\
      \scriptsize  [Ladicky, et. al. SIGGRAPH Asia 2015] }
    \only<3>{
      \centering
      \includegraphics[width=\linewidth]{img/edinburgh.png} \\
      \scriptsize  [Holden, et. al. SIGGRAPH 2017] }
  \end{overlayarea}
\end{frame}

% ****************************************************************************
% *                         Introduce pendulum
% ****************************************************************************
\begin{frame}{The problem}
  \begin{itemize}
  \item<1-> Dynamical system,
    $\dot{\vvec{s}} = \vvec{f}(t,\vvec{s},\vvec{u})$
  \item<1->[] $\vvec{s}$: state variables
  \item<1->[] $\vvec{u}$: control inputs
  \item<2-> Example, three link pendulum:
  \end{itemize}

  \begin{columns}
    \begin{column}{0.48\textwidth}
      \centering
      \includegraphics<2>[width=0.8\linewidth]{img/pendulum-mass.pdf}
      \includegraphics<3>[width=0.8\linewidth]{img/pendulum-dof.pdf}
      \includegraphics<4>[width=0.8\linewidth]{img/pendulum-control.pdf}
      \includegraphics<5>[width=0.8\linewidth]{img/pendulum-others.pdf}
    \end{column}
    \begin{column}{0.48\textwidth}
      \begin{itemize}
      \item<3->[] $\vvec{s}=[\theta_1,\theta_2,\theta_3,\dot\theta_1,\dot\theta_2,\dot\theta_3]$
      \item<4->[] $\vvec{u}=[\tau_1,\tau_2,\tau_3]$
      \item<5->[] Other forces included in $\vvec f$
      \end{itemize}
    \end{column}
  \end{columns}  
\end{frame}

% ****************************************************************************
% *                          GOAL  
% ****************************************************************************
\begin{frame}{The problem -- continued}
  \only<1->{
    \begin{block}{\scriptsize Neuroanimator: Fast neural network emulation and control
        of physics-based models.}
      \scriptsize Grzeszczuk, Radek, Demetri Terzopoulos, and Geoffrey
      Hinton.  SIGGRAPH 1998.
    \end{block}
    \vspace{1em}
  }

  Given a pendulum with fixed properties (fixed $\vvec f$),
  \begin{itemize}
  \item<2-> Train a neural network to solve:
  \item<3->[]   $\dot{\vvec{s}} = \vvec{f}(t,\vvec{s},\vvec{u}), \vvec{s}=\vvec{s}_0, \vvec{u}=\vvec{u}(t),$
  \item<4->[]  i.e., the behaviour of the system.
  \item<5->[] which is ``visually'' acceptable.
  \item<6-> Implement a control algorithm,
  \item<7->[] i.e., solve for $\vvec{u}$ in the inverse problem:
  \item<7->[] $\dot{\vvec{s}} = \vvec{\tilde{f}}(t,\vvec{s},\vvec{u}), s=s(t)$
  \end{itemize}
\end{frame}

% ****************************************************************************
% *                          Learning problem
% ****************************************************************************
\begin{frame}{Learning problem}
  \begin{columns}    
    \begin{column}{0.75\textwidth}
      \begin{itemize}
      \item<1-> Input: $\vvec{y}=[\textcolor{mlightblue}{\vvec{s}_0}, \textcolor{mdarkgreen}{\vvec{u}_0}]$
      \item<2-> Regression Variable: $\vvec{c}=[\textcolor{morange}{\vvec{s}_{\Delta t}}-\textcolor{mlightblue}{\vvec{s}_0}]$, when $\vvec{u}(t)=\textcolor{mdarkgreen}{\vvec{u}_0}$
      \item<3-> $\vvec f$ is found using the Lagrange equations.
      \item<4-> The ODE is solved for using \texttt{RK45}.
      \item<5-> Sampling:
      \item<5->[] {\scriptsize \tt Y = [rand(n, 3)*r1 rand(n, 3)*r2 rand(n, 3)*r3]}
      \item<6-> Paramters: $\Delta t$, $r_1$, $r_2$, $r_3$.
      \end{itemize}
    \end{column}
    \begin{column}{0.14\textwidth}
      \includegraphics<1>[width=0.95\linewidth]{img/training-before.pdf}
      \includegraphics<2->[width=0.95\linewidth]{img/training-after.pdf}
    \end{column}
  \end{columns}
\end{frame}

% ****************************************************************************
% *                          Neural Network
% ****************************************************************************
\begin{frame}{Neural network}
  \begin{itemize}
  \item<1-> Single layer NN with hidden units: \texttt{[8*size(y)]}
  \item<2-> Deep residual NN with hidden units: \texttt{[20, 20, 3*size(y)]}
  \item<3-> We did not perform an extensive search for the network architectures.
  \item<4-> Optimization: SGD, \alert<5->{non-linear CG}, Newton with linesearch
  \item<5->[] {\scriptsize {\tt minimize.m} by Carl Edward Rasmussen. }
  \end{itemize}

\end{frame}

% ****************************************************************************
% *                          Results -- No control
% ****************************************************************************
\begin{frame}{Results -- no $\vvec u$}

  \fbox{\parbox{\textwidth}{\centering\scriptsize \tt n\_links=3; M=[1,1,1]; L=[1,1,1]; LG=L; g=10; I=[0,0,0]; c=[1,1,1]; n=5000; r1=2pi; r2=2;}}
  \begin{columns}
    \begin{column}{0.48\linewidth}
      \centering
      % \includegraphics[width=0.7\linewidth]{img/history-no-u.pdf} \\[1em]
      \footnotesize
      \begin{tabular}{ccc}
        \hline
        Arch. &Train error &Test error \\
        \hline
        deep &$4.13\%$  &$4.75\%$ \\
        single layer &$7.35\%$ &$7.72\%$ \\
        \hline
        \end{tabular}\vspace{3em}
      \href{http://h00shi.github.io/FILES/eosc550_pres/anim-no-u.gif}{\includegraphics[width=0.5\linewidth]{img/demo-no-u.pdf}} 
    \end{column}
    \begin{column}{0.48\linewidth}
      \includegraphics[width=0.7\linewidth]{img/anim-no-u.pdf}
    \end{column}
  \end{columns}
  % add train and test error
\end{frame}

% ****************************************************************************
% *                          Results -- With control
% ****************************************************************************
\begin{frame}{Results -- with $\vvec u$}
  \fbox{\parbox{\textwidth}{\centering\scriptsize \tt n\_links=3; M=[1,1,1]; L=[1,1,1]; LG=L; g=10; I=[0,0,0]; c=[1,1,1]; n=5000; r1=2pi; r2=2; u=[5*cos(t),0,sin(t)]}}
  \begin{columns}
    \begin{column}{0.48\linewidth}
      \centering
      % \includegraphics[width=0.7\linewidth]{img/history-no-u.pdf} \\[1em]
      \footnotesize
      \begin{tabular}{ccc}
        \hline
        Arch. &Train error &Test error \\
        \hline
        deep &$7.23\%$  &$7.97\%$ \\
        single layer &$10.57\%$ &$11.97\%$ \\
        \hline
      \end{tabular}\vspace{3em}
      \href{http://h00shi.github.io/FILES/eosc550_pres/anim-with-u.gif}{\includegraphics[width=0.5\linewidth]{img/demo-with-u.pdf}} 
    \end{column}
    \begin{column}{0.48\linewidth}
      \includegraphics[width=0.7\linewidth]{img/anim-with-u.pdf}
    \end{column}
  \end{columns}
  % add train and test error
\end{frame}

% ****************************************************************************
% *                             Control
% ****************************************************************************
\begin{frame}{Control problem}
  \begin{center}
  \includegraphics[width=0.8\linewidth]{img/control.pdf}
\end{center}
\begin{itemize}
\item \textcolor{mlightblue}{$s_0, \text{target}$}
\item \textcolor{mdarkgreen}{$\vvec U = [\vvec u_0, \vvec u_1, \ldots, \vvec u_{M-1}]$}
\item \textcolor{morange}{$\vvec{S} = [\vvec s_1, \vvec s_2, \ldots, \vvec s_M]$}
  \item Control Problem:
  \item[] $\arg\min_{\vvec{U}} E(\textcolor{mdarkgreen}{\vvec U};\textcolor{mlightblue}{\text{target}, s_0}) =  \mu\|\textcolor{mdarkgreen}{\vvec U}\|_2^2 + (\textcolor{morange}{s_M} - \textcolor{mlightblue}{\text{target}})^2$ 
\end{itemize}
\end{frame}

% ****************************************************************************
% *                     Finding the derivative of E
% ****************************************************************************
\begin{frame}{Gradient of control objective, $\nabla_U E$}
  Similar backpropagation problem as deep neural networks:
  \[
    \begin{bmatrix}%
 \mmat{I} &\mmat{I}+\textcolor{morange}{\partial_{\vvec{s}_{1}}\vvec{C}_2} &&\\
  &\ddots &&           \\
  &\mmat{I} &\mmat{I}+\textcolor{morange}{\partial_{\vvec s_{j}}\vvec{C}_{j+1}} &\\
        &       &\ddots    &                \\
        &       &          &\mmat{I}             \\   
    \end{bmatrix}%   
    %
    \begin{bmatrix}%
      \partial_{\vvec s_{1}}\vvec s_{M}  \\
      \vdots \\
      \partial_{\vvec s_{j}}\vvec s_{M}  \\
      \vdots \\
      \partial_{\vvec s_{M}}\vvec s_{M} 
    \end{bmatrix}%
    = %
    \begin{bmatrix}%
      0  \\
      \vdots \\
      0  \\
      \vdots \\
      \mmat I 
    \end{bmatrix}%
  \]
  \[
    \textcolor{mdarkgreen}{\partial_{\vvec u_j}{\vvec s_M}} = \partial_{\vvec s_{j+1}}{\vvec s_M}\left(\textcolor{morange}{\partial_{\vvec u_{j}}{\vvec{C}_{j+1}}} + \mmat I \right)
  \]
  
\end{frame}

% ****************************************************************************
% *                      Control Results
% ****************************************************************************
\begin{frame}[fragile]{Control Results}
  \small
  \begin{columns}
    \begin{column}{0.48\linewidth}
      Easier pendulum:
\begin{verbatim}
n_links = 3;   
M = [1 1 1];    
L = [1 1 1];     
LG = [1, 1, 1];  
g = 10;          
I = [0,0,0];  
c = [1, 1, 1];   
\end{verbatim}
      \href{http://h00shi.github.io/FILES/eosc550_pres/control-simple.gif}{\includegraphics[width=0.8\linewidth]{img/control-1.pdf}}
    \end{column}
    \begin{column}{0.48\linewidth}
      More complicated physics:
\begin{verbatim}
n_links = 3;  
M = [1 1.5 2];    
L = [1 1.5 2];    
LG = L ./ 2.;  
g = 10;         
I = 1/12 .* M .* L .* L;     
c = [1, 1, 1];
\end{verbatim}
      \href{http://h00shi.github.io/FILES/eosc550_pres/control-hard.gif}{\includegraphics[width=0.8\linewidth]{img/control-2.pdf}}
    \end{column}
  \end{columns}
\end{frame}

% ****************************************************************************
% *                           Appendix -- Lagrange
% ****************************************************************************

% ****************************************************************************
% *                           Appendix -- RK4
% ****************************************************************************

\end{document}