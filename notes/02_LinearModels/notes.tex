%
% Template for Scribe Notes -- CS542F, modified from Will Evans' CS420+500
% course offering
%
% (template is a modified version of one prepared by Erik Demaine at MIT)
%  
%
%
\documentclass[11pt]{article}
\usepackage{latexsym}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{epsfig}
\usepackage[margin=1in]{geometry}
\usepackage{float}

\usepackage{bm}
\usepackage{cancel}

\newcommand{\scribeTitle}[3]{
  \noindent
  \begin{center}
  \framebox{
    \vbox{
      \hbox to 5.78in { \hfill \textbf{EOSC550: Numerical Methods for Deep Learning} \hfill }
      \vspace{4mm}
      \hbox to 5.78in { {\Large \hfill Lecture: #1  \hfill} }
      \vspace{2mm}
      \hbox to 5.78in { \emph{#2 \hfill Scribe: #3} }
    }
  }
  \end{center}
  \vspace*{4mm}
}

\newcommand{\R}{\mathbb{R}}     % use for capital R real number set symbol
\newcommand{\Z}{\mathbb{Z}}     % use for capital Z integer set symbol
\newcommand{\E}{\mathbb{E}}     % use for capital Z expected value
\newcommand{\epi}{\text{epi}\ }
\newcommand{\spacevert}{\ \vert\ }
\newcommand{\mul}[1]{\mathbf{#1} }
\newcommand{\mulvec}[1]{\bm{#1} }
\newcommand{\ve}{\text{vec} }
\newcommand{\mat}{\text{mat} }
\newcommand{\tr}{\text{trace} }

% tabbing environment for pseudo-code 
\newenvironment{code} {
\begin{tt}
\begin{tabbing}
\ \ \ \ \= \ \ \ \ \= \ \ \ \ \= \ \ \ \ \= \ \ \ \ \= \ \ \ \ \=  \ \ \ \ \= \ \ \ \ \= \\ \kill
}{
\end{tabbing}
\end{tt}
}


\newtheorem{theorem}{Theorem}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{fact}[theorem]{Fact}
\newtheorem{assumption}[theorem]{Assumption}

\parindent 0in
\parskip 1.5ex
% \renewcommand{\baselinestretch}{1.25}

\newenvironment{equ} {
  \begin{equation}\begin{aligned}
    }{
    \end{aligned}\end{equation}
}


\begin{document}

\scribeTitle{Linear Models (Least Squares)}{Prof.\ Eldad Haber‎}{Chenxi Liu, Shayan Hoshyari}

\section{Definition}

The goal is to find a model that assigns a label for each input sample.

Formally, let samples and labels respectively be
\begin{align}
  \mathbf{Y} & =
  \begin{pmatrix}
    \mathbf{y_1}^T\\
    \mathbf{y_2}^T\\
    \vdots\\
    \mathbf{y_{n}}^T
  \end{pmatrix} \in \R^{n \times n_f},\\
  \mathbf{C} & =
  \begin{pmatrix}
    \mathbf{c_1}^T\\
    \mathbf{c_2}^T\\
    \vdots\\
    \mathbf{c_{n}}^T
  \end{pmatrix} \in \R^{n \times n_f}.
\end{align}
The goal is to find a function $f(\mathbf{Y}, \mathbf{\theta}) \approx \mathbf{C}$, where $\mulvec{\theta}$ contains parameters.

A simple option is to use a linear model with $\mathbf{\theta} = (\mathbf{W}, \mulvec{b})$ defined as
\begin{equ}
  f(\mathbf{Y}, \mathbf{W}, \mulvec{b}) = \mul{YW} + \mulvec{1b}^T & \approx \mul{C},\\
  \begin{pmatrix}
    \mul{Y} & \mulvec{1}
  \end{pmatrix}
  \begin{pmatrix}
    \mul{W} \\
    \mulvec{b}^T
  \end{pmatrix}
  & \approx \mul{C}.
\end{equ}
where $\mul{W} \in \R^{n_f \times n_c}$ are weights, $\mulvec{b} \in \R^{n_c}$ are biases, $\mulvec{1} \in \R^n$ is a vector of all ones.

The least square problem is introduced to approximately solve this system when there exist infinite solutions or no solution.
It is defined as
\begin{align}
  \min_{\mul{W}} \frac{1}{2} || \mul{YW} - \mul{C} ||^2_F.
\end{align}
Here, $\mul{Y}, \mul{W}$ are redefined to have $\mulvec{1}, \mulvec{b}$ inside.
Frobenius norm is defined as $||\mul{A}||^2_F = \ve(\mul{A})^T\ve(\mul{A}) = \tr(\mul{A}^T\mul{A}) = \sum_{i, j} \mul{A}^2_{i, j}$.
Operation $\ve(\cdot)$ converts inputs to vectors in column-major order.

\section{Normal Equation}

Normal equation is derived by setting the gradient $\nabla_{\mul{W}} f = 0$, where $(\nabla_{\mul{W}} f)_{ij} = \frac{\partial f}{\partial \mul{W}_{ij}}$.
Note that we define  $\nabla_{\mul{W}} f = \mat({J_{\mulvec{w}} f}^T) = \mat(\frac{\partial f}{\partial \mulvec{w}}^T) $, where $\mulvec{w} = \ve(\mul{W})$. (a little different to the slides).

Before the derivation, introduce several helpful formulae.
\begin{align}
  \ve(\mul{AXB}) &= (\mul{B}^T \otimes \mul{A}) \ve( \mul{X} ) %%,\\
\end{align}
where $\mulvec{x} = \ve(\mul{X})$.

% When $\mul{AXB}$ is a vector, we have
% \begin{align}
%   \ve(\mul{AXB}) &= \ve(\mul{B^TX^TA^T}) = (\mul{A} \otimes \mul{B}^T) \ve(\mul{X}^T)\\
%   &= (\mul{A} \otimes \mul{B}^T) \mulvec{x}.
      %   \end{align}
For an operator $\ve^*$ which stacks rows of the matrix to a single
vector, rather than the columns, the identity would read slightly
differently:
\[
  \ve^*(\mul{AXB}) = (\mul{A} \otimes \mul{B}^T)  \ve^*( \mul{X} ).
\]

Now, we derive $\nabla_{\mul{W}} f = \nabla_{\mul{W}} \frac{1}{2} || \mul{YW} - \mul{C}||_F^2$.

Let $\mul{R} = \mul{YW} - \mul{C}, \mulvec{r} = \ve(\mul{R})$.
Then,
\begin{equ}
  \mulvec{r} &= \ve(\mul{YWI}) - \mul{C}\\
  &= (\mul{I} \otimes \mul{Y})\mulvec{w} - \mulvec{c}.
\end{equ}

To find the derivative in the vector form (the Jacobian as a row vector), apply the chain rule,
\begin{align}
  \frac{\partial f}{\partial \mulvec{w}} &= \frac{\partial f}{\partial \mulvec{r}} \frac{\partial \mulvec{r}}{\partial \mulvec{w}}.
\end{align}
\begin{align}
  &\frac{\partial f}{\partial \mulvec{r}}  = \frac{1}{2} \frac{\partial (\mulvec{r^T r})}{\partial \mulvec{r}}   = \mulvec{r}^T\\
  &\frac{\partial \mulvec{r}}{\partial \mulvec{w}}  = \mul{I} \otimes \mul{Y}.
\end{align}
Therefore, $\frac{\partial f}{\partial \mul{w}} = \mul{r}^T(\mul{I} \otimes \mul{Y})$.

To put it back into the matrix form,
\begin{equ}
  \nabla_{\mul{W}} f &= \mat(\frac{\partial f}{\partial \mul{w}}^T)\\
  &= \mat((\mul{I} \otimes \mul{Y}^T) \mul{r})\\
  &= \mat(\mul{Y}^T\mul{rI}) \\
  &= \mul{Y}^T \mul{R}\\
  &= \mul{Y}^T(\mul{YW - C}).
\end{equ}

We also define two notations for finding the matrix-vector products of
the second derivative.
\begin{equ}
  &(J(\nabla f)) \mul{V} = (\nabla^2 f) \mul{V} %
  && = \mat \left( %
    \left[ \frac{\partial}{\partial \mulvec{w} } %
      \left( \frac{\partial f}{ \partial \mulvec{w}}^T \right)  \right] \mulvec{v} %
  \right). \\
%%%%%
  &(J(\nabla f))^T \mul{U}  %
  && = \mat \left( %
    \left[ \frac{\partial}{\partial \mulvec{w} } %
      \left( \frac{\partial f}{ \partial \mulvec{w}}^T \right)  \right]^T \mulvec{u} %
  \right).
\end{equ}



As a special case for linear regression:
\[
  \begin{aligned}
    & \frac{\partial f}{ \partial \mulvec{w}}^T  = %
    \left[ \frac{\partial}{\partial \mulvec{r} } %
      \left( \frac{\partial f}{ \partial \mulvec{w}}^T \right) \right] %
  \frac{\partial \mulvec r}{\partial\mulvec{w}} = %
  (\mul{I} \otimes \mul{Y}^T)(\mul{I} \otimes \mul{Y}) \Rightarrow \\
  &(J_w^2 f) \mulvec{v} = %
  (\mul{I} \otimes \mul{Y}^T)(\mul{I} \otimes \mul{Y}) \mulvec v = %
  (\mul{I} \otimes \mul{Y}^T)\ve(\mul{YV}) = %
  \ve(\mul{Y}^T\mul{YV}) \Rightarrow \\ %
  &(\nabla^2 f) \mul{V} = \mul{Y}^T\mul{YV}.
  \end{aligned}
\]

\section{Regularization}

When the problem is ill-posed, we could solve for the regularized problem
\begin{align}
  \min_{\mul{W}} \frac{1}{2} || \mul{YW} - \mul{C} ||^2_F + \frac{\alpha}{2} ||\mul{W}||^2_F,
\end{align}
where $\alpha > 0$.
This formulation could be interpreted as using a bias of $\mul{0}$ for $\mul{W}$.

The regularized normal equation is then $\mul{W} = (\mul{Y^T Y} + \alpha \mul{I})^{-1}\mul{Y^TC}$.

\subsection{Bias-Variance Decomposition}

Let the ground true model be $\mul{W_t}$.
Introduce noise to the labeling: $\mul{C} = \mul{YW_t} + \mulvec{\epsilon}$, where $\mulvec{\epsilon} \sim \mathcal{N}(0, \sigma)$.

Let $\mul{Y}_\alpha = (\mul{Y^T Y} + \alpha \mul{I})^{-1}$.
The error would be
\begin{equ}
  \mul{W} - \mul{W_t} &= (\mul{Y_\alpha Y^T Y W}_t + \mul{Y_\alpha Y^T}\mulvec{\epsilon}) - \mul{W_t} \\
  &= (\mul{Y_\alpha Y^T Y} - \mul{I})\mul{W}_t + \mul{Y_\alpha Y^T} \mulvec{\epsilon} \\
  &= (\mul{Y_\alpha}(\mul{Y^T Y} + \alpha \mul{I}) - \alpha \mul{Y_\alpha} - \mul{I})\mul{W}_t + \mul{Y_\alpha Y^T} \mulvec{\epsilon} \\
  &= - \alpha \mul{Y_\alpha} \mul{W}_t + \mul{Y_\alpha Y^T } \mulvec{\epsilon}.
\end{equ}

Let $\mul{A} = \mul{Y_\alpha} \mul{W}_t$ and $\mul{B} = \mul{Y_\alpha Y^T}$.
The mean squared error would be
\begin{equ}
  \E || \mul{W} - \mul{W}_t||^2 &= \E (- \alpha \mul{A} + \mul{B} \mulvec{\epsilon})^T(- \alpha \mul{A} + \mul{B} \mulvec{\epsilon})\\
  &= \E (-\alpha \mul{A}^T +  \mulvec{\epsilon}^T\mul{B}^T)(- \alpha \mul{A} + \mul{B} \mulvec{\epsilon})\\
  &= \E (\alpha^2||\mul{A}||^2 - \cancelto{0}{\alpha \mul{A^TB}\mulvec{\epsilon}} - \cancelto{0}{\alpha \mulvec{\epsilon}^T \mul{B^TA}} +\mulvec{\epsilon}^T\mul{B}\mulvec{\epsilon})\\
  &= \alpha^2||\mul{A}||^2 + \E(\mulvec{\epsilon}^T\mul{B}\mulvec{\epsilon})\\
  &= \alpha^2||\mul{A}||^2 + \sigma^2 \tr(\mul{B})\text{, since elements of }\mulvec{\epsilon}\text{ are independent.}
\end{equ}

The first term is called the bias (corresponding to \emph{underfitting}) and the second is called the variance (corresponding to \emph{overfitting}).
The decomposition then suggests that introducing bias can reduce the variance and thus generalize the model.

\end{document}
