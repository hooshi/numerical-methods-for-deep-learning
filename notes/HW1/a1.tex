%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                            INCLUDES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[‎oneside‎,11pt]{article}

% \usepackage[fleqn]{amsmath}
\usepackage{amsmath}
\usepackage{fullpage}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{url}
\usepackage{graphicx}
\usepackage{parskip}
\usepackage{amssymb}
\usepackage{nicefrac}
\usepackage{algorithm2e}
\usepackage{float}
\usepackage{fancyvrb}
% \usepackage{listings} 
\usepackage{tikz}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                            MACROS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  USE LIBERTINE
\usepackage[T1]{fontenc}
\usepackage{kpfonts}%  for math    
\usepackage{libertine}%  serif and sans serif
\usepackage[scaled=0.85]{beramono}%% mono
% \setlength{\mathindent}{0pt}

%% hyperref
\usepackage[bookmarks,bookmarksnumbered,%
allbordercolors={0.2 0.6 0.6},%
linktocpage%
]{hyperref}

% Colors
\definecolor{blu}{rgb}{0.2,0.0,0.7}
\def\blu#1{{\color{blu}#1}}
\definecolor{gre}{rgb}{0,.5,0}
\def\gre#1{{\color{gre}#1}}
\definecolor{red}{rgb}{1,0,0}
\def\red#1{{\color{red}#1}}
\def\norm#1{\|#1\|}

% Math
\def\R{\mathbb{R}}
\def\argmax{\mathop{\rm arg\,max}}
\def\argmin{\mathop{\rm arg\,min}}



 \newlength{\tempheight}
 \newlength{\tempwidth}

 \newcommand{\rowname}[1]% #1 = text
 {\rotatebox{90}{\makebox[\tempheight][c]{#1}}}
 \newcommand{\columnname}[1]% #1 = text
 {\makebox[\tempwidth][c]{#1}}

 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
 %% DOCUMENT
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
 \begin{document}

 \title{EOSC 550 Assignment 1}
 \author{Shayan Hoshyari, 81382153}
 \date{}
 \maketitle

 In this assignment, I have implemented an extreme learning machine
 (ELM) and a fully connected one layer neural network (NN). For
 optimization, our vanilla gradient descent implementation from the
 lectures, Newton's method (except for the neural network), and a
 fancier gradient descent solver from CE Rasumen~\footnote{\tt
   http://learning.eng.cam.ac.uk/carl/code/minimize/} can be used.

 The minimization problem for the extreme learning machine is
 \[
   min_{W,b_w} \left( E( f(YK+eb_k)W+eb_w, C ) + R(W,b_w) \right),
 \]
 where $E$ is the softmax function, $R$ is the regularization term,
 $Y\in\R^{n \times d_1}$ is the training data, $C\in\R^{n \times c}$
 is the observed probabilities, $f$ is the activation function, $e$ is
 a vector of ones, and $K\in\R^{d_1\times d_2}$ and
 $b_k\in\R^{1\times d_2}$ are the random feature transformation matrix
 and the random bias, respectively. Finally, $W \in \R^{d_2\times c}$
 and $b_w \in \R^{1 \times d_2}$ are the weights and the bias that
 have to be solved for.  For a fully connected one layer neural
 network, $K$ and $b_k$ are also solved for. Thus, the minimization
 problem would be
 \[
   min_{W,b_w,K,b_k} \left(  E(f(YK+eb_k)W+eb_w, C) + R(W,b_w,K,b_k) \right).
 \]

 To regularize the extreme learning machine the regularization term
 \[
   R = \alpha \left( \|b_w\|^2_2 +  \|W\|^2_F\right)
 \]
 is used. For the neural network, however, the regularization term is
 slightly more flexible by allowing separate coefficients for each
 variable.
 \[
   R =  %
     \alpha_{bw}\|b_w\|^2_2 +  \alpha_W \|W\|^2_F %
     + \alpha_{bk}\|b_k\|^2_2 +  \alpha_k \|K\|^2_F %
 \]
 
 The classifiers are evaluated on four datasets: circles, spiral,
 MNIST, and CIFAR10. The circles and spiral datasets can be seen in
 Figures \ref{fig:circle-elm} and \ref{fig:spiral-elm},
 respectively. The driver scripts are 
\begin{Verbatim}[frame=single]
driver_linear_test.m         %% ELM for spiral and circles
driver_linear_MNIST.m        %% ELM for MNIST
driver_linear_CIFAR10.m      %% ELM for CIFAR10
driver_nn1full_test.m        %% Neural network for spiral and circles
driver_nn1full_MNIST.m       %% Neural network for MNIST
driver_nn1full_CIFAR10.m     %% Neural network for CIFAR 10
\end{Verbatim}
 and can be found in the root directory of the code. The drivers
 assume that the paths to MNIST and CIFAR10 are specified in the
 script
\begin{Verbatim}[frame=single]
+utility/init_paths.m
\end{Verbatim}
 as {\tt DIR\_MNIST} and {\tt DIR\_CIFAR10}. Also, the functions {\tt
   loadMNIST...} must be on the path.

 As the circles and the spiral datasets are very simple, I have just
 visualized the classifier boundaries for evaluation purposes, and
 skipped calculating a separate validation score. The results are
 presented in the following sections.
 
 %%===========================================================================
 %%                               CIRCLES
 %%===========================================================================
 \section{2-D Circles}

 This is a rather easy problem. The ELM can fit the data with a
 feature ratio of $\frac{d_2}{d_1}=40$, while the neural net only
 needs $\frac{d_2}{d_1}=7$. The regularization parameters used are
 shown in Table~\ref{tab:circle-regularize}, although the problem is
 not very sensitive to regularization. All the working combinations of
 activation function, classifiers, and minimizers are shown in
 Table~\ref{tab:circle-working}. The only combination that I could
 not get to work was NN with relu activation. My guess is that there
 is either a bug or it is because the objective function is
 non-differentiable.

 
  \begin{table}
   \centering
   \caption{Regularization parameters used for the circles dataset.}
   \label{tab:circle-regularize}
   \begin{tabular}{ccccc}
     \hline
      $\alpha$ &$\alpha_{bw}$ &$\alpha_W$ &$\alpha_{bk}$ &$\alpha_K$  \\
     \hline     
     $10^{-7}$ &$0$ &$10^{-4}$ &$0$ &$10^{-6}$ \\
     \hline
   \end{tabular}
   \vspace{1cm}
   
   \caption{List of combination of classifier and minimization methods
     that work reasonably for the circles dataset.}
   \label{tab:circle-working}
   \begin{tabular}{c c c c }
     \hline
     Classifier &Activation &Ratio of features &Minimizer \\
     \hline     
     ELM    &TANH, RELU  &40    &Newton, {\tt minimize}, Vanilla GD \\
     NN     &TANH  &7    &{\tt minimize}, Vanilla GD \\
     \hline
   \end{tabular}
 \end{table}

 The classifier boundaries for the ELM are shown in
 Figure~\ref{fig:circle-elm}. They are obtained using a few iterations
 of the Newton minimizer, and both seem to be reasonable.


 \begin{figure}
   \centering
   \setlength{\tempwidth}{0.45\linewidth}
   \settoheight{\tempheight}{\includegraphics[width=\tempwidth]{img/nn1full_test_history_CIRCLES_tanh_GDMINIMIZE}}
   \columnname{tanh}
   \columnname{relu} \\
   \includegraphics[width=\tempwidth]{img/linear_test_history_CIRCLES_tanh_GDNEWTON}
   \includegraphics[width=\tempwidth]{img/linear_test_history_CIRCLES_relu_GDNEWTON} \\
   \caption{Extreme learning machine: visualizations of the classifier
     boundaries for the circle dataset. Ratio of features is 40.}
   \label{fig:circle-elm}
 \end{figure}

 The classifier boundaries for the NN are shown in
 Figure~\ref{fig:circle-nn}. Two interesting points can be
 observed. First, the relu activation function does not seem to work.
 Secondly, the {\tt minimize} function only needs 300 iterations to
 solve the minimization problem for tanh activation, while our vanilla
 gradient descent needs something around 5000 iterations. This can be
 further observed by looking at the minimizer convergence histories
 shown in Figure~\ref{fig:circle-history}.

 
 \begin{figure}
   \centering
   \setlength{\tempwidth}{0.45\linewidth}
   \settoheight{\tempheight}{\includegraphics[width=\tempwidth]{img/nn1full_test_history_CIRCLES_tanh_GDMINIMIZE}}
   \columnname{gd-minimize, tanh, 300 iters}
   \columnname{gd-minimize, relu, 30000 iters} \\
   \includegraphics[width=\tempwidth]{img/nn1full_test_history_CIRCLES_tanh_GDMINIMIZE}
   \includegraphics[width=\tempwidth]{img/nn1full_test_history_CIRCLES_relu_GDMINIMIZE} \\
   %%% 
   \columnname{gd-vanilla, tanh, 1500 iters}
   \columnname{gd-vanilla, tanh, 2500 iters} \\
   \includegraphics[width=\tempwidth]{img/nn1full_test_history_CIRCLES_tanh_GDOURS_1500}
   \includegraphics[width=\tempwidth]{img/nn1full_test_history_CIRCLES_tanh_GDOURS_2500} \\
   %%%% 
   \columnname{gd-vanilla, tanh, 5000 iters}
   \columnname{gd-vanilla, tanh, 10000 iters} \\
   \includegraphics[width=\tempwidth]{img/nn1full_test_history_CIRCLES_tanh_GDOURS_5000}
   \includegraphics[width=\tempwidth]{img/nn1full_test_history_CIRCLES_tanh_GDOURS_10000} \\
   \caption{Fully connected one layer neural net: visualizations of
     the classifier boundaries for the circle dataset. Ratio of
     features is 7.}
   \label{fig:circle-nn}
 \end{figure}


 \begin{figure}
   \centering
   \includegraphics[width=0.45\linewidth]{img/nn1full_CIRCLES_history}
   \caption{Fully connected one layer neural net: logistic loss
     convergence for the circles dataset}
      \label{fig:circle-history}
 \end{figure}

 %%===========================================================================
 %%                               SPIRAL
 %% ===========================================================================
 
 \section{2-D Spiral}
 
 This problem seems to be much harder than the circles dataset. The
 only working combinations of classifiers and minimizers are shown in
 Table~\ref{tab:spiral-working}. Ironically, only tanh activation
 works for the NN, while only the relu activation works for ELM. Maybe
 relu activation is indeed powerful, but our optimizers can not find
 its minimum. The regularization parameter for the ELM is $10^{-7}$,
 while all $\alpha$ values are set to $10^{-8}$ for the NN. The ELM is
 much less sensitive to regularization compared to the NN.

 \begin{table}
   \centering
   \caption{List of combination of classifier and minimization methods
     that work reasonably for the spiral dataset. ELM is the extreme
     learning machine, and NN is a single layer fully connected neural
     network.}
   \label{tab:spiral-working}
   \begin{tabular}{c c c c }
     \hline
     Classifier &Activation &Ratio of features &Minimizer \\
     \hline
     
     ELM    &RELU  &300   &Newton (but not Gauss-Newton)\\
     ELM    &RELU  &300   &{\tt minimize} \\
     NN     &TANH  &20    &{\tt minimize} \\
     \hline
   \end{tabular}
 \end{table}


 The classifier boundaries for the ELM are shown in
 Figure~\ref{fig:spiral-elm}, where the feature ratio is 300. They are
 obtained the Newton minimizer, which usually terminates after 6 or 7
 iterations due to linesearch break. Only relu activation seems to
 work well.

 The classifier boundaries for the NN are shown in
 Figure~\ref{fig:spiral-nn} for a feature ratio of 20. Unlike the ELM,
 the tanh activation seems to be working best here. Also, our vanilla
 gradient descent fails to find a good minimum. The optimizer
 histories are shown in Figure~\ref{fig:spiral-history}. Not
 surprisingly, only the combination of tanh activation and the {\tt
   minimize} function is able to reduce the logistic loss
 considerably.
 
 \begin{figure}
   \centering
   \setlength{\tempwidth}{0.45\linewidth}
   \settoheight{\tempheight}{\includegraphics[width=\tempwidth]{img/nn1full_test_history_CIRCLES_tanh_GDMINIMIZE}}
   \columnname{tanh}
   \columnname{relu} \\
   \includegraphics[width=\tempwidth]{img/linear_test_history_SPIRAL_tanh_GDNEWTON}
   \includegraphics[width=\tempwidth]{img/linear_test_history_SPIRAL_relu_GDNEWTON}\\
   \caption{Extreme learning machine: visualizations of the classifier
     boundaries for the spiral dataset. Ratio of features is 300.}
   \label{fig:spiral-elm}
 \end{figure}

 
 \begin{figure}
   \centering
   \setlength{\tempwidth}{0.45\linewidth}
   \settoheight{\tempheight}{\includegraphics[width=\tempwidth]{img/nn1full_test_history_CIRCLES_tanh_GDMINIMIZE}}
   \columnname{gd-minimize, tanh} \\
   \includegraphics[width=\tempwidth]{img/nn1full_test_history_SPIRAL_tanh_GDMINIMIZE}\\
   \columnname{gd-minimize, relu}
   \columnname{gd-vanilla, tanh} \\
   \includegraphics[width=\tempwidth]{img/nn1full_test_history_SPIRAL_relu_GDMINIMIZE}
   \includegraphics[width=\tempwidth]{img/nn1full_test_history_SPIRAL_tanh_GDOURS} \\
   %%%% 
   \caption{Fully connected one layer neural net: visualizations of
     the classifier boundaries for the circle dataset. Ratio of
     features is 20.}
   \label{fig:spiral-nn}
 \end{figure}

  \begin{figure}
   \centering
   \includegraphics[width=0.45\linewidth]{img/nnfull_SPIRAL_history.pdf}
   \caption{Fully connected one layer neural net: logistic loss
     convergence for the spiral dataset}
   \label{fig:spiral-history}
 \end{figure}


 %%===========================================================================
 %%                                 MNIST
 %%===========================================================================
 \section{MNIST}

 The obtained results for the MNIST data set are shown in
 Table~\ref{tab:mnist}. The best testing accuracy is $97\%$ and is
 obtained by using ELM with RELU activation and a feature ratio of $2$
 or $3$. For all ELMs I have used $\alpha=10^{-2}$. The Newton solver
 is used for ELMs, while {\tt minimize} is used for the
 NN. Figure~\ref{fig:mnist-history} shows the convergence of the
 optimizers.
 
 \begin{table}
   \centering
   \caption{Performance of different classifiers on the MNIST
     dataset.}
   \label{tab:mnist}
   \begin{tabular}{c c c c c c c}
     \hline
     Classifier &Activation &Ratio of features &Regularization Matrix
     &Training Accuracy &Testing Accuracy \\
     \hline
     
     Linear &---   &---   & Laplacian &0.91 &0.91 \\
     Linear &---   &---   & Identity  &0.90 &0.91 \\
     ELM    &TANH  &2     & Identity  &0.99 &0.97 \\
     ELM    &TANH  &3     & Identity  &1.00 &0.97 \\
     ELM    &RELU  &2     & Identity  &0.96 &0.95 \\
     ELM    &RELU  &3     & Identity  &0.97 &0.96 \\
     NN     &TANH  &1     & Identity    &1.00 &0.96 \\
     \hline
   \end{tabular}
 \end{table}
   
 \begin{figure}
   \centering
   \includegraphics[width=0.85\linewidth]{img/MNIST_history.pdf}
   \caption{Logistic loss convergence for the MNIST dataset. (Left)
     Single layer neural net using the {\tt minimize}
     function. (Right) Linear classifier with Newton iterations.}
   \label{fig:mnist-history}
 \end{figure}

 %%===========================================================================
 %%                               CIFAR 10
 %%===========================================================================
\section{CIFAR10}

The obtained results for the CIFAR10 data set are shown in
Table~\ref{tab:cifar10}. The best testing accuracy is $49\%$ and is
obtained by using ELM with RELU activation and a feature ratio of
$3$. For all ELMs I have used $\alpha=10^{-2}$. The Newton solver is
used for ELMs, while {\tt minimize} is used for the
NN. Figure~\ref{fig:cifar10-history} shows the convergence of the
optimizers.
 
\begin{table}
   \centering
   \caption{Performance of different classifiers on the CIFAR10
     dataset. }
   \label{tab:cifar10}
   \begin{tabular}{c c c c c c c}
     \hline
     Classifier &Activation &Ratio of features &Regularization Matrix
     &Training Accuracy &Testing Accuracy \\
     \hline
     
     Linear &---   &---   & Identity  &0.49 &0.39 \\
     ELM    &TANH  &2     & Identity  &0.86 &0.39 \\
     ELM    &TANH  &3     & Identity  &0.97 &0.39 \\
     ELM    &RELU  &2     & Identity  &0.75 &0.48 \\
     ELM    &RELU  &3     & Identity  &0.79 &0.49 \\
     NN     &TANH  &1     & Identity  &0.25 &0.21 \\
     \hline
   \end{tabular}
 \end{table}


 \begin{figure}
   \centering
   \includegraphics[width=0.85\linewidth]{img/CIFAR10_history.pdf}
   \caption{Logistic loss convergence for the CIFAR10 dataset. (Left)
     Single layer neural net using the {\tt minimize}
     function. (Right) Linear classifier with Newton iterations..}
   \label{fig:cifar10-history}
 \end{figure}
 
\end{document}