% Classification of CIFAR10 using a single layer fully connected neural
% network
%
% 
% Objective function is 
% logistic( sigma(YK + bk)W + bw, C)

clear
close all
rng(0);

%% Parameters
OPTIMIZATION_METHOD = "GD_MINIMIZE";
% OPTIMIZATION_METHOD = "GD_OURS";         % does not work well

RATIO_HIDDEN_UNITS = 1;                    % Bigger values really take time
MAX_OPTIMIZATION_ITERATIONS = 300;         % Bigger values really take time
REGULARIZATION_RATIO_K      = 1e-4;        
REGULARIZATION_RATIO_BK      = 0;
REGULARIZATION_RATIO_W      = 1e-6;
REGULARIZATION_RATIO_BW      = 0;

ACTIVATION = "tanh";
% ACTIVATION = "relu";                    % relu does not work very well.

N_CIFAR10_BATCHES=5;

historyfile = fopen('nn1full_CIFAR10_history_.txt', 'w');

fprintf(historyfile, "# OPTIMIZATION_METHOD = %s \n", OPTIMIZATION_METHOD);
fprintf(historyfile, "# RATIO_HIDDEN_UNITS = %.2f \n", RATIO_HIDDEN_UNITS);
fprintf(historyfile, "# REGULARIZATION_RATIO_K = %e \n", REGULARIZATION_RATIO_K);
fprintf(historyfile, "# REGULARIZATION_RATIO_BK = %e \n", REGULARIZATION_RATIO_BK);
fprintf(historyfile, "# REGULARIZATION_RATIO_W = %e \n", REGULARIZATION_RATIO_W);
fprintf(historyfile, "# REGULARIZATION_RATIO_BW = %e \n", REGULARIZATION_RATIO_BW);
fprintf(historyfile, "# ACTIVATION = %s \n", ACTIVATION);
fprintf(historyfile, "# N_CIFAR10_BATCHES = %d \n", N_CIFAR10_BATCHES);

%  6  5.968880e+01 # Function evaluation
%      8  5.301724e+01 # Function evaluation
%      9  5.002028e+01 # Function evaluation
%     13  4.975065e+01 # Function evaluation

%% setting all the path
% setting all the path
run('utility.init_paths.m')
addpath( DIR_CIFAR10 )

%% Upload the data and set it for training

% Read all
Yall = []; lball = [];
if(N_CIFAR10_BATCHES >= 1)
    load data_batch_1.mat
    Yall = [Yall; data]; lball = [lball; labels];
end
if(N_CIFAR10_BATCHES >= 2)
    load data_batch_2.mat
    Yall = [Yall; data];  lball = [lball; labels];
end
if(N_CIFAR10_BATCHES >= 3)
    load data_batch_3.mat
    Yall = [Yall; data]; lball = [lball; labels];
end
if(N_CIFAR10_BATCHES >= 4)
    load data_batch_4.mat
    Yall = [Yall; data];  lball = [lball; labels];
end
if(N_CIFAR10_BATCHES >= 5)
    load data_batch_5.mat
    Yall = [Yall; data];  lball = [lball; labels];
end

% Split to training and testing
splitter = cval.Splitter( size(Yall, 1), 2, true);

ind1 = splitter.get_indices(1);
Y = double(Yall( ind1, :));
lb = lball( ind1 );

ind2 = splitter.get_indices(2);
Ytest = double(Yall( ind2, :));
lbtest = lball( ind2 );

label_tools = utility.LabelTools(lbtest);
c = label_tools.n_labels;

Ctest = label_tools.old_labels_to_proba_matrix( lbtest );
Ctrain = label_tools.old_labels_to_proba_matrix( lb );

%% Neural net params
params = cl_nn_single_full.default_params();
params.n_d1 = size(Y, 2);
params.n_d2 = params.n_d1 * RATIO_HIDDEN_UNITS;
params.n_c = size(Ctrain, 2);
params.activation = ACTIVATION;
params.lambda_K = REGULARIZATION_RATIO_K;
params.lambda_bk = REGULARIZATION_RATIO_BK;
params.lambda_W = REGULARIZATION_RATIO_W;
params.lambda_bw = REGULARIZATION_RATIO_BW;
params.silence_k = false;


%% Initial conditions
x0 = randn( cl_nn_single_full.n_vars(params), 1) ; % sensitive


%% Train 

% ================= GD
if(strcmp(OPTIMIZATION_METHOD, "GD_OURS") )
    optim_reg = @(x,params) cl_nn_single_full.l2_regularizer(x,params);
    optim_fun = @(x,~)     cl_nn_single_full.objective(x,Y,Ctrain, params);
    params.alpha   = 1;
    params.verbosity = 1;
    params.n_gd_max_iter = MAX_OPTIMIZATION_ITERATIONS;
    % param.mu0=0.01;
    % param.fixed_mu = 1;
    params.n_linesearch_max_iter=40;
    params.mu0 = 0.01;
    x = optimization.gradient_descent(optim_fun,optim_reg,x0,params);
end

% ================= Minimize
if(strcmp(OPTIMIZATION_METHOD, "GD_MINIMIZE") )
    n_iters = -MAX_OPTIMIZATION_ITERATIONS;
    x = optimization.minimize(x0, @objective_for_minimize, n_iters, historyfile, Y, Ctrain, params);
end

% view solution
% [K, bk, W, bw] = cl_nn_single_full.unwrap_vars(x, params)




%% Test
cls = struct;
cls.predict = @(Yt) fn_predict(Yt, x, params);
fprintf(historyfile, '# Testing    Acc %3.2g\n', metric.accuracy(Ctrain, cls.predict(Y)) );
fprintf(historyfile, '# Validation Acc %3.2g\n', metric.accuracy(Ctest, cls.predict(Ytest)) )

fclose(historyfile);

%% Helpers
%%% Objective for minimize.m
function [f, df] = objective_for_minimize(x_, Y_, Cobs_, params_)
[R,dR] = cl_nn_single_full.l2_regularizer(x_,params_);
[E, dE] = cl_nn_single_full.objective(x_,Y_,Cobs_, params_);
f = E+ R;
df = dE+dR;
end

%%% Classification
function Cnew = fn_predict(Yt, x, params)
[K, bk, W, bw] = cl_nn_single_full.unwrap_vars(x, params);
switch params.activation
    case "tanh"
        Snew = tanh( Yt*K + bk ) * W + bw;
    case "relu"
        Snew = max(tanh( Yt*K + bk ) * W + bw, 0);
end
Snew = Snew - max(Snew,[],2);
Cnew = exp(Snew);
Cnew = Cnew ./ sum(Cnew, 2);
end
