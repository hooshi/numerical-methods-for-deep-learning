% NOTES:
% 1- Visualize the data. Espescially how the different colors are
% incorporated
% 2- Stick with gradient descent and L2 regularization.

%  Fold1, Train: 0.79 Test: 0.49 alpha=0.01, d_ratio=3

clear
close all

% setting all the path
run('utility.init_paths.m')
addpath( DIR_CIFAR10 )

% set the random seed
rng(0)

% Upload the data and set it for training
%  Each batch contains 10,000 samples. Number of features are 3072(3*32*32) 
Y = []; lb = [];
load data_batch_1.mat
Y = [Y; data]; lb = [lb; labels];
load data_batch_2.mat
Y = [Y; data];  lb = [lb; labels];    
load data_batch_3.mat
Y = [Y; data]; lb = [lb; labels];
load data_batch_4.mat
Y = [Y; data];  lb = [lb; labels];
load data_batch_5.mat
Y = [Y; data];  lb = [lb; labels];

% Split to training and testing
splitter = cval.Splitter( size(Y, 1), 2, true);

ind1 = splitter.get_indices(1);
Y1 = double(Y( ind1, :));
lb1 = lb( ind1 );

ind2 = splitter.get_indices(2);
Y2 = double(Y( ind2, :));
lb2 = lb( ind2 );

% transformation
d_ratio = 3;
activation_type = "tanh";
tr_nrm = transformation.Normalizer(Y1, true);
tr_rand =  transformation.RandomLayer(Y1, d_ratio, activation_type, true);
tr = transformation.Glue(tr_nrm, tr_rand);
tr = tr;
% tr = transformation.None();


% parameters for learning
params.random_guess = false;
params.optimizer='newton';
params.verbosity=1;
params.tr = tr;
params.alpha = 0.01;
params.optimizer_iters = 10;
params.regularization = "identity";

% Train
rg1 = cl_linear.LogisticRegression(Y1, lb1, params);
% rg2 = cl_linear.LogisticRegression(Y2, lb2, params);
C1 = rg1.Cobs;
C2 = rg1.label_tools.old_labels_to_proba_matrix(lb2);
% C2 = rg2.Cobs;

% Fold 1
train_accuracy_1 = metric.accuracy( rg1.predict(Y1), C1 );
test_accuracy_1  = metric.accuracy( rg1.predict(Y2), C2 );

% Fold 2
% train_accuracy_2 = metric.accuracy( rg2.predict(Y2), C2 );
% test_accuracy_2  = metric.accuracy( rg2.predict(Y1), C1 );

fprintf("Fold1, Train: %.2f Test: %.2f \n", train_accuracy_1, test_accuracy_1);
% fprintf("Fold2, Train: %.2f Test: %.2f \n", train_accuracy_2, test_accuracy_2);

% Save the stuff
% save('cifar10-params.obj', 'params')
% save("cifar10-classifier1.obj", 'rg1')
% save("cifar10-Y1", 'Y1')
% save("cifar10-Y2", 'Y2')
% save("cifar10-lb1", 'lb1')
% save("cifar10-lb2", 'lb2')
