import matplotlib.pyplot as plt
import numpy as np

#### ===================== CIFAR10 NN1FULL
v =  np.loadtxt('nn1full_CIFAR10_history_B5.txt', dtype=float)
plt.figure( figsize=(10,5) )
plt.subplot(1,2,1)
plt.semilogy(v[:,0], v[:,1], color='b', linewidth=2, label='nn')
plt.xlabel('Function evaluations')
plt.ylabel('Logistic loss')
plt.legend()

plt.subplot(1,2,2)
v = [2.30e+00,1.58e+00,1.53e+00,1.52e+00,1.52e+00,1.52e+00,1.52e+00,]
plt.semilogy(range(1,len(v)+1), v, color='g', linewidth=2, label='linear')

v = [2.30e+00, 9.63e-01,  8.38e-01, 7.95e-01]
plt.semilogy(range(1,len(v)+1), v, color='b', linewidth=2, label='elm, relu, dr=2')

v = [2.30e+00, 8.87e-01, 7.57e-01, 7.08e-01, 7.04e-01, 7.03e-01, 7.03e-01, 7.03e-01,]
plt.semilogy(range(1,len(v)+1), v, color='m', linewidth=2, label='elm, relu, dr=3')

v= [2.30e+00,8.69e-01,7.45e-01,6.97e-01,6.71e-01,6.63e-01,6.63e-01,6.63e-01,6.63e-01,]
plt.semilogy(range(1,len(v)+1), v, color='b', linestyle='--', linewidth=2, label='elm, tanh, dr=2')

v=[2.30e+00,6.48e-01,5.18e-01,4.65e-01,4.60e-01,4.17e-01,4.13e-01,4.13e-01,4.13e-01,]
plt.semilogy(range(1,len(v)+1), v, color='m', linestyle='--', linewidth=2, label='elm, tanh, dr=3')
plt.xlabel('Newton iterations')

plt.legend()
plt.savefig('_CIFAR10_history.pdf',bbox_inches='tight')
# plt.show()

#### ===================== MNIST NN1FULL
plt.clf()
v =  np.loadtxt('nn1full_MNIST_history.txt', dtype=float)
plt.figure( figsize=(10,5) )
plt.subplot(1,2,1)
plt.semilogy(v[:,0], v[:,1], color='b', linewidth=2, label='nn')
plt.xlabel('Function evaluations')
plt.ylabel('Logistic loss')
plt.legend()


plt.subplot(1,2,2)
v= [2.30e+00,4.86e-01,3.75e-01,3.27e-01,3.15e-01,3.12e-01,]
plt.semilogy(range(1,len(v)+1), v, color='r', linewidth=2, label='linear,laplacian regularized')
v=[2.30e+00,5.16e-01,4.74e-01,4.16e-01,4.06e-01,4.06e-01]
plt.semilogy(range(1,len(v)+1), v, color='g', linewidth=2, label='linear')


v= [2.30e+00,1.61e-01,9.89e-02,8.14e-02,5.33e-02,5.00e-02,]
plt.semilogy(range(1,len(v)+1), v, color='b', linewidth=2, label='elm, relu, dr=2')
v= [2.30e+00,1.35e-01,7.70e-02,5.33e-02,3.47e-02,3.24e-02,]
plt.semilogy(range(1,len(v)+1), v, color='m', linewidth=2, label='elm, relu, dr=3')

v=[2.30e+00,2.13e-01,1.77e-01,1.67e-01,1.60e-01,1.57e-01, ]
plt.semilogy(range(1,len(v)+1), v, color='b', linestyle='--', linewidth=2, label='elm, tanh, dr=2')
v=[2.30e+00,1.72e-01,1.40e-01,1.32e-01,1.26e-01,1.23e-01,]
plt.semilogy(range(1,len(v)+1), v, color='m', linestyle='--', linewidth=2, label='elm, tanh, dr=3')


plt.xlabel('Newton iterations')

plt.legend()
plt.savefig('_MNIST_history.pdf',bbox_inches='tight')
plt.show()

#### CIRCLES
plt.clf()
v =  np.loadtxt('nn1full_test_history_CIRCLES_tanh_GDMINIMIZE.txt', dtype=float)
plt.semilogy(v[:,0], v[:,1], color='b', linewidth=2, label='tanh-minimize')
v =  np.loadtxt('nn1full_test_history_CIRCLES_tanh_GDOURS.txt', dtype=float)
plt.semilogy(v[:,0], v[:,1], color='r', linewidth=2, label='tanh-vanilla')
v =  np.loadtxt('nn1full_test_history_CIRCLES_relu_GDMINIMIZE.txt', dtype=float)
plt.semilogy(v[:,0], v[:,1], color='g', linewidth=2, label='relu-minimize')

plt.xlabel('Function evaluations')
plt.ylabel('Logistic loss')
plt.gca().set_xlim([0, 10000])
plt.legend()
plt.savefig('_nn1full_CIRCLES_history.pdf',bbox_inches='tight')
# plt.show()

#### SPIRAL
plt.clf()
v =  np.loadtxt('nn1full_test_history_SPIRAL_tanh_GDOURS.txt', dtype=float)
plt.semilogy(v[:,0], v[:,1], color='g', linewidth=2, label='tanh-vanilla (20,000 iters)')
v =  np.loadtxt('nn1full_test_history_SPIRAL_relu_GDMINIMIZE.txt', dtype=float)
plt.semilogy(v[:,0], v[:,1], color='b', linewidth=2, label='relu-minimize (stopped)')
v =  np.loadtxt('nn1full_test_history_SPIRAL_tanh_GDMINIMIZE.txt', dtype=float)
plt.semilogy(v[:,0], v[:,1], color='r', linewidth=2, label='tanh-minimize')

plt.gca().set_xlim([0, 5000])

plt.xlabel('Function evaluations')
plt.ylabel('Logistic loss')
plt.legend();
plt.savefig('_nnfull_SPIRAL_history.pdf',bbox_inches='tight')
plt.show()

