import os

FILES = [
    'nn1full_test_history_CIRCLES_relu_GDMINIMIZE',
    'nn1full_test_history_CIRCLES_tanh_GDMINIMIZE',
    'nn1full_test_history_CIRCLES_tanh_GDOURS_10000',
    'nn1full_test_history_CIRCLES_tanh_GDOURS_5000',
    'nn1full_test_history_CIRCLES_tanh_GDOURS_2500',
    'nn1full_test_history_CIRCLES_tanh_GDOURS_1500',
    ###
    'linear_test_history_CIRCLES_relu_GDNEWTON',
    'linear_test_history_CIRCLES_tanh_GDNEWTON',
    ##
    'nn1full_test_history_SPIRAL_relu_GDMINIMIZE',
    'nn1full_test_history_SPIRAL_tanh_GDMINIMIZE',
    'nn1full_test_history_SPIRAL_tanh_GDOURS',
    ##
    'linear_test_history_SPIRAL_relu_GDNEWTON',
    'linear_test_history_SPIRAL_tanh_GDNEWTON',
]

for f in FILES:
    command = 'rsvg-convert -f pdf {0}.svg > ../img/{0}.pdf '.format(f)
    print command
    os.system(command)
