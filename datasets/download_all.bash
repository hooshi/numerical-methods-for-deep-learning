RM="rm -rf"

CIFAR10_PYTHON="https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz"
CIFAR10_MATLAB="https://www.cs.toronto.edu/~kriz/cifar-10-matlab.tar.gz"

MNIST_TRAIN_IMAGE="http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz"
MNIST_TRAIN_LABELS="http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz"
MNIST_TEST_IMAGE="http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz"
MNIST_TEST_LABELS="http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz"

##
## Download cifar-10
##
wget $CIFAR10_MATLAB
tar xzf cifar-10-matlab.tar.gz

##
## Download MNIST
##
pushd .
${RM} "mnist"
mkdir -p mnist
cd  mnist
wget $MNIST_TRAIN_IMAGE
wget $MNIST_TRAIN_LABELS
wget $MNIST_TEST_IMAGE
wget $MNIST_TEST_LABELS

find . -type f -exec gunzip -k {} \;
popd
