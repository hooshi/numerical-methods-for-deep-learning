function [X, param2] = sgdMomentum(X, dX, param)
%SGDMOMENTUM Summary of this function goes here
%   Detailed explanation goes here
param2 = param;

alpha = param.alpha;
mu = param.mu;

prev_dX = param.dX;
param2.dX = mu * prev_dX - alpha * dX;
X = X + param2.dX;

end

