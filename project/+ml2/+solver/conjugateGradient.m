function [ w, b ] = conjugateGradient( Y, c, w0, alpha, tol, itr_max )
%CONJUGATEGRADIENT [ w, b ] = conjugateGradient( Y, c )
%   Detailed explanation goes here

% First convert to Ax = b
Y_ext = [Y, ones(size(Y, 1), 1)];

A = Y_ext' * Y_ext;
A = A + alpha * eye(size(A));
B = Y_ext' * c;

r = B - A * w0;
d = r;
w = w0;
r_prev = r;
for i = 1:itr_max
   Ad = A * d;
   alpha =  (r' * r) / (d' * Ad);
   w = w + alpha * d;
   
   % Update residual with the real value to avoid floating point error.
   if mod(i, 50) ~= 0
       r = r_prev - alpha * Ad;
   else
       r = B - A * w;
   end
   
   beta = (r' * r) / (r_prev' * r_prev);
   d = r + beta * d;
   
   r_prev = r;
   
   if mod(i, 500) == 0
    fprintf('Itr %d: r - %f\n', i, norm(r));
   end
   
   if norm(r) < tol
       break
   end
end

b = w(end);
w = w(1:end-1);

end

