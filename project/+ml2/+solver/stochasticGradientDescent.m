function [X] = stochasticGradientDescent( obj, reg, Y, C, X0, ...
    annealFunc, updateFunc, param )
%STOCHASTICGRADIENTDESCENT [X] = stochasticGradientDescent( obj, reg, Y, C, X0, param )
%   Detailed explanation goes here

itr_max = param.itr_max;
batch_size = param.batch_size;
tol = param.tol;

% Annealing
epochs = param.epochs;

% Random
n = size(Y, 1);
indices = randperm(n);

X = X0;
dX = zeros(size(X));

prev_E = 0;

for itr=1:itr_max
    for i = 1:batch_size:n
        j = min(i + batch_size - 1, n);
        
        [energy, dF] = obj(Y(indices(i:j), :), C(indices(i:j), :), X);
        [r, dR, ~] = reg(X);
        
        [X, param] = updateFunc(X, (dF + dR), param);

        if i == 1
            fprintf('Itr %d: E: %e; norm(dX): %e;\n', ...
            itr, energy, norm(dF + dR));
        end
    end
    
    %% Updating rates
    if mod(itr, epochs) == 0
        param = annealFunc(param);
        
        %indices = randperm(n);
    end
    
    if norm(dF + dR) < tol || abs(energy - prev_E) < tol
       break;
    end
    prev_E = energy;
end

end

