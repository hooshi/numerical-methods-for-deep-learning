function [X, param2] = sgdAdam(X, dX, param)
%SGDADAM Summary of this function goes here
%   Detailed explanation goes here
param2 = param;

beta1 = param.beta1;
beta2 = param.beta2;
eps = param.eps;

t = param.t;
param2.t = t + 1;

param2.m = beta1*param.m + (1-beta1)*dX;
mt = param2.m / (1-beta1^t);
param2.v = beta2 * param.v + (1-beta2)*(dX.^2);
vt = param2.v / (1-beta2^t);
X = X - param.alpha * mt ./ (sqrt(vt) + eps);

end

