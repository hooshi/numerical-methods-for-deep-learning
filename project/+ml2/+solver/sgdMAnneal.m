function [param2] = sgdMAnneal(param)
%SGDMANNEAL Summary of this function goes here
%   Detailed explanation goes here
alpha_ratio = param.alpha_ratio;
alpha_min = param.alpha_min;
mu_ratio = param.mu_ratio;
mu_max = param.mu_max;

param2 = param;

param2.mu = param2.mu * mu_ratio;
param2.mu = min(mu_max, param2.mu);
param2.alpha = param2.alpha * alpha_ratio;
param2.alpha = max(param2.alpha, alpha_min);
        
end

