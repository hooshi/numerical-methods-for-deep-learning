function [ X ] = newton( obj, reg, X0, param )
%NEWTON [ X ] = newton( obj, reg, X0, param )
%   Detailed explanation goes here

% Read params
tol = param.tol;
itr_max = param.itr_max;
mu0 = param.mu0;
gamma = param.gamma;
cgtol = param.cgtol;
cgmax = param.cgmax;

X = X0;
M = [];

energy_loss = 1e3;

num_classes = size(X, 2);
num_dim = size(X, 1);
vec = @(V) V(:);
mat = @(v) reshape(v, num_dim, num_classes);
   
for i=1:itr_max
   [energy1, dF, hF] = obj(X);
   [r1, dR, hR] = reg(X);
   energy1 = energy1 + r1;
   
   hF_hR_vec = @(v) vec(hF(mat(v)) + hR * mat(v));
   
   [delta, ~] = pcg(hF_hR_vec, -dF(:)-dR(:), cgtol, cgmax, M);
   delta = mat(delta);
   
   % line search
   mu = mu0;
   for j=1:itr_max
      [energy2, ~, ~] = obj(X + mu * delta);
      [r1, ~, ~] = reg(X + mu * delta);
      energy2 = energy2 + r1;
      
      if energy2 < energy1
          energy_loss = energy1 - energy2;
          % no line search needed
          % increase mu for next itr
          if j == 1
             mu0 = gamma * mu;
          else
             mu0 = mu;
          end
          
          break;
      end
      mu = mu / 2;
   end
   
   X = X + mu * delta;
   
   if mod(i, 1) == 0
       fprintf('Itr %d: E: %f; norm(dW): %f; dE: %f\n', ...
        i, energy2, norm(mu * delta), energy_loss);
   end
   
   if norm(mu * delta) < tol || energy_loss < tol
       break;
   end
end

end

function min_example()

end