function [ X ] = conjugateGradientMulti( system, X0, tol, itr_max )
%CONJUGATEGRADIENT [ X ] = conjugateGradientMulti( system, X0, tol, itr_max )
%   A = system.A;
%   B = system.B;

A = system.A;
B = system.B;

r = B - A * X0;
d = r;
X = X0;
r_prev = r;
for i = 1:itr_max
   Ad = A * d;
   alpha = diag(r' * r) ./ diag(d' * Ad);
   X = X + d * diag(alpha);
   
   % Update residual with the real value to avoid floating point error.
   if mod(i, ceil(sqrt(size(A, 1)))) ~= 0
       r = r_prev - Ad * diag(alpha);
   else
       r = B - A * X;
   end
   
   beta = diag(r' * r) ./ diag(r_prev' * r_prev);
   d = r + d * diag(beta);
   
   r_prev = r;
   
   if mod(i, 10) == 0
    fprintf('Itr %d: r - %f\n', i, norm(r));
   end
   
   if norm(r) < tol
       break
   end
end

end

