function [X, param2] = sgdNesterov(X, dX, param)
%SGDNESTEROV Summary of this function goes here
%   Detailed explanation goes here
param2 = param;

mu = param.mu;
alpha = param.alpha;

v_prev = param.v;
param2.v = mu * param.v - alpha * dX;
X = X - mu * v_prev + (1 + mu) * param2.v;

end

