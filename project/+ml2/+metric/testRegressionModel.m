function [error, out_labels] = testRegressionModel(model, data, labels)
%TESTREGRESSIONMODEL [error, out_labels] = testRegressionModel(model, data, labels)
%   Detailed explanation goes here

Y = data;
W = model.W;
C_test = labels;

Snew = Y * W;
Snew = Snew - max(Snew,[],2);
Cnew = exp(Snew);
C = Cnew ./ sum(Cnew, 2);

C_label = max(C, [], 2);
C = bsxfun(@eq, C, C_label);

if nargout > 1
    out_labels = C;
end

C_error = abs(C - C_test);
error_count = sum(C_error, 2);
error = double(sum(error_count ~= 0)) / size(error_count, 1);

end

