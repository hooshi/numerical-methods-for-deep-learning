function [ error ] = testLinearModel( model, data, labels )
%TESTLINEARMODEL [ error ] = testLinearModel( model, data, labels )
%   model.W, model.b

W = model.W;
b = model.b;
Y = data;
C_test = labels;

C = [Y, ones(size(Y, 1), 1)] * [W; b'];

C_label = max(C, [], 2);
C = bsxfun(@eq, C, C_label);

C_error = abs(C - C_test);
error_count = sum(C_error, 2);
error = double(sum(error_count ~= 0)) / size(error_count, 1);

end

