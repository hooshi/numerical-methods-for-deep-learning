function [error, out_labels] = testRNNModel(model, data, labels)
%TESTRNNMODEL [error, out_labels] = testRNNModel(model, data, labels)
%   Detailed explanation goes here

[YN,~,~] = nn.rnnForward(data', model.P, model.K, model.b, model.act);
reg_model.W = model.W;
[error, out_labels] = metric.testRegressionModel(reg_model,...
    [YN', ones(size(data, 1), 1)], labels);

end

