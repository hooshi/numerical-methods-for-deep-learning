function [train_indices, test_indices] = foldIndices(num_folds, num_samples, cur_fold)
%FOLDINDICES [train_indices, test_indices] = foldIndices(num_fold, cur_fold)
%   Detailed explanation goes here

cur_fold = cur_fold - 1;
num_per_set = num_samples / num_folds;

test_indices = cur_fold * num_per_set + 1:(cur_fold+1) * num_per_set;
train_indices = 1:num_samples;
train_indices = train_indices(train_indices < cur_fold * num_per_set + 1 |...
    train_indices > (cur_fold+1) * num_per_set);

end

