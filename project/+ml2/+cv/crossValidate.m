function [model, accu] = crossValidate(num_folds, data_all, labels_all,...
    train_func, test_func)
%CROSSVALIDATE [model, accu] = crossValidate(num_folds, data_all, labels_all, train_func, test_func)
%   Detailed explanation goes here

min_error = 1;

for i=1:num_folds
   % Grab data
   [train_indices, test_indices] = cv.foldIndices(num_folds, ...
       size(data_all, 1), i);
   cur_train_data = data_all(train_indices, :);
   cur_train_labels = labels_all(train_indices, :);
   cur_test_data = data_all(test_indices, :);
   cur_test_labels = labels_all(test_indices, :);
   
   % Now train 
   model_temp = train_func(cur_train_data, cur_train_labels);

   % Compute error rate
   error = test_func(model_temp, cur_test_data, cur_test_labels);
   fprintf('CV: %f\n', 1 - error);
   
   if error < min_error
       min_error = error;
       model = model_temp;
   end
end

accu = 1 - min_error;
fprintf('Final CV: %f\n', 1 - min_error);

end

