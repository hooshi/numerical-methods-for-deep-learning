
% Load CIFAR10
data.loadCIFAR10;

% Debug
%data_all = data_all(1:1000, :);
%labels_all = labels_all(1:1000, :);
%

rng(57);

alpha = 1e-2;
tol = 1e-6;
max_itr = 5000;

larger_dim = 2 * data_dim;
K = randn(data_dim, larger_dim);
k_transfer = @(x) (max(x * K, 0));

trans_test_data = k_transfer(test_data);
trans_data_all = k_transfer(data_all);

% Build training function:
% model_temp = train_func(cur_train_data, cur_train_labels);
X0 = zeros(larger_dim + 1, num_classes);
linearTrain = @(data, labels) (model.toLinearModel(...
    solver.conjugateGradientMulti(...
    model.buildLinearSystem(...
    data ./ larger_dim, labels ./ larger_dim, alpha, @func.tikhonov),...
    X0, tol, max_itr)));
   
% 2-fold cross validation
num_folds = 2;
[model, accu] = cv.crossValidate(num_folds, trans_data_all, labels_all, ...
    linearTrain, ...
    @(m, d, l) (metric.testLinearModel(m, d, l)));

% Test
error = metric.testLinearModel(model, trans_test_data, test_labels);
fprintf('Test: %f\n', 1 - error);

