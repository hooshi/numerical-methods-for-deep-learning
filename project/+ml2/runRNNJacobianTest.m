clear

% Width of each block (including initial conditions)
n = [2  5   5   5   5   10; ...
     5  5   5   5  10   3];

 P = cell(1, size(n,2));
 K = cell(1, size(n,2));
 b = cell(1, size(n,2));
 
 for i=1:size(n,2)
    if n(1,i) ~= n(2,i)
        P{i} = func.opZero(n(2,i),n(1,i));
    else
        P{i} = func.opEye(n(1,i));
    end
    K{i} = randn(n(2,i),n(1,i));
    b{i} = randn(n(2,i),1);
end
N = length(P);
param.P = P;
Y1 = randn(2,100);
%% Run the NN forward
[Y,Yall,dA] = nn.rnnForward(Y1, P, K, b, @func.smoothRelU);

dY1 = randn(size(Y1))*0;
dK = cell(1, N);
K1 = cell(1, N);
db = cell(1, N);
b1 = cell(1, N);

for i=1:N 
    dK{i} = randn(size(K{i}))*1e-3;
    K1{i} = K{i} + dK{i};
    db{i} = randn(size(b{i})) * 1e-3;
    b1{i} = b{i} + db{i};
end
Y1 = nn.rnnForward(Y1+dY1, P, K1, b1, @func.smoothRelU);

dY = nn.rnnJacobian(dK, db, dY1, Yall, dA, P, K);

fprintf('=== Derivative test ========================\n')
fprintf('%3.2e  %3.2e\n',norm(Y1(:)-Y(:)), norm(Y1(:)-Y(:)-dY(:)))

fprintf('=== Adjoint test ========================\n')
dZ = nn.rnnJacobian(dK, db, dY1, Yall, dA, P, K);

dW = randn(size(dZ));
t1 = dZ(:)'*dW(:);

[dK1, db1] = nn.rnnJacobianT(dW, Yall, dA, P, K);

dk  = util.cell2vec(dK);
dk1 = util.cell2vec(dK1);
d  = util.cell2vec(db);
d1 = util.cell2vec(db1);

t2  = dk'*dk1 + d'*d1;
fprintf('%3.2e  %3.2e\n',t1,t2)
