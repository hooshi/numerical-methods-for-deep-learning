% CIFAR 10
train_set_names = {...
    '../data/CIFAR10/data_batch_1.mat',...
    '../data/CIFAR10/data_batch_2.mat',...
    '../data/CIFAR10/data_batch_3.mat',...
    '../data/CIFAR10/data_batch_4.mat',...
    '../data/CIFAR10/data_batch_5.mat'...
    };

if exist('data_all', 'var')
    clearvars -except data_all labels_all data_dim num_classes ...
        test_data test_labels train_set_names
    return;
end

%train_set_names = train_set_names(1);
data_dim = 32*32*3;
load('../data/CIFAR10/batches.meta.mat');

num_classes = size(label_names, 1);
num_sets = size(train_set_names, 2);

data_all = zeros(num_sets * 10000, data_dim);
labels_all = zeros(num_sets * 10000, num_classes);

% loading
load('../data/CIFAR10/test_batch.mat');
% convert to 0 - 1 floating color
data = double(data) / 255;
    
% parse labels
labels = double(labels);
labels_vec = zeros(10000, num_classes);
    
for j=1:num_classes
    labels_vec(labels == (j-1), j) = 1;
end
test_data = data;
test_labels = sparse(labels_vec);

for i=0:(num_sets-1)
    load(cell2mat(train_set_names(i+1)));

    % convert to 0-1 floating color
    data = double(data) / 255;
    
    % parse labels
    labels = double(labels);
    labels_vec = zeros(10000, num_classes);
    for j=1:num_classes
        labels_vec(labels == (j-1), j) = 1;
    end
    
    data_all(i * 10000 + 1:(i+1) * 10000, :) = data;
    labels_all(i * 10000 + 1:(i+1) * 10000, :) = labels_vec;
end

clearvars labels_vec labels data;
labels_all = sparse(labels_all);
