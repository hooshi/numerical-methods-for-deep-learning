function [ data_out ] = parseCifar10Data( data )
%PARSECIFAR10DATA Summary of this function goes here
%   Detailed explanation goes here

data_out = double(data) / 256;

data_cell = mat2cell(data_out, ones(1, size(data_out, 1)), ...
    3 * ones(1, size(data_out, 2) / 3));
data_cell = cellfun(@tolab, data_cell,'UniformOutput',false);
% rgb2lab
for i=1:size(data_out, 1)
   for j=1:size(data_out, 2)/3
       lab = rgb2lab(data_out(i, (j-1)*3 + 1 : j*3));
       data_out(i, (j-1)*3 + 1 : j*3) = (lab + [0, 128, 128])...
           ./ [100, 128*2, 128*2];
   end
end

data_out = data_out * 2 - 1;

end

function [ lab ] = tolab( rgb )
    lab = rgb2lab(rgb);
    lab = (lab + [0, 128, 128])...
           ./ [100, 128*2, 128*2];
end