
if exist('data_all', 'var')
    clearvars -except data_all labels_all data_dim num_classes ...
        test_data test_labels train_set_names
    return;
end

num_classes = 10;

test_data = data.loadMNISTImages('../data/mnist/t10k-images-idx3-ubyte')';
test_labels = data.loadMNISTLabels('../data/mnist/t10k-labels-idx1-ubyte');
test_labels = test_labels + 1;
test_labels = sparse(1:size(test_data, 1), test_labels, ...
    ones(size(test_data, 1), 1), size(test_data, 1), num_classes);

data_all = data.loadMNISTImages('../data/mnist/train-images-idx3-ubyte')';
labels_all = data.loadMNISTLabels('../data/mnist/train-labels-idx1-ubyte');
labels_all = labels_all + 1;
labels_all = sparse(1:size(data_all, 1), labels_all, ...
    ones(size(data_all, 1), 1), size(data_all, 1), num_classes);

data_dim = size(data_all, 2);
