
clear

DATASET = "SPIRAL";

%% Get data
switch DATASET
    case "LINEAR"
        [Y, labels] = dataset.make_linear(10000);
    case "SPIRAL"
        [Y, labels] = dataset.make_spiral();
    case "CIRCLES"
        [Y, labels] = dataset.make_circle(10000, [true, true, true]);
end

temp_labels = zeros(size(labels, 1), 2);
temp_labels(labels == 0, 1) = 1;
temp_labels(labels == 1, 2) = 1;
labels = temp_labels;

%% Build NN
num_classes = 2;
data_dim = size(Y, 2);
% circle
n_cir = [data_dim, data_dim; ...
      data_dim, data_dim * 8 ];
% spiral
n_sp = [data_dim, data_dim, data_dim*4, data_dim*8; ...
      data_dim, data_dim*4, data_dim*8, data_dim * 16 ];
  
n = n_sp;
P = cell(1, size(n,2));
Kb_size = 0;

for i=1:size(n,2)
   if n(1,i) ~= n(2,i)
       P{i} = func.opZero(n(2,i),n(1,i));
   else
       P{i} = func.opEye(n(1,i));
   end
   Kb_size = Kb_size + n(2,i)*n(1,i) + n(2,i);
end

r_alpha = 3.5e-4; %0 for circle
reg = @(X) (func.tikhonov( r_alpha, X ));

X0 = randn((n(2, end) + 1) * num_classes + Kb_size, 1);
%act = @func.smoothRelU;
act = @func.tanhG;

% circle
param_cir.alpha = 0.8;
param_cir.mu = 0.5;
param_cir.itr_max = 1000;
param_cir.batch_size = 100;
param_cir.tol = 0;%1e-6;

param_cir.epochs = 20;
param_cir.alpha_ratio = 0.9;
param_cir.alpha_min = 0.05;
param_cir.mu_ratio = 1.1;
param_cir.mu_max = 0.9;

% spiral
param_sp.alpha = 0.9;
param_sp.mu = 0.5;
param_sp.itr_max = 2000;
param_sp.batch_size = 200;
param_sp.tol = 0;%1e-6;

param_sp.epochs = 50;
param_sp.alpha_ratio = 0.9;
param_sp.alpha_min = 0.1;
param_sp.mu_ratio = 1.1;
param_sp.mu_max = 0.9;

param = param_sp;
objFunc = @(Y1, C, X) (nn.objRNN(Y1, C, X, n, P, act));
[X] = solver.stochasticGradientDescent(...
    objFunc, reg, Y, labels, X0, param);

%% Test accuracy
[model] = model.toRNNModel(X, P, act, n, num_classes);
[error, ~] = metric.testRNNModel(model, Y, labels);
fprintf('Test: %f\n', 1 - error);

%% Plot
cls = struct;
cls.predict = @(Yt) classifier.rnnClassify(model, Yt);

Yploty = Y; % params.tr.tr(Y);
hold off; metric.plot2d( cls, min(Yploty, [], 1), max(Yploty, [], 1));
hold on; plot( Yploty(labels(:, 1) == 1, 1), Yploty(labels(:, 1) == 1, 2), 'go');
hold on ; plot( Yploty(labels(:, 2) == 1, 1), Yploty(labels(:, 2) == 1, 2), 'ro');
