function [E, dE] = objRNN(Y1, C, X, n, P, act)
%OBJRNN Summary of this function goes here
%   Detailed explanation goes here

% Parse X
num_classes = size(C, 2);
num_dim = n(2, end) + 1;
W = reshape(X(1:num_classes * num_dim), num_dim, num_classes);

K = cell(1, size(n,2));
b = cell(1, size(n,2));
itr = num_classes * num_dim + 1;

for i=1:size(n,2)
    K{i} = reshape(X(itr:itr + n(2,i)*n(1,i) - 1), n(2,i), n(1,i));
    itr = itr + n(2,i)*n(1,i);
end
for i=1:size(n,2)
    b{i} = reshape(X(itr:itr + n(2,i) - 1), n(2,i),1);
    itr = itr + n(2,i);
end

%% Run the NN forward for YN and back-propagation
[YN,Yall,dA] = nn.rnnForward(Y1', P, K, b, act);

%% Compute d_W E
[ E, d_W, ~, dSE ] = func.softmax( [YN', ones(size(YN', 1), 1)], W, C );

%% Back propagate to compute d_K and d_b
% Drop the last row (d_b)
[d_K, d_b] = nn.rnnJacobianT(W(1:end-1, :) * dSE', Yall, dA, P, K);

dE = [d_W(:); util.cell2vec(d_K); util.cell2vec(d_b)];

end
