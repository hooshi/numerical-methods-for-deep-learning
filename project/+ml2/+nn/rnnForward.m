function [Yn, Yall, dA] = rnnForward(Y1, P, K, b, acc)
%RNNFORWARD [Yn] = rnnForward(Y0, P, K, b, acc)
%   Detailed explanation goes here

num_layer = length(P);
Yall = cell(1, num_layer + 1);
dA = cell(1, num_layer);
Yall{1} = Y1;

for i = 1:num_layer
    [A, dA{i}] = acc(K{i} * Yall{i} + b{i});
    Yall{i + 1} = P{i} * Yall{i} + A;
end

Yn = Yall{end};

end

