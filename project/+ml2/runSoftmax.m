
% Load CIFAR10
data.loadCIFAR10;

% Debug
%data_all = data_all(1:1000, :);
%labels_all = labels_all(1:1000, :);
%

rng(57);

larger_dim = 2 * data_dim;
K = randn(data_dim, larger_dim);
k_transfer = @(x) (max(x * K, 0));

trans_test_data = k_transfer(test_data);
trans_data_all = k_transfer(data_all);

% Build training function:
% model_temp = train_func(cur_train_data, cur_train_labels);
X0 = zeros(larger_dim + 1, num_classes);
param.gamma = 1.5;
param.mu0 = 1;
param.tol = 1e-3;
param.itr_max = 50;
param.cgtol = 1e-1;
param.cgmax = 10;
alpha = 1e-3;
reg = @(X) (func.tikhonov( alpha, X ));
   
logisticRegressionTrain = @(data, labels) (model.toLinearModel(...
    solver.newton(...
    @(X) (func.softmax([data, ones(size(data, 1), 1)], X, labels )),...
    reg, X0, param)));
   
% 2-fold cross validation
num_folds = 2;
[model, accu] = cv.crossValidate(num_folds, trans_data_all, labels_all, ...
    logisticRegressionTrain, ...
    @(m, d, l) (metric.testLinearModel(m, d, l)));

% Test
error = metric.testLinearModel(model, trans_test_data, test_labels);
fprintf('Test: %f\n', 1 - error);

