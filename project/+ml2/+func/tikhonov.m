function [ R, dR, d2R ] = tikhonov( alpha, W, L )
%TIKHONOV [ R, dR, d2R ] = tikhonov( alpha, W)
%   Detailed explanation goes here

if nargin == 2 && isinteger(W)
    % Should be L'*L here but we know it's an eye
    L = speye(W);
    R = 0.5 * alpha * L;
    return;
elseif nargin == 2
    L = speye(size(W, 1));
end

LW = L * W;

R = 0.5 * alpha * (LW(:)' * LW(:));

if nargout > 1
    dR = alpha * L' * LW;
    d2R = alpha * (L' *L);
end

end

