function [ E, dE, d2E, dSE ] = softmax( Y, X, C_obs )
%SOFTMAX [ E, dE, d2E ] = softmax( Y, X, C_obs )
%   Detailed explanation goes here

n = size(Y,1);

S = Y * X;
s = max(S, [], 2);
num_c = size(S, 2);
S = bsxfun(@minus, S, s);

exp_S = exp(S);
sS   = sum(exp_S, 2);

E = -C_obs(:)'*S(:) +  sum(log(sS)); 
E = E / n;

if nargout > 1
    
    sum_exp_S = repmat(sum(exp_S, 2), 1, num_c);
    
    dE = Y' * (-C_obs + exp_S ./ sum_exp_S) / n;

    E1 = @(V) exp_S ./ sum_exp_S .* (Y * V);
    E2 = @(V) exp_S .* ((1 ./ (sum_exp_S.^2)) .* ...
        repmat(sum(exp_S .* (Y * V), 2), 1, num_c));

    d2E = @(V) Y' * (E1(V) - E2(V)) / n;
    
    if nargout > 3
        dSE = (-C_obs + exp_S ./ sum_exp_S) / n;
    end
end

end
