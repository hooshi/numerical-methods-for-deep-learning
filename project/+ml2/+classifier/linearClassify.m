function [C] = linearClassify(model,data)
%LINEARCLASSIFY Summary of this function goes here
%   Detailed explanation goes here
W = model.W;
b = model.b;
Y = data;

C = [Y, ones(size(Y, 1), 1)] * [W; b'];
end

