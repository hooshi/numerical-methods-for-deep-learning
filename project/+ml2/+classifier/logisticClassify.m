function [labels] = logisticClassify(model, data)
%LOGISTICCLASSIFY Summary of this function goes here
%   Detailed explanation goes here

Y = data;
W = model.W;

Snew = Y * W;
Snew = Snew - max(Snew,[],2);
Cnew = exp(Snew);
C = Cnew ./ sum(Cnew, 2);

C_label = max(C, [], 2);
labels = bsxfun(@eq, C, C_label);

end

