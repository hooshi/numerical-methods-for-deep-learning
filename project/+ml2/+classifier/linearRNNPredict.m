function [pred] = linearRNNPredict(model, data)
%LINEARRNNPREDICT Summary of this function goes here
%   Detailed explanation goes here

[YN,~,~] = ml2.nn.rnnForward(data', model.P, model.K, model.b, model.act);
[linear_model] = ml2.model.toLinearModel(model.W);
pred = ml2.classifier.linearClassify(linear_model, YN');
end

