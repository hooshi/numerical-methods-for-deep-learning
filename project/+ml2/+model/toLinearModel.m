function [model] = toLinearModel(W)
%TOLINEARMODEL [model] = toLinearModel(W)
model.b = W(end, :)';
model.W = W(1:end-1, :);

end

