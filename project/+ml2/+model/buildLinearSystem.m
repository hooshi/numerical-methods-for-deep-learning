function [system] = buildLinearSystem(data, labels, alpha, reg)
%BUILDLINEARSYSTEM [system] = buildLinearSystem(data, labels, alpha, reg)

if nargin == 1
    min_example();
    return;
end

    %Convert to Ax = b with regularization
    Y_ext = [data, ones(size(data, 1), 1)];

    A = Y_ext' * Y_ext;
    A = A + reg(alpha, size(A, 2));
    B = Y_ext' * labels;
    
    system.A = A; system.B = B;
end

function min_example()

end

