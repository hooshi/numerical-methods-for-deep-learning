function [model] = toRNNModel(X, P, act, n, num_classes)
%TORNNMODEL [model] = toRNNModel(X, P, act, n, num_classes)
%   Detailed explanation goes here

model.P = P;
model.act = act;

num_dim = n(2, end) + 1;
model.W = reshape(X(1:num_classes * num_dim), num_dim, num_classes);

K = cell(1, size(n,2));
b = cell(1, size(n,2));
itr = num_classes * num_dim + 1;

for i=1:size(n,2)
    K{i} = reshape(X(itr:itr + n(2,i)*n(1,i) - 1), n(2,i), n(1,i));
    itr = itr + n(2,i)*n(1,i);
end
for i=1:size(n,2)
    b{i} = reshape(X(itr:itr + n(2,i) - 1), n(2,i),1);
    itr = itr + n(2,i);
end

model.K = K;
model.b = b;

end

