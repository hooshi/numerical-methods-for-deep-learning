
% Load CIFAR10
data.loadMNIST;

% Debug
%data_all = data_all(1:5000, :);
%labels_all = labels_all(1:5000, :);
%

rng(57);

% Width of each block (including initial conditions)
dim_per_ch = data_dim/3;
n = [data_dim  ; ...
     dim_per_ch * 3 ];

P = cell(1, size(n,2));
Kb_size = 0;

for i=1:size(n,2)
   if n(1,i) ~= n(2,i)
       P{i} = func.opZero(n(2,i),n(1,i));
   else
       P{i} = func.opEye(n(1,i));
   end
   Kb_size = Kb_size + n(2,i)*n(1,i) + n(2,i);
end

r_alpha = 1e-2;
reg = @(X) (func.tikhonov( r_alpha, X ));

X0 = randn(n(2, end) * num_classes + Kb_size, 1);
param.alpha = 0.1;
param.mu = 0.5;
param.itr_max = 30;
param.batch_size = 5000;
param.tol = 1e-6;

objFunc = @(Y1, C, X) (nn.objRNN(Y1, C, X, n, P, @func.smoothRelU));
[X] = solver.stochasticGradientDescent(...
    objFunc, reg, data_all, labels_all, X0, param);

%% Test accuracy
[model] = model.toRNNModel(X, P, act, n, num_classes);
[error, ~] = metric.testRNNModel(model, test_data, test_labels);
fprintf('Test: %f\n', 1 - error);

return

%% old test
num_classes = size(labels_all, 2);
num_dim = n(2, end);
W = reshape(X(1:num_classes * num_dim), num_dim, num_classes);

K = cell(1, size(n,2));
b = cell(1, size(n,2));
itr = num_classes * num_dim + 1;

for i=1:size(n,2)
    K{i} = reshape(X(itr:itr + n(2,i)*n(1,i) - 1), n(2,i), n(1,i));
    itr = itr + n(2,i)*n(1,i);
end
for i=1:size(n,2)
    b{i} = reshape(X(itr:itr + n(2,i) - 1), n(2,i),1);
    itr = itr + n(2,i);
end
[YN,Yall,dA] = nn.rnnForward(test_data', P, K, b, @func.smoothRelU);
model.Y = YN';
model.W = W;
[error, ~] = metric.testRegressionModel(model, test_labels);
fprintf('Test: %f\n', 1 - error);
