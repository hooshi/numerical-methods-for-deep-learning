close all
clear 

ctx = neuroanim.Context();
ctx.read_file('training_data/l2_c_0101_train0001.mat');
[Y,C] = ctx.training_data();
C(1:20,:)

dt = ctx.dt;
nt = 1000;
t = double(0:1:nt)*dt;
pdlm = ctx.pdlm;
n_links = pdlm.n_links;
num_step = 3;
%energyFunc = @control.energyEntire;
energyFunc = @control.energyAllTime;
tau0 = 2 * randn(n_links * num_step, 1);
epsilon = 1e-4;

p0 = [pi/2 -pi/8  0];
q0 = [0 0 0];
vec = @(xx) xx(:);

p_obj = [0 0 0];
q_obj = [0 0 0];
objs.S = cell(2, 1);
objs.t = [2; 3];
Sobj = vec([p_obj(1:n_links); q_obj(1:n_links)]');
objs.S{1} = Sobj;
objs.S{2} = Sobj;

init_v = vec([p0(1:n_links); q0(1:n_links); zeros(1, n_links)]');
init_v = init_v';

[E, dE] = energyFunc(tau0, objs, ctx.regressor.model, init_v(1:pdlm.n_dof()));

dE_finite = zeros(size(dE));
for i=1:size(dE, 1)
    e = eye(size(dE, 1));
    e = e(:, i);
    [Epos, ~] = energyFunc(tau0 + epsilon * e, ...
        objs, ctx.regressor.model, init_v(1:pdlm.n_dof()));
    [Eneg, ~] = energyFunc(tau0 - epsilon * e, ...
        objs, ctx.regressor.model, init_v(1:pdlm.n_dof()));
    dE_finite(i) = (Epos - Eneg) / (epsilon * 2);
end

fprintf("epsilon: %e\n", epsilon);
fprintf("dE:   %s;\ndE_f: %s\n", sprintf('%.8f ', dE'), ...
    sprintf('%.8f ', dE_finite));
