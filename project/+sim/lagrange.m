function [M, f] = lagrange(pdl, p, q, tau)
% function [M, f] = dyn_system(pdl, p, q, tau)

n = pdl.n_links;


m1 = pdl.M(1);
m2 = pdl.M(2);
m3 = pdl.M(3);

p1 = get_entry(p,1);
p2 = get_entry(p,2);
p3 = get_entry(p,3);

q1 = get_entry(q,1);
q2 = get_entry(q,2);
q3 = get_entry(q,3);

l1 = get_entry(pdl.L,1);
l2 = get_entry(pdl.L,2);
% l3 = pdl.L(3);

lt1 = get_entry(pdl.Lt,1);
lt2 = get_entry(pdl.Lt,2);
lt3 = get_entry(pdl.Lt,3);

I1 = get_entry(pdl.I,1);
I2 = get_entry(pdl.I,2);
I3 = get_entry(pdl.I,3);

tau1 = get_entry(tau,1);
tau2 = get_entry(tau,2);
tau3 = get_entry(tau,3);

c1 = get_entry(pdl.c,1);
c2 = get_entry(pdl.c,2);
c3 = get_entry(pdl.c,3);

g = pdl.g;

%
% MASS MATRIX
%
M = zeros(3,3);
M(1,1)= m1*lt1*lt1 + (m2+m3)*l1*l1 + I1 + I2 + I3;
M(1,2)= (m2*l1*lt2 + m3*l1*l2)*cos(p1-p2) + I2 + I3;
M(1,3)= (m3*l1*lt3)*cos(p1-p3) + I3;
%
M(2,1)= (m2*l1*lt2 + m3*l1*l2)*cos(p2-p1) + I2 + I3;
M(2,2)= m2*lt2*lt2 + m3*l2*l2 + I2 + I3;
M(2,3)= m3*l2*lt3*cos(p2-p3) + I3;
% 
M(3,1)= m3*l1*lt3*cos(p3-p1) + I3;
M(3,2)= m3*l2*lt3*cos(p2-p3) + I3;
M(3,3)= m3*lt3*lt3 + I3;

%
% FORCE VECTOR
%
f = zeros(3,1);
f(1)= q2*q2 * sin(p2-p1) * (m2*l1*lt2 + m3*l1*l2) + q3*q3 * sin(p3-p1) * (m3*l1*lt3);
f(2)= q1*q1 * sin(p1-p2) * (m2*l1*lt2 + m3*l1*l2) + q3*q3 * sin(p3-p2) * (m3*l2*lt3);
f(3)= q1*q1 * sin(p1-p3) * (m3*l1*lt3           ) + q2*q2 * sin(p2-p3) * (m3*l2*lt3);
f(1)= f(1) - g*(m1*lt1+m2*l1+m3*l1)*sin(p1);
f(2)= f(2) - g*(m2*lt2+m3*l2)*sin(p2);
f(3)= f(3) - g*(m3*lt3)*sin(p3);

%
% External moments (controls)
%
f(1) = f(1) + tau1 - c1*q1;
f(2) = f(2) + tau2 - c2*q2;
f(3) = f(3) + tau3 - c3*q3;

M = M(1:n,1:n);
f = f(1:n);
end

function [out] = get_entry(vv, ii)

if (numel(vv) >= ii) out = vv(ii);
else                 out = 0;
end
end