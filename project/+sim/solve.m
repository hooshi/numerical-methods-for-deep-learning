function [tout, Yout] = solve(Y0, tspan, tau_fn, pdl)
% function [tout, Yout] = solve(Y0, tspan, tau, pendulum)
% orders are p, q

Y0 = Y0(:);

if(isempty(tau_fn)) tau_fn = @(~) zeros(size(pdl.n_dof())); end;

options = odeset('RelTol', 1e-8, 'AbsTol', 1e-12); 
rhs_wrapper = @(t_, Y_) rhs_function(t_, Y_, tau_fn, pdl);
[tout, Yout] = ode45(rhs_wrapper, tspan, Y0,options);
% rhs_wrapper(Y0, 0)

end

function dY = rhs_function(t, Y, tau_fn, pdl)
n = pdl.n_links;
p = Y(1:n);
q = Y(n+1:2*n);
%fprintf("%f \n",t);
%tau = zeros(n,1); % change this
tau = tau_fn(t);

[M, f] = sim.lagrange(pdl, p, q, tau);
dY = zeros(2*n,1);
dY(1:n) = q;
dY(n+1:2*n) = M\f;
end
