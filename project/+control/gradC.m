function [CT, dCT] = gradC(nn_model, s, tau)
%GRADC Summary of this function goes here
%   Detailed explanation goes here

%% Create Y1 with the current tau
Y1 = [s(:); tau(:)]';
W = nn_model.W(1:end-1, :); % drop b
b = nn_model.W(end, :);

%% Compute dY1T_YnT * W (dYnT_CT)
[Yn, dY1CT] = control.jacobianTY1Yn(nn_model, Y1, W);

CT = Yn' * W + b;
dCT = dY1CT;

%fprintf("Y1: %s; Yn: %s; CT: %s; dCT: %s \n", sprintf('%.6f ', Y1), sprintf('%.6f ', Yn'), ...
%    sprintf('%.6f ', CT), sprintf('%.6f ', dCT));

end
