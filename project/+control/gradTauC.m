function [CT, dCT] = gradTauC(nn_model, s, tau)
%GRADC [E, dE] = gradTauC(nn_model, tau)
%   Detailed explanation goes here

%% Create Y1 with the current tau
Y1 = [s, tau'];
W = nn_model.W(1:end-1, :); % drop b
b = nn_model.W(end, :);

%% Compute dY1T_YnT * W (dYnT_CT)
[Yn, dY1CT] = control.jacobianTY1Yn(nn_model, Y1, W);

CT = Yn' * W + b;

tau_len = size(tau, 1);
dCT = dY1CT(end-(tau_len-1):end, :);

%fprintf("Y1: %s; Yn: %s; CT: %s; dCT: %s \n", sprintf('%.6f ', Y1), sprintf('%.6f ', Yn'), ...
%    sprintf('%.6f ', CT), sprintf('%.6f ', dCT));

end

