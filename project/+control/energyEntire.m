function [E, dE] = energyEntire(tau, Cobj, nn_model, Y1, ratio)
%ENERGYENTIRE Summary of this function goes here
%   Detailed explanation goes here

if ~exist('ratio','var') || isempty(ratio)
    ratio = 1;
end

[CT, dCT] = control.gradTauC(nn_model, Y1, tau);

E = ratio * 0.5 * norm(CT - Cobj')^2;
dE = ratio * dCT * (CT' - Cobj);

% fprintf("E: %.6f; dE: %s; tau: %s\n", E, sprintf('%.6f ', dE'), ...
%     sprintf('%.6f ', tau'));

end
