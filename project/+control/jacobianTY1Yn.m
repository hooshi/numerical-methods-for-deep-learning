function [Yn, dY1Yn] = jacobianTY1Yn(nn_model, Y1, V0)
%JACOBIANTY1YN Summary of this function goes here
%   Detailed explanation goes here

V = V0;
[Yn, ~, dA] = ml2.nn.rnnForward(Y1', nn_model.P, nn_model.K, ...
    nn_model.b, nn_model.act);
for i=length(nn_model.P):-1:1
    dY1Yn = nn_model.P{i}' * V + nn_model.K{i}' * (dA{i} .* V);
    V = dY1Yn;
end

end

