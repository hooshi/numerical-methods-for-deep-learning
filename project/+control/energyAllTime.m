function [E, dE] = energyAllTime(Tau, objs, nn_model, s0, ratio)
%ENERGYPOSITION [E, dE] = energyAllTime(Tau, Sobj, nn_model, s0, ratio)
%   Detailed explanation goes here

if ~exist('ratio','var') || isempty(ratio)
    ratio = 1;
end

tau_len = size(nn_model.K{1}, 2) - size(objs.S{1}, 1);
num_step = max(objs.t);

%% Collect all dsi_Ci+1, dtaui_Ci+1
dsC = cell(num_step, 1);
dtauC = cell(num_step, 1);
s_Sobj = cell(length(objs.t), 1);
s_Sobj_sum = 0;

s = s0(:);
s = s';
for i=1:num_step
    tau = Tau((i-1)*tau_len + 1:i*tau_len);
    [CT, dCT] = control.gradC(nn_model, s, tau);
    
    dsC{i} = dCT(1:end-tau_len, :);
    dtauC{i} = dCT(end-(tau_len-1):end, :);
    
    s = s + CT;
    
    if ismember(i, objs.t)
        ind = find(objs.t==i);
        s_Sobj{ind} = s - objs.S{ind}';
        s_Sobj_sum = s_Sobj_sum + norm(s_Sobj{ind})^2;
    end
end

E = ratio * 0.5 * s_Sobj_sum;

%% Compute dtau_E for each time step by chain rule
dE = zeros(size(Tau));
for i=1:num_step   
   for k = 1:length(objs.t)
       end_step = objs.t(k);
       
       if end_step < i
           continue;
       end
       
       dtau_S =  dtauC{i}; % should also *sigma from normalization
       for j=i+1:end_step
           dtau_S = dtau_S * (dsC{j} + eye(size(dsC{j})));
       end
       dE((i-1)*tau_len + 1:i*tau_len) = dE((i-1)*tau_len + 1:i*tau_len) + ...
           ratio * dtau_S * (s_Sobj{k})';
   end
end

% fprintf("E: %.6f; dE: %s; tau: %s\n", E, sprintf('%.6f ', dE'), ...
%     sprintf('%.6f ', tau'));

end

