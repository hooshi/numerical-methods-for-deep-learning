function [tau] = solve(Cobj, nn_model, tau0, s1, energy, reg, itr_max)
%SOLVE [tau] = solve(Cobj, nn_model, Y1, func)
%   Detailed explanation goes here

objFunc = @(tau, varargin) (combineFunc(tau, energy.func, reg, varargin));
tau = ml.optimization.minimize(tau0, objFunc, itr_max, [], ...
    Cobj, nn_model, s1, energy.ratio);

end

function [E, dE] = combineFunc(tau, func, reg, varargin)
    varargin = varargin{:};
    [f, df] = feval(func, tau, varargin{:});
    [r, dr] =  reg(tau);
    E = f + r;
    dE = df + dr;
end
