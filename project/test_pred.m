close all
clear 

ctx = neuroanim.Context();
ctx.read_file('training_data/l3_c_0201_train0003.mat');
%ctx.read_file('training_data/l2_c_0101_train0002.mat');
vec = @(xx) xx(:);

dt = ctx.dt;
nt = 1000;
t = double(0:1:nt)*dt;
pdlm = ctx.pdlm;
n_links = pdlm.n_links;

p0 = [0 0 0];
q0 = [0 0 0];
tau_fn = @(t) [5*cos(t);0;sin(t)]; %@(t) 2 * cos(t) * ones(1, n_links) ;%[0.2*sin(t); 0; 0];
[~, Yode] = sim.solve([p0(1:n_links);  q0(1:n_links)]', t, tau_fn, pdlm);


if ~isfield(ctx.regressor,'params')
    ctx.regressor.params.n_d1 = size(ctx.regressor.model.K{1}, 2);
end

Ynn = zeros(nt, ctx.regressor.params.n_d1);
init_v = [vec(p0(1:n_links)); vec(q0(1:n_links)); vec(tau_fn(0))];
Ynn(1, :) = init_v(1:ctx.regressor.params.n_d1);

% no control
%Ynn = zeros(nt, ctx.regressor.params.n_d1);
%Ynn(1, :) =  [vec(p0(1:n_links)); vec(q0(1:n_links))];

for i=2:nt
    Ynn_v = [vec(Ynn(i-1,1:pdlm.n_dof())); vec(tau_fn((i-2) * dt)) ]';
    Ynn(i-1, 1:ctx.regressor.params.n_d1) = Ynn_v(1:ctx.regressor.params.n_d1);
    Ynn(i,1:pdlm.n_dof()) = ...
    ctx.predict(Ynn_v(1:ctx.regressor.params.n_d1));

    % no control
    %Ynn(i,:) = ctx.predict(Ynn(i-1,:));
    
    % Yscaled = ctx.animation_scaler.scale(Ynn(i-1, :), []);
    % C = ctx.regressor.predict(Yscaled);
    % C2 = ctx.animation_scaler.unscale_C(Ynn(i-1, :), C)
    % C2 = ctx.animation_scaler.std_scaler_C.unscale(C);
    % Ynn(i, :) = C2 + Ynn(i-1, :);
end

save('/home/hooshi/anim-no-u-ode2.mat','Yode');
save('/home/hooshi/anim-no-u-nndeep.mat','Ynn');

for i=1:1:nt
    prange = 1:(pdlm.n_p());
    qrange  = (pdlm.n_p()+1):(pdlm.n_dof());
    p = Yode( i , 1:(pdlm.n_links) );
    hold off, pdlm.draw(p(:), [0 0]);
    %
    p = Ynn( i , 1:(pdlm.n_links) );
    hold on, pdlm.draw(p(:), [0 0]);
    %
    fprintf("p,q: %s | %s \n", sprintf("%f ",Yode(i, :)), sprintf("%f ",Ynn(i, :)) );
    title( sprintf("t = %f ",  t(i)) );
    pause(0.05);
end
