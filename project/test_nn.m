close all
clear 

d = load('training_data/basisData.mat');

Ytrain =  d.data.X;
Ctrain =  d.data.y;
Ytest =   d.data.Xtest;
Ctest =   d.data.ytest;

scaler_Y = neuroanim.StandardScaler();
scaler_C = neuroanim.StandardScaler();
%scaler_Y = neuroanim.NoScaler();
%scaler_C = neuroanim.NoScaler();
scaler_Y.fit( Ytrain );
scaler_C.fit( Ctrain );
 
% No scaling
%REGULARIZATION_RATIO_K = 1e-2;
%REGULARIZATION_RATIO_BK = 0;
%REGULARIZATION_RATIO_W = 1e-2;
%REGULARIZATION_RATIO_BW = 0;

% with scaling
REGULARIZATION_RATIO_K = 1e-8;
REGULARIZATION_RATIO_BK = 0;
REGULARIZATION_RATIO_W = 1e-8;
REGULARIZATION_RATIO_BW = 0;

% classifier
RATIO_HIDDEN_UNITS = 40;
ACTIVATION = 'srelu';
MAX_OPTIMIZATION_ITERATIONS = 50000;
 
nn_params = ml.nn_single.NNSingleParams();
nn_params.n_d1 = size(Ytrain, 2);
nn_params.n_d2 = nn_params.n_d1 * RATIO_HIDDEN_UNITS;
nn_params.n_c = size(Ctrain, 2);
nn_params.activation_type = ACTIVATION;
nn_params.lambda_K = REGULARIZATION_RATIO_K;
nn_params.lambda_bk = REGULARIZATION_RATIO_BK;
nn_params.lambda_W = REGULARIZATION_RATIO_W;
nn_params.lambda_bw = REGULARIZATION_RATIO_BW;
nn = ml.nn_single.NNSingle(nn_params);

x0 = randn( ml.nn_single.n_vars(nn.params), 1) ; 
n_iters = -MAX_OPTIMIZATION_ITERATIONS;
% x = optimization.minimize(x0, @objective_for_minimize, n_iters, historyfile, Y, Cobs, params);
x = ml.optimization.minimize(x0, @nn.objective, n_iters, [], scaler_Y.scale(Ytrain) , scaler_C.scale(Ctrain) );
nn.set_x(x);

Ctrainpred = scaler_C.unscale( nn.predict( scaler_Y.scale(Ytrain) ) );
Ctestpred = scaler_C.unscale( nn.predict( scaler_Y.scale(Ytest) ) );

% ctx.set_regressor( nn );
% ctx.dump_file('training_data/s1_nc_0002.mat')

fprintf("Accuracy on test: %.8f  \n", norm(Ctrain-Ctrainpred, 'fro')/ sqrt(size(Ytrain, 1)) );
fprintf("Accuracy on train: %.8f \n", norm(Ctest-Ctestpred, 'fro')/sqrt(size(Ytest, 1))   )

y = linspace(min(Ytrain), max(Ytrain), 1000);
y = y(:);
hold off, plot(Ytrain, Ctrain, 'mo');
hold on, plot(Ytest, Ctest, 'ro');
hold on, plot(y, scaler_C.unscale( nn.predict( scaler_Y.scale(y) ) ), 'k-', 'linewidth', 3);
