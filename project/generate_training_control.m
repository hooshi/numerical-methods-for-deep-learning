clear
close all
restoredefaultpath

%% =========================== 1 LINK PENDULUM
n_links = 1;     % 1 link
M = [1 0 0];     % mass of the link is 1
L = [1 0 0];     % the length of the link is 1
Lt = [1, 0, 0];  % the center of mass is located at length of 1
g = 10;          % acceleration of gravity
I = [0,0,0];     % Moment of inertial of each link ( 0 means that the mass is concentrated)


dt = 0.05;           % NN time step
n_data = 10000;      % Number of training samples
angle_range = pi;    % Full space
max_deriv_ratio = 10; % Can handle derivatives up to 10*angle_range
tau_range = 10;       % Tau varies within [-10, 10] and remains const for the full time step

% no damping
c  = [0, 0, 0];
pdl = sim.Pendulum(n_links, M, I, L, Lt, g, c);
[Y, C] = neuroanim.training_data_control(pdl, n_data, dt, max_deriv_ratio, angle_range, tau_range);
ctx = neuroanim.Context();
ctx.init(pdl, Y, C, dt, max_deriv_ratio, angle_range, tau_range);
ctx.dump_file('training_data/l1_c_0001.mat');


% with damping 
c  = [1, 0, 0];
pdl = sim.Pendulum(n_links, M, I, L, Lt, g, c);
[Y, C] = neuroanim.training_data_control(pdl, n_data, dt, max_deriv_ratio, angle_range, tau_range);
ctx = neuroanim.Context();
ctx.init(pdl, Y, C, dt, max_deriv_ratio, angle_range, tau_range);
ctx.dump_file('training_data/l1_c_0101.mat');

%% =========================== 2 LINK PENDULUM
n_links = 2;     % 2 links
M = [1 1 0];     % mass of the link is 1
L = [1 1 0];     % the length of the link is 1
Lt = [1, 1, 0];  % the center of mass is located at length of 1
g = 10;          % acceleration of gravity
I = [0,0,0];     % Moment of inertial of each link ( 0 means that the mass is concentrated)


% =========== No damping
% ===========
c = [0,0,0];

% === Same as 1 link
dt = 0.05;           
n_data = 10000;     
angle_range = pi;    
max_deriv_ratio = 2;
pdl = sim.Pendulum(n_links, M, I, L, Lt, g, c);
[Y, C] = neuroanim.training_data_control(pdl, n_data, dt, max_deriv_ratio, angle_range, tau_range);
ctx = neuroanim.Context();
ctx.init(pdl, Y, C, dt, max_deriv_ratio, angle_range, tau_range);
ctx.dump_file('training_data/l2_c_0001.mat');


% === Reduce the domain of angles on which we train
%     This seemed to be working well.
max_deriv_ratio = 2;
dt = 0.05;
n_data = 10000;
angle_range = pi/4;

pdl = sim.Pendulum(n_links, M, I, L, Lt, g, c);
[Y, C] = neuroanim.training_data_control(pdl, n_data, dt, max_deriv_ratio, angle_range, tau_range);
ctx = neuroanim.Context();
ctx.init(pdl, Y, C, dt, max_deriv_ratio, angle_range, tau_range);

ctx.dump_file('training_data/l2_c_0002.mat');

% =========== With damping
% ===========
c = [1,1,0];

% === Same as 1 link
dt = 0.05;           
n_data = 10000;     
angle_range = pi;    
max_deriv_ratio = 2;
pdl = sim.Pendulum(n_links, M, I, L, Lt, g, c);
[Y, C] = neuroanim.training_data_control(pdl, n_data, dt, max_deriv_ratio, angle_range, tau_range);
ctx = neuroanim.Context();
ctx.init(pdl, Y, C, dt, max_deriv_ratio, angle_range, tau_range);
ctx.dump_file('training_data/l2_c_0101.mat');


% === Reduce the domain of angles on which we train
%     This seemed to be working well.
max_deriv_ratio = 2;
dt = 0.05;
n_data = 10000;
angle_range = pi/4;

pdl = sim.Pendulum(n_links, M, I, L, Lt, g, c);
[Y, C] = neuroanim.training_data_control(pdl, n_data, dt, max_deriv_ratio, angle_range, tau_range);
ctx = neuroanim.Context();
ctx.init(pdl, Y, C, dt, max_deriv_ratio, angle_range, tau_range);

ctx.dump_file('training_data/l2_c_0102.mat');

%% =========================== 3 LINK PENDULUM
n_links = 3;     % 3 links
M = [1 1 1];     % mass of the link is 1
L = [1 1 1];     % the length of the link is 1
Lt = [1, 1, 1];  % the center of mass is located at length of 1
g = 10;          % acceleration of gravity
I = [0,0,0];     % Moment of inertial of each link ( 0 means that the mass is concentrated)

% =========== Without damping
c = [0, 0, 0];

% === Full range of angles
dt = 0.05;           
n_data = 10000;     
angle_range = pi;    
max_deriv_ratio = 2;
pdl = sim.Pendulum(n_links, M, I, L, Lt, g, c);
[Y, C] = neuroanim.training_data_control(pdl, n_data, dt, max_deriv_ratio, angle_range, tau_range);
ctx = neuroanim.Context();
ctx.init(pdl, Y, C, dt, max_deriv_ratio, angle_range, tau_range);
ctx.dump_file('training_data/l3_c_0001.mat');


% === Reduce the domain of angles on which we train
%     This seemed to be working well.
max_deriv_ratio = 2;
dt = 0.05;
n_data = 10000;
angle_range = pi/4;

pdl = sim.Pendulum(n_links, M, I, L, Lt, g, c);
[Y, C] = neuroanim.training_data_control(pdl, n_data, dt, max_deriv_ratio, angle_range, tau_range);
ctx = neuroanim.Context();
ctx.init(pdl, Y, C, dt, max_deriv_ratio, angle_range, tau_range);

ctx.dump_file('training_data/l3_c_0002.mat');

% =========== With damping
% ===========
c = [1,1,1];
% === Full range of angles
dt = 0.05;           
n_data = 10000;     
angle_range = pi;    
max_deriv_ratio = 2;
pdl = sim.Pendulum(n_links, M, I, L, Lt, g, c);
[Y, C] = neuroanim.training_data_control(pdl, n_data, dt, max_deriv_ratio, angle_range, tau_range);
ctx = neuroanim.Context();
ctx.init(pdl, Y, C, dt, max_deriv_ratio, angle_range, tau_range);
ctx.dump_file('training_data/l3_c_0101.mat');


% === Reduce the domain of angles on which we train
%     This seemed to be working well.
max_deriv_ratio = 2;
dt = 0.05;
n_data = 10000;
angle_range = pi/4;

pdl = sim.Pendulum(n_links, M, I, L, Lt, g, c);
[Y, C] = neuroanim.training_data_control(pdl, n_data, dt, max_deriv_ratio, angle_range, tau_range);
ctx = neuroanim.Context();
ctx.init(pdl, Y, C, dt, max_deriv_ratio, angle_range, tau_range);

ctx.dump_file('training_data/l3_c_0102.mat');