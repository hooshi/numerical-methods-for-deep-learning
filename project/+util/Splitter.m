function [out] = Splitter(n_samples, n_folds, should_permute)
% function [out] = Splitter(n_samples, n_folds)

%% TEST
if(nargin==0) run_min_example(); return; end

%% CONSTRUCTION
n_samples = uint32(n_samples);
n_folds   = uint32(n_folds);

if(should_permute) 
    all_indices = randperm( n_samples );
else
    all_indices = 1:n_samples; 
end
n_samples_per_fold = n_samples / n_folds;


%% FUNCTIONS

% GET THE INDICES ASSOCIATED WITH EACH FOLD
    function fold_indices = fn_get_indices(fold_id)
        assert( fold_id >= 1 );
        assert( fold_id <= n_folds );
        % DAMN this indices that start from one.
        begin_id = n_samples_per_fold * (fold_id - 1) + 1; 
        
        if(fold_id == n_folds)
            end_id = uint32(n_samples);
        else
            end_id = begin_id+n_samples_per_fold-1;
        end
        
        fold_indices= all_indices(begin_id:end_id);
    end

%% CREATE FUNCTIONS

out = struct;
out.get_indices = @fn_get_indices;
end

function run_min_example()
rng(0)
sp = cval.Splitter(53, 4);
sp.get_indices(1)
sp.get_indices(2)
sp.get_indices(3)
sp.get_indices(4)
end