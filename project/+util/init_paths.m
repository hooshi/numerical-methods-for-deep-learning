% Set all the paths that might be required by a script
restoredefaultpath

DIR_ROOT = pwd;
DIR_MNIST = sprintf('%s%s', DIR_ROOT, '/../../datasets/mnist');
DIR_CIFAR10 = sprintf('%s%s', DIR_ROOT, '/../../datasets/cifar-10-batches-mat');

% Add minFunc
MINFUNC_DIR = sprintf('%s/minFunc', pwd);
addpath( genpath(MINFUNC_DIR) );
% example_minFunc