close all
clear 

ctx = neuroanim.Context();
ctx.read_file('training_data/l3_c_0201_train0003.mat');

dt = ctx.dt;
nt = 150;
t = double(0:1:nt)*dt;
pdlm = ctx.pdlm;
n_links = pdlm.n_links;

vec = @(xx) xx(:);

p0 = [0 0 0];
q0 = [0 0 0];
%% objective
p_obj = [-pi/2 -pi/4 -pi/4];
q_obj = [0 0 0];

nt_range = 1;
objs.S = cell(nt_range, 1);
objs.t = (nt - nt_range:nt - 1);
Sobj = vec([p_obj(1:n_links); q_obj(1:n_links)]');
objs.S(1:nt_range) = {Sobj};

%% optimization
itr_max = 500;
r_alpha = 1e-7;
reg = @(tau) (regularization( r_alpha, tau ));
e_alpha = 5;
energy.func = @control.energyAllTime;
energy.ratio = e_alpha;

Ynn = zeros(nt, ctx.regressor.params.n_d1);
init_v = vec([p0(1:n_links); q0(1:n_links); zeros(1, n_links)]');
Ynn(1, :) = init_v(1:ctx.regressor.params.n_d1);

%% Solve for control
Tau0 = randn(n_links * (nt - 1), 1);
Tau = control.solve(objs, ctx.regressor.model, Tau0, ...
    Ynn(1,1:pdlm.n_dof()), energy, reg, itr_max);

for i=2:nt
    %% NN as the fake physics
    j = i - 1;
    Ynn_v = [Ynn(i-1,1:pdlm.n_dof()), Tau((j-1)*n_links + 1:j*n_links)' ];
    Ynn(i-1, 1:ctx.regressor.params.n_d1) = Ynn_v(1:ctx.regressor.params.n_d1);
    Ynn(i,1:pdlm.n_dof()) = ctx.predict(Ynn_v(1:ctx.regressor.params.n_d1));
end

save('/home/hooshi/control-deep-time.mat','t');
save('/home/hooshi/control-deep-u.mat','Ynn');
save('/home/hooshi/tau-deep.mat','Tau');

for i=1:1:nt
    hold off, pdlm.draw(Sobj, [0 0]);
    %
    p = Ynn( i , 1:(pdlm.n_links) );
    hold on, pdlm.draw(p(:), [0 0]);
    %
    fprintf("p,q: %s | %s | E: %.3f \n", sprintf("%f ",Sobj), ...
        sprintf("%f ",Ynn(i, :)), 0.5*norm(Sobj' - Ynn(i, 1:pdlm.n_dof())) );
    title( sprintf("t = %f ",  t(i)) );
    pause(0.05);
end

function [R, dR] = regularization(alpha, X)
    [R, dR, ~] = ml2.func.tikhonov( alpha, X );
end
