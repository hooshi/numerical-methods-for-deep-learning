clear
close all
restoredefaultpath

Mstar = [1 1];
Lstar = [1 1];
p0 = [1, 0];
g = 10;
nt = 300;
dt = 0.05;
[t0, Y0] = sim.double_pendulum_simulation(p0(1), p0(2), Lstar(1), Lstar(2), Mstar(1), Mstar(2), g, nt, dt);
from_head = 0;

if( from_head )
    n_links = 2;
    M = [Mstar 0];
    L = [Lstar 0];
    Lt = [Lstar 0];
    I = [0,0,0];
    pdl = sim.Pendulum(n_links, M, I, L, Lt, g);
    [t1, Y1] = sim.solve([p0, 0, 0], double(0:1:nt) * dt, [], pdl);
else
    n_links = 3;
    M = [1e10 , Mstar];
    I = [0,0,0];
    L = [1, Lstar];
    Lt = [1, Lstar];
    pdl = sim.Pendulum(n_links, M, I, L, Lt, g);
    [t1, Y1] = sim.solve([0, p0, 0, 0, 0], double(0:1:nt) * dt, [], pdl);
end


for i=1:nt
    p = Y1(i,1:n_links);
    hold on, pdl.draw(p(:), [0 0]);
    %
    if(n_links <= 2) p = Y0(i,1:n_links);
    else             p = [0 Y0(i, 1:2)];            
    end
    hold off, pdl.draw(p(:), [0 0]);
    %
    title( sprintf("t = (verif:)%f (me:)%f ", t0(i), t1(i)) );
    pause(0.05);
end
