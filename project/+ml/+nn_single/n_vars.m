function ans = n_vars(params)
% function ans = n_vars(params)

n_K = [params.n_d1, params.n_d2];
n_bk = [1, params.n_d2]; % add b to every row.
n_W = [params.n_d2,  params.n_c];
n_bw = [1,  params.n_c]; % add b to every row

ans = prod(n_K) + prod(n_bk) + prod(n_W) + prod(n_bw);

end

