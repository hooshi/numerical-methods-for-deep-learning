classdef NNSingle < handle
    % NNSingle
    
    properties
        params % necessary info
        ei     % extra info
        x
    end
    
    methods
        function this = NNSingle(params)
            % function this = NNSingle(params)
            this.params = params;
            this.x = [];
            ei = struct;
        end
        
        function set_x(this, x)
            this.x = x;
        end
        
        function [f, df, d2f] = objective(this, x, Y, C)
            % function [f, df, d2f] = objective(this, x, Y, C)
            [R,dR] = ml.nn_single.l2_regularizer(x,this.params);
            [E, dE, d2E] = ml.nn_single.objective(x,Y,C,this.params);
            f = E+ R;
            df = dE+dR;
            d2f = @(x_) d2E(x_); % To do, add regularization terms.
        end
        
        function C = predict(this, Y)
            assert( ~isempty(this.x) );
            [K, bk, W, bw] = ml.nn_single.unwrap_vars(this.x, this.params);
            switch this.params.activation_type
                case "tanh"
                    C = tanh( Y*K + bk ) * W + bw;
                case "relu"
                    C = max( Y*K + bk , 0) * W + bw;
                case "srelu"
                    C = ml.nn_single.smooth_relu( Y*K + bk ) * W + bw;
            end
        end
        
    end
end

