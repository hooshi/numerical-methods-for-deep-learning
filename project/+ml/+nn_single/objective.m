function [E, dE, d2E] = objective(x, Y, C, params)
% [E, dE] = objective(x, Y, C, params)

if( nargin == 0 ) 
    %run_min_example('relu');
    %run_min_example('tanh');
    run_min_example('linear');
    %run_min_example('srelu');
    return; 
end

% Get the matrices
[K, bk, W, bw] =  ml.nn_single.unwrap_vars(x, params);

% Get activation
switch params.activation_type
    case "srelu"
        sigma_f = @(l) ml.nn_single.smooth_relu(l);
        dsigma_f = @(l) smooth_relu_der(l);
    case "relu"
        sigma_f = @(l) max(l, 0);
        dsigma_f = @(l) l > 0;        
    case "tanh"        
        sigma_f = @(l) tanh(l);
        pow2_f = @(l) l.*l;
        dsigma_f = @(l) 1-pow2_f(tanh(l));
    case "linear" % To test gauss newton jacobian
        sigma_f = @(l) l;
        dsigma_f = @(l) ones(size(l));
    otherwise
        assert(0)
end

% Get Z and handles to its jacobian
[Z, dZdK_f, dZdb_f, dZdKt_f, dZdbt_f] =  ml.nn_single.full_layer(K, bk, Y, sigma_f, dsigma_f);
    
% Get sofmax and its derivative
S = Z*W + bw;
% [E, dEdS] =  ml.nn_single.softmax(S, C);
%size(S)
%size(C)
fro = norm(S-C, 'fro');
E = 0.5 * fro * fro ./ size(Y,1);
dEdS = (S-C) ./ size(Y,1);
d2EdS = @(V) V ./ size(Y,1);
 
if( nargout >= 2)
% Get derivatives for K

dEdZ = dEdS * W';
dEdK = dZdKt_f( dEdZ );
dEdbk = dZdbt_f( dEdZ );

% Get the derivatives for W
dEdW = Z' * dEdS;
dEdbw = sum(dEdS, 1);

% Get the jacobian
dE = ml.nn_single.wrap_vars(dEdK,dEdbk, dEdW, dEdbw, params);
end

% Second derivative
    function d2obj = d2objective(dx) 
        [dK, dbk, dW, dbw] =  ml.nn_single.unwrap_vars(dx, params);
        d2EdZ = @(V) d2EdS(V*W)*W';
        
        % ======= Diagonal terms
        d2EdK =  dZdKt_f( d2EdZ( dZdK_f( dK ) ) );
        d2Edbk = dZdbt_f( d2EdZ( dZdb_f( dbk ) ) );
        d2EdW =  Z'  * d2EdS( Z  * dW  );
        d2Edbw = sum(  d2EdS( repmat(dbw, size(S,1) , 1) ), 1);
        
        % ======= Cross terms (bw terms missing)
        
        % (d/dK)dW * dW
        d2EdKdW = dZdKt_f ( d2EdS( Z*dW )*W' + dEdS * dW');
        % (d/dK)dbk * dbk
        d2EdKdbk = dZdKt_f( d2EdZ(dZdb_f( dbk )) );
        
        % (d/dW)/dK * dK
        d2EdWdK = Z'*d2EdS( (dZdK_f(dK))*W ) +  dZdK_f(dK)'*dEdS;
        % (d/dW)/dbk * dbk
        d2EdWdbk = Z'*d2EdS( (dZdb_f(dbk))*W ) +  dZdb_f(dbk)'*dEdS;
        
        % (d/dbk)/dK*dK
        d2EdbkdK = dZdbt_f( d2EdZ(dZdK_f( dK )));
        % (d/dbk)/dW*dW
        d2EdbkdW = dZdbt_f ( d2EdS( Z*dW )*W' + dEdS * dW');
        
        % Assemble all
        d2obj = ml.nn_single.wrap_vars(d2EdK + d2EdKdW + d2EdKdbk, ...
            d2Edbk + d2EdbkdW + d2EdbkdK, ...
            d2EdW + d2EdWdK + d2EdWdbk, ...
            d2Edbw, ...
            params);
    end
if( nargout >= 3)
    d2E = @d2objective;
end

end

function dR = smooth_relu_der(Y)
[~,dR] = ml.nn_single.smooth_relu(Y);
end

%%%=================================================
%%%            Test function
%%%=================================================
function run_min_example(activation_name)

fprintf("================= TESTING %s ================ \n", activation_name);

% Fix random seed
rng(214);

n =  100;
d1 = 3;
d2 = 8;
c =4;

Y = randn(n, d1);
C = max( randn(n,c), 0.1);
C = C./sum(C,2);

params = ml.nn_single.NNSingleParams();
params.n_d1 = d1;
params.n_d2 = d2;
params.n_c    = c;
params.activation_type = activation_name;
[K, bk, W, bw] = ml.nn_single.unwrap_vars([], params);
x = ml.nn_single.wrap_vars(K, bk, W, bw, params);
x = randn( size(x) );

[E0,dE, d2E] = ml.nn_single.objective(x, Y, C, params);

%%======================= Test the derivative
h = 1;
n_attempt = 20;

 % Bias cross terms for bw is not computed so setting dbw to non-zero
 % and other to non-zero at the same time will not work.
dK = rand(size(K));
dbk = rand(size(bk));
dW = rand(size(W));
dbw = zeros(size(bw));
dx = ml.nn_single.wrap_vars(dK, dbk, dW, dbw, params);

% dx = randn(size(x));

lw = 10;

fprintf('%*s %*s %*s %*s %*s %*s \n', ...
    lw, "constant", lw, "ratio", lw, "linear", lw, "ratio", lw, "quadratic", lw, "ratio");
    
for i=1:n_attempt
    Ee =  ml.nn_single.objective(x+h*dx, Y, C, params);
    El = E0 +  dE(:)'*(h*dx);
    Eq = El +  0.5*h*h*dx(:)'*d2E(dx); % This fails because it is not exact

    
    t  = abs(E0-Ee);
    t1 = abs(El-Ee);
    t2 = abs(Eq-Ee);
    %
    if(i > 1)
        fprintf('%*.2e %*.2f %*.2e %*.2f %*.2e %*.2f\n', ...
            lw, t, lw, told/t,  lw, t1, lw, t1old/t1, lw, t2, lw, t2old/t2);
    end
    
    % Half the finite-difference step
    told = t;
    t1old = t1;
    t2old = t2;
    h = h/2;
end

end