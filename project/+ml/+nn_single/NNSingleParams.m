classdef NNSingleParams 
    % NNSingleParams
    properties (Access = public)
        n_c 
        n_d1
        n_d2 
        activation_type
        lambda_K
        lambda_bk
        lambda_W
        lambda_bw
    end
    
    methods
        function this = NNSingleParams()
            this.n_c = -1;
            this.n_d1 = -1;
            this.n_d2 = -1;
            this.activation_type = "";
            this.lambda_K = 0;
            this.lambda_bk = 0;
            this.lambda_W = 0;
            this.lambda_bw = 0;
        end
    end

end



