function[x] = newton(fun, reg, x0,param)
% function[x] = newton(fun,x0,param)
%
% params:
%  - alpha: amount of regularization in objective = loss + alpha *
%    regularizationi
%  - n_newton_max_iter: maximum number of iterations for steepest descent
%  - n_max_linesearch_iter: maximum number of iterations for gradient
%    descent
%  - mu0: first value for linesearch parameter
%  - Other parameters that are passed to the objective function should
%    also be here.

% =========== Check for default parameters
param = utility.check_default(param, 'alpha', 1e-5);
param = utility.check_default(param, 'n_newton_max_iter', 10);
param = utility.check_default(param, 'n_linesearch_max_iter', 10);
param = utility.check_default(param, 'n_pcg_max_iter', 10);
param = utility.check_default(param, 'pcg_tol', 1e-1);
param = utility.check_default(param, 'n_linesearch_max_iter', 10);
param = utility.check_default(param, 'verbosity', 1);
param = utility.check_default(param, 'diag', 0);


% =========== Handle not assigning a regularizer function
regularizer_function = [];
if(~isempty(reg))
    regularizer_function = @(x_) reg(x_,param);
else
    regularizer_function = @(x_) deal(0, zeros('like', x_), sparse(size(x_,1), size(x_,1)) );
end

% =========== Duplicating the preconditioner

alpha = param.alpha;
x = x0;
[obj,dobj,Hobj] = fun(x);
[R,dR,HR] = regularizer_function(x);
bs = size(x,1)/size(HR,2);

F = obj + alpha * R;
dF = dobj + alpha * dR;
HF = @(x_) Hobj(x_) + alpha * apply_mult_columnwise(HR,x_,bs) +  x_ * param.diag;

mu = 1; 


M = [];
% This is broken and makes things worse. Needs debugging.
% Also backslash solve is not O(n) in two dimensions. Need
% to do ADI instead.
% if(~isempty(reg))
%     M = @(v) 1/alpha * apply_precon_columnwise(HR, v, bs);
% end
    
for j=1:param.n_newton_max_iter
    
    if(param.verbosity >= 1)
        fprintf('%3d.0   %3.2e   %3.2e\n',j,obj,norm(dF))
    end
    
    [s,~] = pcg(HF,-dF,param.pcg_tol,param.n_pcg_max_iter, M);
    
    % test s is a descent direction
    if s(:)'*dF(:) > 0
        s = -dF;
    end
    
    % Armijo line search
    cnt = 1;
    while 1
        %Try smaller step
        xtry = x + mu*s;
        [objtry,~,~] = fun(xtry);
        [Rtry, ~,~] = regularizer_function(x);
        Ftry = objtry + alpha * Rtry;

        if(param.verbosity >= 1)
            fprintf('%3d.%d   %3.2e \n',j,cnt,Ftry)
        end
            
        % Success
        if Ftry< F
            break
        end
        
        % Break step
        mu = mu/2;
        cnt = cnt+1;
        
        % Check for max iterations
        if cnt > param.n_linesearch_max_iter
            warning('Line search break, Iter: %d, n_ls: %d norm(dF): %.3g',...
                j, cnt, norm(dF) );
            return
        end
    end
    
    % Increase mu if cnt was already (cap at 1)
    if cnt == 1
        mu = min(mu*1.5,1);
    end
    
    x = xtry;    
    [obj,dobj,Hobj] = fun(x);
    [R,dR,~] = regularizer_function(x);
    F = obj + alpha * R;
    dF = dobj + alpha * dR;
    HF =  @(x_) Hobj(x_) + alpha * apply_mult_columnwise(HR,x_,bs);

    
    % Plot while solving 
    if (param.verbosity >= 2)
        w = reshape(x,3,2);
        figure(1)
        t = linspace(-3,4,129);
        q = -w(1,1)/w(2,1)*t -w(3,1)/w(2,1);
        hold on
        plot(t,q,'k','linewidth',3)
        hold off
        pause(0.3);
    end
end

end

function v = apply_precon_columnwise(HR, v, bs)
    assert(numel(v) == size(HR,2)*bs )
    assert( size(HR,1) == size(HR,2) )
    
    v = reshape(v, [],bs);
    v = HR\v;
    v = v(:);
end

function v = apply_mult_columnwise(HR, v, bs)
    assert(numel(v) == size(HR,2)*bs )
    assert( size(HR,1) == size(HR,2) )
    
    v = reshape(v, [],bs);
    v = HR * v;
    v = v(:);
end
