function[x] = gradient_descent(fun,reg,x0,param)
%[x] = steepestDescent(fun,reg,x0,param)
%
% params:
%  - alpha: amount of regularization in objective = loss + alpha *
%    regularizationi
%  - n_max_gd_iter: maximum number of iterations for steepest descent
%  - n_max_linesearch_iter: maximum number of iterations for gradient
%    descent
%  - mu0: first value for linesearch parameter
%  - Other parameters that are passed to the objective function should
%    also be here.

% =========== Check for default parameters
param = utility.check_default(param, 'alpha', 1e-5);
param = utility.check_default(param, 'n_gd_max_iter', 10);
param = utility.check_default(param, 'n_linesearch_max_iter', 10);
param = utility.check_default(param, 'mu0', 1.);
param = utility.check_default(param, 'verbosity', 1);
param = utility.check_default(param, 'fixed_mu', 0);
param = utility.check_default(param, 'historyfile', []);

% =========== Handle not assigning a regularizer function
regularizer_function = [];
if(~isempty(reg))
    regularizer_function = @(x_) reg(x_,param);
else
    regularizer_function = @(x_) deal(0, zeros('like', x_));
end

x = x0;
alpha = param.alpha;
[obj,dobj] = fun(x);
[R,dR]     = regularizer_function(x);

F  = obj + alpha*R;
dF = dobj + alpha*dR;

nrm0 = norm(dF);
mu = param.mu0; 
verbosity = param.verbosity;

for j=1:param.n_gd_max_iter
    
    if(verbosity >= 1)
        if( isempty( param.historyfile ))
            fprintf('%3d.0   %3.2e   %3.2e   %3.2e   %3.2e\n',j,F,obj,R,norm(dF)/nrm0);
        else
            fprintf(param.historyfile, '%3d.0   %3.2e   %3.2e   %3.2e   %3.2e\n',j,F,obj,R,norm(dF)/nrm0);
        end
    end
    
    s = -dF;
    
    % Armijo line search
    if(~param.fixed_mu)
        cnt = 1;
        while 1
            xtry = x + mu*s;
            [objtry,dobj] = fun(xtry);
            [Rtry,dR]     = regularizer_function(x);
            Ftry = objtry + alpha*Rtry;
            dF   = dobj + alpha*dR;
            
            if(verbosity >= 1)
                fprintf('%3d.%d   %3.2e   %3.2e   %3.2e   %3.2e\n',...
                    j,cnt,Ftry,objtry,Rtry,norm(dF)/nrm0)
            end
            
            if Ftry< F
                break
            end
            mu = mu/2;
            cnt = cnt+1;
            if cnt > param.n_linesearch_max_iter
                warning('Line search break');
                return;
            end
        end
        
        %If no line search was needed increase initial step
        if cnt == 1
            mu = mu*1.5;
        end
    else
        xtry = x + mu*s;
        [objtry,dobj] = fun(xtry);
        [Rtry,dR]     = regularizer_function(x);
        Ftry = objtry + alpha*Rtry;
        dF   = dobj + alpha*dR;
    end
        
    
    % Update solution
    x = xtry;
    obj = objtry;
    R   = Rtry;
    F   = Ftry;
 
end
