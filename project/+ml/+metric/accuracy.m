function ans = accuracy(Cobs,C)
% function ans = accuracy(Cobs,C)

assert(size(Cobs, 1) == size(C, 1) );
assert(size(Cobs, 2) == size(C, 2) );

[~,ind] = max(C,[],2);
[~,ind_obs] = max(Cobs,[],2);

ans =  sum(ind == ind_obs) / numel(ind);

end

