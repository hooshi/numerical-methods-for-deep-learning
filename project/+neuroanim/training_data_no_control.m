function [Y, C] = training_data_no_control(pdlm, n_data, dt, maxderiv, angle_range)
% function [Y, C] = training_data_no_control2(pdlm, n_data, dt)

if(nargin == 0) run_min_example(); return; end

d = pdlm.n_dof();
c = pdlm.n_dof();
n = n_data;
prange = 1:pdlm.n_p();
qrange = (pdlm.n_p()+1):pdlm.n_dof();

C = zeros(n, c);

Y = rand(n ,d);
Y(:, prange) = (Y(:, prange) - 0.5) * angle_range;
Y(:, qrange) = (Y(:, qrange) - 0.5) * angle_range * maxderiv;

% Find the output
vec = @(x_) x_(:);
if( nargout >= 2)
    for i=1:n
        [~, Co] = sim.solve( vec(Y(i, :)) , vec([0, dt/2, dt]), [], pdlm);
        C(i,:) = Co(end, :);
        if( mod(i, 1000)==0) 
            fprintf("Processed %d data \n", i);
        end
    end
end

end

function run_min_example()
n_links = 3;
M = [1 1 1];
L = [1 1 1];
Lt = [1 1 1];
I = [0,0,0];
g = 10;
pdl = sim.Pendulum(n_links, M, I, L, Lt, g);

[Y, C] = neuroanim.training_data_no_control(pdl, 500, 0.01, 10, pi/4);
plot(Y(:,1), 'o')

pr = 1:n_links;
qr = (n_links+1):(2*n_links);

figure()
for i=1:9
    subplot(3,3,i);
    j = randi([1 500], 1,1);
    title(sprintf("Idx=%d",j));
    fprintf("[0]L,v: %f %f, [1]L,v: %f, %f \n", Y(j,1), Y(j,2), C(j,1), C(j,1) );
    hold off, pdl.draw( Y(j,pr) );
    hold on, pdl.draw( C(j,pr) );
end
end
