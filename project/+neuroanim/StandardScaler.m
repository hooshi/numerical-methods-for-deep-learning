classdef StandardScaler < handle
    % DataSet
    properties
        mean_value
        std_value
    end
    
    methods
        
        function this = StandardScaler(this)
        end
        
        function fit(this, Y)
            this.mean_value = mean(Y,1);
            this.std_value = std(Y);
            this.std_value( this.std_value < 1e-6 ) = 1;
        end
        
        function Yhat = scale(this, Y)
            Yhat = (Y - this.mean_value) ./ this.std_value;
        end

        function Y = unscale(this, Yhat)
            Y = Yhat .* this.std_value + this.mean_value;
        end
        
    end
end

