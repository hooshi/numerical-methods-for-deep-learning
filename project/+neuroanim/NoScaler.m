classdef NoScaler < handle
    % DataSet
    properties
    end
    
    methods
        
        function this = StandardScaler(this)
        end
        
        function fit(this, Y)
        end
        
        function Yhat = scale(this, Y)
            Yhat = Y;
        end

        function Y = unscale(this, Yhat)
            Y = Yhat;
        end
        
    end
end

