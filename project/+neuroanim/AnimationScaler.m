classdef AnimationScaler < handle
    % AnimationScaler
    
    properties
        n_dof
        std_scaler_Y
        std_scaler_C
    end
    
    methods
        function this = AnimationScaler()
            % function this = AnimationScaler()
            this.n_dof = 0;
            this.std_scaler_Y = [];
            this.std_scaler_C = [];
        end
        
        function fit(this, n_dof, Y, C)
            % function fit(n_dof, Y, C)
            this.n_dof = n_dof;
            
%             this.std_scaler_Y = neuroanim.StandardScaler();
%             this.std_scaler_C = neuroanim.StandardScaler();            
            this.std_scaler_Y = neuroanim.NoScaler();
            this.std_scaler_C = neuroanim.NoScaler();
            
            % angles should be in range
            Y(:,1:(n_dof/2)) = wrapToPi(Y(:,1:(n_dof/2)));
            this.std_scaler_Y.fit(Y(:, 1:n_dof));
            this.std_scaler_C.fit(C);
        end
        
        function [Yhat, Chat] = scale(this, Y, C)
            % function [Yhat, Chat] = scale(this, Y, C)
            if(nargin < 3) C = []; end
            
            Y(:,1:(this.n_dof/2)) = wrapToPi(Y(:,1:(this.n_dof/2)));
            Yhat = Y;
            Yhat(:, 1:this.n_dof) = this.std_scaler_Y.scale(Y(:, 1:this.n_dof));
            
            if( (~isempty(C)) )
                C = C - Y(:, 1:this.n_dof);
                Chat = this.std_scaler_C.scale(C);
            end                
        end

        function C = unscale_C(this, Y, Chat)
            % function C = unscale_C(this, Y, Chat)
            C = this.std_scaler_C.unscale(Chat);
            C = C + Y(:, 1:this.n_dof);
            C(:,1:(this.n_dof/2)) = wrapToPi(C(:,1:(this.n_dof/2)));
        end

        function Y = unscale_Y(this, Yhat)
            % function Y = unscale_Y(this, Yhat)
            Y = this.std_scaler_Y.unscale(Yhat);
        end

    end
end

