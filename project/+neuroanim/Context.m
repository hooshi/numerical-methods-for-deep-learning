classdef Context < handle
     % Context
    
    properties
        animation_scaler
        YY
        CC
        pdlm
        dt
        max_deriv_ratio
        angle_range
        tau_range
        regressor
    end
    
    methods
        % Constructor
        function this = Context()
            % function this = Context()
            this.animation_scaler = [];
            this.YY= [];
            this.CC= [];
            this.pdlm= [];
            this.dt= [];
            this.max_deriv_ratio = [];
            this.regressor = [];
        end
        
        % Constructor
        function init(this, pdlm, Y, C, dt, max_deriv_ratio, angle_range, tau_range )
            % function init(this, pdlm, Y, C, dt, max_deriv_ratio )
            if ~exist('tau_range','var') || isempty(tau_range)
                tau_range = 0;
            end
            
            this.YY = Y;
            this.CC = C;
            this.pdlm = pdlm;
            this.dt = dt;
            this.max_deriv_ratio = max_deriv_ratio;
            this.angle_range = angle_range;
            this.tau_range = tau_range;
            this.animation_scaler = neuroanim.AnimationScaler();
            this.animation_scaler.fit( pdlm.n_dof(), Y, C);
        end
        
        function set_regressor(this, regressor)
            % function set_regressor(this, regressor)
            this.regressor = regressor;
        end
        
        % IO
        function dump_file(this, filename)
            % function dump_file(this, filename)
            to_dump = struct;
            to_dump.YY = this.YY;
            to_dump.CC = this.CC;
            to_dump.dt = this.dt;
            to_dump.pdlm = this.pdlm;
            to_dump.max_deriv_ratio =  this.max_deriv_ratio;
            to_dump.regressor= this.regressor;
            to_dump.angle_range = this.angle_range;
            to_dump.tau_range = this.tau_range;
            save(filename, 'to_dump');
        end
        
        function read_file(this, filename)
            % function read_file(this, filename)
            load(filename, 'to_dump');
            this.YY = to_dump.YY;
            this.CC = to_dump.CC;
            this.dt = to_dump.dt;
            this.pdlm = to_dump.pdlm;
            this.max_deriv_ratio =  to_dump.max_deriv_ratio;
            this.regressor = to_dump.regressor;
            %
            if( isfield(to_dump,'angle_range') )
                this.angle_range = to_dump.angle_range;
            else
                n = 1:this.pdlm.n_links;
                this.angle_range = [0; 0];
                this.angle_range(1) = min(this.YY(:, 1:n), [], 1);
                this.angle_range(2) = max(this.YY(:, 1:n), [], 1);
            end
            if( isfield(to_dump,'tau_range') )
                this.tau_range = to_dump.tau_range;
            else
                n = 1:this.pdlm.n_links;
                this.tau_range = [0; 0];
                this.tau_range(1) = min(this.YY(:, 1:n), [], 1);
                this.tau_range(2) = max(this.YY(:, 1:n), [], 1);
            end
            %
            this.animation_scaler = neuroanim.AnimationScaler();
            this.animation_scaler.fit( this.pdlm.n_dof(), this.YY, this.CC);
        end
        
        % prediction
        function C = predict(this, Y)
            % function C = predict(this, Y)
            assert( ~isempty(this.regressor) );
            Yscaled = this.animation_scaler.scale(Y, []);
            C = this.regressor.predict(Yscaled);
            C = this.animation_scaler.unscale_C(Y, C);
        end
        
        % training data
        function [Y, C] = training_data(this)
            % function [Y, C] = training_data(this)
            [Y, C] = this.animation_scaler.scale(this.YY, this.CC);
        end
        
    end
end

