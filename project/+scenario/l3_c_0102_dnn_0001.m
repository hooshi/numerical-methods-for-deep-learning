close all
clear 

ctx = neuroanim.Context();
ctx.read_file('training_data/l3_c_0102.mat');

[Y, C] = ctx.training_data();
ind = randperm( size(Y,1 ) );
Y = Y(ind, :);
C = C(ind, :);
nhalf = size(Y,1)/2;
Ytrain = Y(1:nhalf, :);
Ctrain = C(1:nhalf, :);
Ytest = Y((nhalf+1):end, :);
Ctest = C((nhalf+1):end, :);
 
%% Train
% Build NN
data_dim = size(Y, 2);
num_output = size(C, 2);

% NN architecture
n = [data_dim, 20, 20; ...
      20, 20, num_output*3];
P = cell(1, size(n,2));
Kb_size = 0;

for i=1:size(n,2)
   if n(1,i) ~= n(2,i)
       P{i} = ml2.func.opZero(n(2,i),n(1,i));
   else
       P{i} = ml2.func.opEye(n(1,i));
   end
   Kb_size = Kb_size + n(2,i)*n(1,i) + n(2,i);
end

r_alpha = 0;%0;%1e-4;
reg = @(X) (ml2.func.tikhonov( r_alpha, X ));

rng(57);
X0 = randn((n(2, end) + 1) * num_output + Kb_size, 1);
%act = @ml2.func.smoothRelU;
act = @ml2.func.tanhG;

% 1 link
single_param.alpha = 0.1;
single_param.mu = 0.5;

% 3 link
param.itr_max = 20000;
param.batch_size = 1000;%10000;
param.tol = 0;%1e-6;

param.epochs = 20;

param.alpha = 0.8;
param.beta1 = 0.9;
param.beta2 = 0.999;
param.eps = 1e-8;
param.t = 1;
param.m = 0;
param.v = 0;

mo_param = param;
mo_param.v = 0;
mo_param.epochs = 50;
mo_param.alpha = 0.2;%0.5;%0.03;
mo_param.mu = 0.5;
mo_param.alpha_ratio = 0.95;
mo_param.alpha_min = 0.01;%0.2;%0.02;
mo_param.mu_ratio = 1.1;
mo_param.mu_max = 0.9;

mo_param.alpha = 0.6;%0.03;
mo_param.alpha_min = 0.1;%0.02;

objFunc2 = @(X, y, c) (ml2.nn.objLinearRNN(y, c, X, n, P, act));
X = ml.optimization.minimize(X0, objFunc2, param.itr_max, [], Ytrain, Ctrain);

% objFunc = @(y1, c, X) (ml2.nn.objLinearRNN(y1, c, X, n, P, act));
% [X] = ml2.solver.stochasticGradientDescent(...
%     objFunc, reg, Ytrain, Ctrain, X0, ... 
%     @ml2.solver.sgdNilAnneal, @ml2.solver.sgdAdam, param);
    %@ml2.solver.sgdMAnneal, @ml2.solver.sgdNesterov, mo_param);
% http://cs231n.github.io/neural-networks-3/

%% Test accuracy
[model] = ml2.model.toRNNModel(X, P, act, n, num_output);

Ctrainpred = ml2.classifier.linearRNNPredict(model, Ytrain);
Ctestpred = ml2.classifier.linearRNNPredict(model, Ytest);

fprintf("Error on test: %.2f  \n", norm(Ctrain-Ctrainpred, 'fro')/ norm(Ctrain, 'fro') * 100 );
fprintf("Error on train: %.2f \n", norm(Ctest-Ctestpred, 'fro')/ norm(Ctest, 'fro') * 100  )

%% Dump to filesystem
regressor.train_param = mo_param;
regressor.model = model;
regressor.predict = @(Y) (ml2.classifier.linearRNNPredict(regressor.model, Y));
regressor.params.n_d1 = data_dim;
ctx.set_regressor( regressor );
ctx.dump_file('training_data/l3_c_0102_train0001.mat')
