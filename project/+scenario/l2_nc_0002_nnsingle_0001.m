close all
clear 

ctx = neuroanim.Context();
ctx.read_file('training_data/l2_nc_0002.mat');

[Y, C] = ctx.training_data();
ind = randperm( size(Y,1 ) );
Y = Y(ind, :);
C = C(ind, :);
nhalf = int32(size(Y,1)/2);
Ytrain = Y(1:nhalf, :);
Ctrain = C(1:nhalf, :);
Ytest = Y((nhalf+1):end, :);
Ctest = C((nhalf+1):end, :);
 
% classifier
RATIO_HIDDEN_UNITS = 5;
ACTIVATION = 'tanh';
REGULARIZATION_RATIO_K = 0;
REGULARIZATION_RATIO_BK = 0;
REGULARIZATION_RATIO_W = 0;
REGULARIZATION_RATIO_BW = 0;
MAX_OPTIMIZATION_ITERATIONS = 30000;
 
nn_params = ml.nn_single.NNSingleParams();
nn_params.n_d1 = size(Ytrain, 2);
nn_params.n_d2 = nn_params.n_d1 * RATIO_HIDDEN_UNITS;
nn_params.n_c = size(Ctrain, 2);
nn_params.activation_type = ACTIVATION;
nn_params.lambda_K = REGULARIZATION_RATIO_K;
nn_params.lambda_bk = REGULARIZATION_RATIO_BK;
nn_params.lambda_W = REGULARIZATION_RATIO_W;
nn_params.lambda_bw = REGULARIZATION_RATIO_BW;
nn = ml.nn_single.NNSingle(nn_params);

x0 = randn( ml.nn_single.n_vars(nn.params), 1) ; 
n_iters = -MAX_OPTIMIZATION_ITERATIONS;
% x = optimization.minimize(x0, @objective_for_minimize, n_iters, historyfile, Y, Cobs, params);
x = ml.optimization.minimize(x0, @nn.objective, n_iters, [], Ytrain, Ctrain);
nn.set_x(x);

Ctrainpred = nn.predict(Ytrain);
Ctestpred = nn.predict(Ytest);

ctx.set_regressor( nn );
ctx.dump_file('training_data/l2_nc_0002_train_nnsingle_0001.mat') % This name must match the file name

fprintf("Error on test: %.2f  \n", norm(Ctrain-Ctrainpred, 'fro')/ norm(Ctrain, 'fro') * 100 );
fprintf("Error on train: %.2f \n", norm(Ctest-Ctestpred, 'fro')/ norm(Ctest, 'fro') * 100  );


%%% This is a two-link with reduced range.
%%% Error on test: 0.29  
%%% Error on train: 0.30 
