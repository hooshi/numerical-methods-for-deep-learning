clear
close all
restoredefaultpath

n_links = 1;
M = [1 0 0];
L = [1 0 0];
Lt = [1, 0, 0];
g = 10;
nt = 3000;
dt = 0.05;
I = [0,0,0];
c = [1,0,0];
pdl = sim.Pendulum(n_links, M, I, L, Lt, g, c);
p0 = [0];
q0 = [0];
%tau_fn = @(t) cos(t) ;%[0.2*sin(t); 0; 0];
tau_fn = @(t) 1 ;%[0.2*sin(t); 0; 0];
[t1, Y1] = sim.solve([p0(:); q0(:)], double(0:1:nt) * dt, tau_fn, pdl);

% hold off, plot( t1(1:1000), Y1(1:1000,1) ); return;

for i=1:nt
    p = Y1(i,1:n_links);
    hold off, pdl.draw(p(:), [0 0]);
    %
    title( sprintf("t = %f ",  t1(i)) );
    pause(0.005);
end
