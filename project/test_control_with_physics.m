close all
clear 

ctx = neuroanim.Context();
ctx.read_file('training_data/l2_c_0101_train0012.mat');
[Y,C] = ctx.training_data();
C(1:20,:)

dt = ctx.dt;
nt = 100;
t = double(0:1:nt)*dt;
pdlm = ctx.pdlm;
n_links = pdlm.n_links;

vec = @(xx) xx(:);

p0 = [0 0 0];
q0 = [0 0 0];
%% objective
p_obj = [-pi/8 -pi/4 -pi/4];
q_obj = [0 0 0];

nt_range = 2;
objs.S = cell(nt_range, 1);
objs.t = (nt - nt_range:nt - 1);
Sobj = vec([p_obj(1:n_links); q_obj(1:n_links)]');
objs.S(1:nt_range) = {Sobj};

%% optimization
itr_max = 100;
e_alpha = 5;
energy.func = @control.energyAllTime;
energy.ratio = e_alpha;

s1 = vec([p0(1:n_links); q0(1:n_links)]');

%% Sliding window
win_len = 10;
win_times = ceil(nt / win_len);

Yode = [];

for k = 1:win_times
    objs.t = (nt - nt_range - (k-1) * win_len:nt - 1 - (k-1) * win_len);
    
    %% Solve for control
    Tau0 = randn(n_links * ((nt - 1) - (k-1) * win_len), 1);
    r_alpha = min(1e-6, 1e-7 * (k - 1) * 1e4);
    reg = @(tau) (regularization( r_alpha, tau ));
    Tau = control.solve(objs, ctx.regressor.model, Tau0, ...
        s1, energy, reg, itr_max);

    %% Fit splines
    taus = reshape(Tau, n_links, (nt - 1) - (k-1) * win_len);
    tau_sp = cell(n_links, 1);
    
    tt = 1:nt-1 - (k-1) * win_len;
   
    for i = 1:n_links
       tau_sp{i} = spline(tt, taus(i, :));

       plot_tt = 1:0.25:nt-1 - (k-1) * win_len;
       %hold on, plot(plot_tt, ppval(tau_sp{i}, plot_tt),'-')
    end
    %hold off
    
    %% Real physics with controls computed from the fake one
    tau_fn = @(t) tau_spline(tau_sp, t);%[0.2*sin(t); 0; 0];
    [~, s1] = sim.solve(s1, t((k-1)*win_len + 1:k*win_len), tau_fn, pdlm);
    
    Yode = [Yode; s1];
    s1 = s1(end, :);
end

figure
for i=1:1:nt
    hold off, pdlm.draw(Sobj, [0 0]);
    %
    p = Yode( i , 1:(pdlm.n_links) );
    hold on, pdlm.draw(p(:), [0 0]);
    %
    fprintf("p,q: %s | %s | E: %.3f \n", sprintf("%f ",Sobj), ...
        sprintf("%f ",Yode(i, :)), 0.5*norm(Sobj' - Yode(i, 1:pdlm.n_dof())) );
    title( sprintf("t = %f ",  t(i)) );
    pause(0.05);
end

function [R, dR] = regularization(alpha, X)
    [R, dR, ~] = ml2.func.tikhonov( alpha, X );
end

function [tau] = tau_spline(tau_sp, t)
    tau = zeros(length(tau_sp), 1);
    for i=1:length(tau_sp)
        tau(i) =  ppval(tau_sp{i}, t);
    end
end