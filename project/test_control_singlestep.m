close all
clear 

ctx = neuroanim.Context();
ctx.read_file('training_data/l1_c_0101_train0001.mat');
[Y,C] = ctx.training_data();
C(1:20,:)

dt = ctx.dt;
nt = 2000;
t = double(0:1:nt)*dt;
pdlm = ctx.pdlm;
n_links = pdlm.n_links;

vec = @(xx) xx(:);

%% objective
p_obj = [pi/10 0 0];
q_obj = [0 0 0];
Cobj = vec([p_obj(1:n_links); q_obj(1:n_links)]');

%% optimization
itr_max = 100;
r_alpha = 1e-3;
r_alpha3 = 1e-2;
reg = @(tau) (regularization( r_alpha, tau ));
e_alpha = 10;
energy.func = @control.energyEntire;
energy.ratio = e_alpha;

p0 = [pi/2 -pi/8  0];
q0 = [0 0 0];

Ynn = zeros(nt, ctx.regressor.params.n_d1);
init_v = vec([p0(1:n_links); q0(1:n_links); zeros(1, n_links)]');
Ynn(1, :) = init_v(1:ctx.regressor.params.n_d1);
for i=2:nt
    %% Solve for control
    tau0 = randn(n_links, 1);
    tau = control.solve(Cobj, ctx.regressor.model, tau0, ...
        Ynn(i-1,1:pdlm.n_dof()), energy, reg, itr_max);
    
    %% NN as the fake physics
    Ynn_v = [Ynn(i-1,1:pdlm.n_dof()), tau' ];
    Ynn(i-1, 1:ctx.regressor.params.n_d1) = Ynn_v(1:ctx.regressor.params.n_d1);
    Ynn(i,1:pdlm.n_dof()) = ctx.predict(Ynn_v(1:ctx.regressor.params.n_d1));
end

for i=1:1:nt
    hold off, pdlm.draw(Cobj, [0 0]);
    %
    p = Ynn( i , 1:(pdlm.n_links) );
    hold on, pdlm.draw(p(:), [0 0]);
    %
    fprintf("p,q: %s | %s | E: %.3f \n", sprintf("%f ",Cobj), ...
        sprintf("%f ",Ynn(i, :)), 0.5*norm(Cobj' - Ynn(i, 1:pdlm.n_dof())) );
    title( sprintf("t = %f ",  t(i)) );
    pause(0.05);
end

function [R, dR] = regularization(alpha, X)
    [R, dR, ~] = ml2.func.tikhonov( alpha, X );
end