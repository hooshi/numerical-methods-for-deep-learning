function[x] = steepestDescent(fun,reg,x0,param)
%[x,hist] = steepestDescent(fun,reg,x0,param)
%

x = x0;
alpha = param.alpha;
[obj,dobj] = fun(x,param);
[R,dR,d2R]     = reg(x,param);

F  = obj + alpha*R;
dF = dobj + alpha*dR;

nrm0 = norm(dF);

mu = 1; 

for j=1:param.maxIter
    
    fprintf('%3d.0   %3.2e   %3.2e   %3.2e   %3.2e\n',j,F,obj,R,norm(dF)/nrm0)
    s = -dF;
    
    % Armijo line search
    cnt = 1;
    while 1
        xtry = x + mu*s;
        [objtry,dobj] = fun(xtry,param);
        [Rtry,dR]     = reg(xtry,param);
        Ftry = objtry + alpha*Rtry;
        dF   = dobj + alpha*dR;
        fprintf('%3d.%d   %3.2e   %3.2e   %3.2e   %3.2e\n',...
                  j,cnt,Ftry,objtry,Rtry,norm(dF)/nrm0)

        if Ftry< F
            break
        end
        mu = mu/2;
        cnt = cnt+1;
        if cnt > 10
            error('Line search break');
        end
    end
    if cnt == 1
        mu = mu*1.5;
    end
    x = xtry;
    obj = objtry;
    R   = Rtry;
    F   = Ftry;
 
end