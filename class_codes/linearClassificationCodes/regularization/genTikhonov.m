function[R,dR,d2R] = genTikhonov(W,param)
%[R,dR,d2R] = genTikhonov(W,L,param)
% nc - number of classes

nc = param.nc;
L  = param.L;
h  = param.h;
W = reshape(W,[],nc);
L = blkdiag(L,1);

LW = L*W;

R  = 0.5* prod(h) * (LW(:)'*LW(:));
dR = prod(h) * L'*LW;
dR = dR(:);

d2R = prod(h)*(L'*L);