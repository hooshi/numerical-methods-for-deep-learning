clear
close all

% testing linear regression

% FIX random number generator
rng(0)

% --------------------------------
% Create a dummy regression problem
% --------------------------------
a = 0.0001;
% Y = [1, 1+a; 1, 1+2*a; 1, 1+3*a];
Y = randn(3,2)

% true solution in null space of the Y
% w = [5;-5];
w = [1;-1];

% right hand side
c = Y*w + randn(3,1)*1e-2;
  
% Objective function
F = @(x) 0.5*norm(Y*x-c)^2;

% --------------------------------
% Draw the iso-contours of the problem
% --------------------------------
n=64;
t = linspace(-4,4,n);
f = zeros( size(t,1), size(t,1) );
for i=1:n
    for j=1:n
        f(i,j)=F([t(i);t(j)]);
    end 
end

contourf(t,t,f)

% --------------------------------
% Run Steepest Descent
% --------------------------------
n_iteration=50
alpha_in = 0.1
[w, rho, eta, W] = steepestDescent(Y, c, n_iteration);

hold on
plot(W(1,:), W(2,:), '-o');
hold off

% --------------------------------
% Run Conjugate Gradient
% --------------------------------

hold