function [w, rho, eta, W] = steepestDescent(Y, c, maxIter, alpha_in, w0)
%function [w, rho, eta] = steepestDescent(Y, c, maxIter)
% r = c - Yw and obj = |r|^2

if nargin >= 5
    w = w0;
else
    w = zeros(size(Y,2), 1);
end

r = c;
rho = zeros(maxIter,1);
eta = zeros(maxIter,1);
W = [w];

for i=1:maxIter
    
    % function value
    rho(i) = 0.5 * r' * r;
    fprintf('%3d %3.4e %3.4e\n',i,rho(i), eta(i))
    
    % gradient 
    s = Y' * r;
    Ys = Y*s;
    
    % computing learning rate
    if(nargin >= 4)
        alpha = alpha_in;
    else
        alpha = (r'*Ys)/norm(Ys)^2;
    end
        
    % update
    w = w - alpha *s;
    
    % Full update, has less error for huge number of iterations because 
    % of raound of errors
    %r = c - Y*w;
    
    % Cheap way.
    r = r - alpha * Ys;
    
    % Record the solution
    W = [W, w];
    
end

end

