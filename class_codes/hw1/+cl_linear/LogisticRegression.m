function [out] = LogisticRegression(Y, labels, params)
% [out] = LogisticRegression(Y, labels, params)
% parameters
%  alpha:          regularization amount
%  tr: Transformation to be applied to input
%  optimizer: "newtoncg", "gd", "gd_minimize"
%  optimizer_iters: number of iterations
%  verbosity: 0  1 or 2
%  random_guess: 0  or 1
params = utility.check_default(params, 'historyfile', []);
params = utility.check_default(params, 'optimizier_diag', 0);

if(nargin == 0) run_min_example(); return; end

% Get the labels
label_tools = utility.LabelTools(labels);
c = label_tools.n_labels;

% Add Bias
tr = transformation.Glue( params.tr, transformation.Bias);

% Get W and C
d0 = size(Y,2);
Y = tr.tr(Y);
Cobs = label_tools.old_labels_to_proba_matrix( labels );
d = size(Y, 2);

% W = zeros(d,c);
W = [];
if(params.random_guess) W = randn(d,c); 
else                    W = zeros(d,c);
end

%  setup the function and regularization 
% L   = Functions_laplacian(n,param.h); Won't work unless you take care of
% RG and B separately.
optim_params.nc = c;
switch params.regularization
    case "identity"
        optim_params.h = 1;
        optim_params.L = speye((d-1),(d-1)); %% Take care of bias
    case "laplacian"
        assert( d == d0 +1 );
        d_image = sqrt(d0);
        assert( d_image*d_image == d0 );
        
        optim_params.h = [1/d_image, 1/d_image];
        optim_params.L = cl_linear.laplacian([d_image, d_image], optim_params.h); 
end

optim_reg = @(W_,param_) cl_linear.tikhonov(W_,param_);
optim_fun = @(W_,~)     cl_linear.softmax(W_,Y,Cobs);
optim_params.alpha   = params.alpha;
optim_params.verbosity = params.verbosity;

%% Train
    function [f, df] = objective_for_minimize(W_)
        [E, dE] = optim_fun(W_);
        [R, dR] =  optim_reg(W_, optim_params);
        f = E + params.alpha*R;
        df = dE + params.alpha*dR;
    end

if( strcmp(params.optimizer, 'newtoncg') )    
    optim_params.diag = params.optimizier_diag;
    optim_params.n_newton_max_iter = params.optimizer_iters;
    optim_params.n_pcg_max_iter = 30;
    optim_params.pcg_tol = 1e-3;
    W = optimization.newton(optim_fun,optim_reg,W(:),optim_params);

elseif( strcmp(params.optimizer, 'gd') )
    optim_params.n_gd_max_iter = params.optimizer_iters;
    %param.mu0=0.01;
    %param.fixed_mu = 1;
    optim_params.n_linesearch_max_iter=20;
    optim_params.mu0 = 0.01;
    W = optimization.gradient_descent(optim_fun,optim_reg,W(:),optim_params);
    
elseif( strcmp(params.optimizer, 'gd_minimize') )
    length = -params.optimizer_iters;
    W = optimization.minimize(W(:), @objective_for_minimize, length, params.historyfile);

end
W = reshape(W, d, c);
 
%% Predict
    function C = fun_predict(Y)
        Y = tr.tr(Y);
        YW = Y*W;
        YW = YW - max(YW,[],2);
        C = exp(YW);
        C = C ./ sum(C, 2);
    end

out = struct;
out.predict = @fun_predict;
out.weights = W;
out.Cobs = Cobs;
out.label_tools = label_tools;
end

function run_min_example()

[Y, labels] = dataset.make_linear(500);

params.verbosity = 0;
params.alpha = 1e-7;
params.regularization = 'identity';
params.tr = transformation.Normalizer(Y,true);
params.random_guess = false;

params.optimizer = 'newtoncg';
params.optimizer_iters = 10;
lrnewton = cl_linear.LogisticRegression(Y, labels, params);

params.optimizer = 'gd';
params.optimizer_iters = 300;
lrgd = cl_linear.LogisticRegression(Y, labels, params);

% return
% Plot the stuff
Yploty = Y; % params.tr.tr(Y);
hold off; metric.plot2d( lrgd, min(Yploty, [], 1), max(Yploty, [], 1));
hold on; plot( Yploty(labels == 0,1), Yploty(labels == 0,2), 'go');
hold on ; plot( Yploty(labels == 1,1), Yploty(labels == 1,2), 'ro');

% Plot the line
figure(1)
w = reshape(lrnewton.weights,3,2);
t = linspace(-0.5,0.5,129);
q = -w(1,1)/w(2,1)*t -w(3,1)/w(2,1);
% q = -a/b*t + -1/b;
% hold on; plot(t,q,'k','linewidth',1);

% Plot the line
w = reshape(lrgd.weights,3,2);
t = linspace(-0.5,0.5,129);
q = -w(1,1)/w(2,1)*t -w(3,1)/w(2,1);
% q = -a/b*t + -1/b;
% hold on; plot(t,q,'k--','linewidth',1);


end
