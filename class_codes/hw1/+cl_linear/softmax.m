function[E,dE,d2E] = softmax(W,Y,C)
% [E,dE,d2E] = Functions_softmax(W,Y,C)
% W: All the weight functions + bias either in matrix or vector form
% Y: Data matrix appended with a column of 1's
% C: Observed probability matrix
%
% E: The objective function
% dE: gradient of obj. function in vector form
% d2E: Function handle that gets a V ( dims should be equal to vectorized
% version of W, and returns vectorized version of \nabla^2 E v )

% Minimum example
if nargin == 0
   run_min_example();
   return
end

% Get the matrix sizes and make sure they are consistent
n = size(Y,1);
d = size(Y,2);
c = size(C,2);
assert( size(C,1) == n, "%d != %d", size(C,1), n )
assert( numel(W) == d * c, "%d != %d", numel(W), d*c )
assert( size(C, 1) == n, "%d != %d", size(C, 1), n)

% Reshape W to be a matrix
W = reshape(W, d, c);

% the linear model
S = Y*W;

% Scale the linear model output
s = max(S,[],2);
S = S-s;

% The cross entropy
expS = exp(S);
sum_expS   = sum(expS,2);

% Find the objective function and normalize it
E = -C(:)'*S(:) +  sum(log(sum_expS)); 
E = E/ n;

% If number of outputs is more than one, compute the first and second
% derivative
if nargout > 1
    % First derivative
    dE  = -C + expS ./ sum_expS;
    dE  = (Y'*dE) / n;
    dE = dE(:);
    
    % Second derivative
    mat = @(v) reshape(v, d, c);
    vec = @(V) V(:);
    
    d2E1 = @(v) 1/n * (Y'*( (expS./sum_expS) .* (Y*mat(v))));
    d2E2 = @(v) -1/n *(Y'*  ( expS.* ( (1./sum_expS.^2) .* sum(expS.*(Y*mat(v)),2) ) ) );
    d2E  = @(v) vec(d2E1(v) + d2E2(v));
end

end % End of Functions_softmax()

% --------------------------------------------------------------------
%                          run_min_example
% --------------------------------------------------------------------

function run_min_example()

vec = @(x) x(:);

nex = 100;
Y = hilb(500)*255;
Y = Y(1:nex,:);
Y = [Y, ones(size(Y,1),1)];
C = ones(nex,3);
C = C./sum(C,2);
W = hilb(501);
W = W(:,1:3);

[E,dE,d2E] = func.softmax(W,Y,C);

n_attempts = 20;
lw = 13;
h = 1;
rho = zeros(n_attempts,3);
dW = randn(size(W));
fprintf('%*s %*s %*s\n', lw, "E", lw, "Taylor(1)", lw, "Taylor(2)")

for i=1:n_attempts
    E1 = func.softmax(W+h*dW,Y,C);
    
    t  = abs(E1-E);
    t1 = abs(E1-E-h*dE(:)'*dW(:));
    t2 = abs(E1-E-h*dE(:)'*dW(:) - h^2/2 * dW(:)'*vec(d2E(dW)));
    %
    rho(i,1) = t;
    rho(i,2) = t1;
    rho(i,3) = t2;
    fprintf('%*.2e %*.2e %*.2e\n', lw, t, lw, t1, lw, t2)
    
    % Half the finite-difference step
    h = h/2;
end

ratios = rho(1:end-1,:) ./ rho(2:end,:);
fprintf('%*s %*s %*s\n', lw, "E", lw, "Taylor(1)", lw, "Taylor(2)")
for i=1:(n_attempts-1)
    fprintf('%*.2f %*.2f %*.2f\n', lw, ratios(i,1), lw, ratios(i,2), lw, ratios(i,3))
end

end