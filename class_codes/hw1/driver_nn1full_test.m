% Classification of simple 2-D datasets.
%
% Objective function is 
% logistic( sigma(YK + bk)W + bw, C)

rng(0) %Random seed you will kill me at some point
clear 
close all

%% Parameters
OPTIMIZATION_METHOD = "GDMINIMIZE";
% OPTIMIZATION_METHOD = "GDOURS";         % does not work well

RATIO_HIDDEN_UNITS = 90.00 ;
REGULARIZATION_RATIO_K =  1.000000e-06;
REGULARIZATION_RATIO_BK = 1.000000e-06 ;
REGULARIZATION_RATIO_W = 1.000000e-06 ;
REGULARIZATION_RATIO_BW = 1.000000e-06 ;
MAX_OPTIMIZATION_ITERATIONS = 10000;


%ACTIVATION = "tanh";
ACTIVATION = "relu";                    % relu does not work very well.
% ACTIVATION = "srelu";  

% DATASET = "LINEAR";
DATASET = "SPIRAL";
%DATASET = "CIRCLES";

filename = sprintf('nn1full_test_history_%s_%s_%s.txt', DATASET, ACTIVATION, OPTIMIZATION_METHOD);
filename2 = sprintf('nn1full_test_history_%s_%s_%s.svg', DATASET, ACTIVATION, OPTIMIZATION_METHOD);
historyfile =  fopen(filename, 'wb');

fprintf(historyfile, "# OPTIMIZATION_METHOD = %s \n", OPTIMIZATION_METHOD);
fprintf(historyfile, "# RATIO_HIDDEN_UNITS = %.2f \n", RATIO_HIDDEN_UNITS);
fprintf(historyfile, "# REGULARIZATION_RATIO_K = %e \n", REGULARIZATION_RATIO_K);
fprintf(historyfile, "# REGULARIZATION_RATIO_BK = %e \n", REGULARIZATION_RATIO_BK);
fprintf(historyfile, "# REGULARIZATION_RATIO_W = %e \n", REGULARIZATION_RATIO_W);
fprintf(historyfile, "# REGULARIZATION_RATIO_BW = %e \n", REGULARIZATION_RATIO_BW);
fprintf(historyfile, "# ACTIVATION = %s \n", ACTIVATION);
fprintf(historyfile, "# DATASET = %s \n", DATASET);

%% Get data
switch DATASET
    case "LINEAR"
        [Y, labels] = dataset.make_linear(1000);
    case "SPIRAL"
        [Y, labels] = dataset.make_spiral();
    case "CIRCLES"
        [Y, labels] = dataset.make_circle(1000, [true, true, true]);
end

label_tools = utility.LabelTools(labels);
c = label_tools.n_labels;
Cobs = label_tools.old_labels_to_proba_matrix( labels );

%% Neural net params
params = cl_nn_single_full.default_params();
params.n_d1 = size(Y, 2);
params.n_d2 = params.n_d1 * RATIO_HIDDEN_UNITS;
params.n_c = size(Cobs, 2);
params.activation = ACTIVATION;
params.lambda_K = REGULARIZATION_RATIO_K;
params.lambda_bk = REGULARIZATION_RATIO_BK;
params.lambda_W = REGULARIZATION_RATIO_W;
params.lambda_bw = REGULARIZATION_RATIO_BW;
params.silence_k = false;

%% Initial conditions
x0 = randn( cl_nn_single_full.n_vars(params), 1) ; % sensitive


%% Optimization
switch OPTIMIZATION_METHOD
    case "GDOURS"
        optim_reg = @(x,params) cl_nn_single_full.l2_regularizer(x,params);
        optim_fun = @(x,~)     cl_nn_single_full.objective(x,Y,Cobs, params);
        params.alpha   = 1;
        params.verbosity = 1;
        params.n_gd_max_iter = MAX_OPTIMIZATION_ITERATIONS;
        % param.mu0=0.01;
        % param.fixed_mu = 1;
        params.historyfile = historyfile;
        % params.n_linesearch_max_iter=40;
        % params.mu0 = 0.01;
        x = optimization.gradient_descent(optim_fun,optim_reg,x0,params);
    case "GDMINIMIZE"
        %  Some fancier gradient descent from Carl Edward Rasmussen
        n_iters = -MAX_OPTIMIZATION_ITERATIONS;
        % x = optimization.minimize(x0, @objective_for_minimize, n_iters, historyfile, Y, Cobs, params);
        x = optimization.minimize(x0, @objective_for_minimize, n_iters, [], Y, Cobs, params);
end



% [K, bk, W, bw] = cl_nn_single_full.unwrap_vars(x, params)


cls = struct;
cls.predict = @(Yt) fn_predict(Yt, x, params);

% return
% Plot the stuff
Yploty = Y; % params.tr.tr(Y);
hold off; metric.plot2d( cls, min(Yploty, [], 1), max(Yploty, [], 1));
hold on; plot( Yploty(labels == 0,1), Yploty(labels == 0,2), 'go');
hold on ; plot( Yploty(labels == 1,1), Yploty(labels == 1,2), 'ro');

saveas(gcf(),filename2);
fclose( historyfile);

%%% Classification
function Cnew = fn_predict(Yt, x, params)
[K, bk, W, bw] = cl_nn_single_full.unwrap_vars(x, params);
switch params.activation
    case "tanh"
        Snew = tanh( Yt*K + bk ) * W + bw;
    case "relu"
        Snew = max( Yt*K + bk , 0) * W + bw;
    case "srelu"
        Snew = cl_nn_single_full.smooth_relu( Yt*K + bk ) * W + bw;
end
Snew = Snew - max(Snew,[],2);
Cnew = exp(Snew);
Cnew = Cnew ./ sum(Cnew, 2);
end

%%% Objective for minimize.m
function [f, df] = objective_for_minimize(x_, Y_, Cobs_, params_)
[R,dR] = cl_nn_single_full.l2_regularizer(x_,params_);
[E, dE] = cl_nn_single_full.objective(x_,Y_,Cobs_, params_);
f = E+ R;
df = dE+dR;
end


