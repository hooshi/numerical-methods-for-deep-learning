function output = None()
% function out = None()
    function X = fn_none(X)
        X=X;
    end

output = struct;
output.tr = @fn_none;
output.trinv = @fn_none;
end

