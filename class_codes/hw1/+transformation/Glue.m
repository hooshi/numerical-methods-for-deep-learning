function output = Glue(tr1, tr2)
% function output = Glue(tr1, tr2)

if(nargin < 2) run_min_example(); return; end

    function X1 = fn_tr(X)
        X1 = tr2.tr( tr1.tr(X) );
    end

    function X = fn_trinv(X1)
        X = tr1.trinv( tr2.trinv(X1) );
    end

output = struct;
output.tr = @fn_tr;
output.trinv = @fn_trinv;

end

function run_min_example()
Y = randn(8,3)
tr1 = transformation.Normalizer(Y, false);
tr2 = transformation.RandomLayer(Y, 3, "sigmoid");
gl = transformation.Glue(tr1, tr2);
Ytr = gl.tr(Y)
end