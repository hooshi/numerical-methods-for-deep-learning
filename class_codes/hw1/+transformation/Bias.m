function output = Bias(~)
% function output = Bias(Y)

if(nargin ~= 0) run_min_example(); return; end

    function X1 = fn_tr(X)
       X1 = [X ones(size(X,1), 1) ];
    end

    function X = fn_trinv(X1)
       X = X1(:, 1:end-1);
    end

output = struct;
output.tr = @fn_tr;
output.trinv = @fn_trinv;

end


function run_min_example()
Y = randn(8,3)
nrm = transformation.Bias();
Ytr = nrm.tr(Y)
Y2 = nrm.trinv(Ytr)
Y-Y2
end