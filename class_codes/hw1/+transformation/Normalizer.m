function output = Normalizer(Y, make_mean_zero)
% function output = Normalizer(Y, make_mean_zero)

if(nargin == 0) run_min_example(); return; end

max_per_col = max(Y, [], 1);
min_per_col = min(Y, [], 1);
ratios = max_per_col - min_per_col;
shift = [];
if (make_mean_zero)
    shift = zeros(size(max_per_col))-0.5;
else
    shift = zeros(size(max_per_col));
end     
dont_touch = ~ratios;
ratios( dont_touch ) = 1;
shift( dont_touch ) = 0;

  
    function X1 = fn_tr(X)
        X1 = (X - min_per_col) ./ ratios + shift;
    end

    function X = fn_trinv(X1)
        X = X1;
        if (make_mean_zero)
            X = X + 0.5;
        end
        X = X .* ratios + min_per_col;
    end

output = struct;
output.tr = @fn_tr;
output.trinv = @fn_trinv;

end


function run_min_example()
Y = randn(8,3)
nrm = transformation.Normalizer(Y, false);
Ytr = nrm.tr(Y)
Y2 = nrm.trinv(Ytr)
Y - Y2
end