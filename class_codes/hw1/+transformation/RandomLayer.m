function output = RandomLayer(Y, d_ratio, activation, is_normal_dist)
% output = RandomLayer(Y, d_ratio, activation)
% Activation can be:
%  "relu", "tanh", or "sigmoid"

if(nargin <= 2) run_min_example(); return; end

d = size(Y,2);
% K = speye(d, uint32(d_ratio*d)); 
K = [];
b = [];
if(is_normal_dist) 
    K = randn(d, uint32(d_ratio*d) );
    b = randn(1, uint32(d_ratio*d) ); 
else
    K = rand(d, uint32(d_ratio*d) );
    b = rand(1, uint32(d_ratio*d) );
end

    function X1 = fn_tr(X)
        switch activation
            case "identity"
                X1 = X ;
            case "relu"
                X1 = max(X*K +b, 0);
            case "sigmoid"
                X1 = (tanh(X*K+b) + 1) ./ 2;
            case "tanh"
                X1 = tanh(X*K+b);
            otherwise
                error ("activation not recognized");
        end
    end

    function X = fn_trinv(X1)
       error( "Not implemented");
    end

output = struct;
output.tr = @fn_tr;
output.trinv = @fn_trinv;

end


function run_min_example()
Y = randn(8,3)
nrm = transformation.RandomLayer(Y, 2, "sigmoid", true);
Ytr = nrm.tr(Y)
end