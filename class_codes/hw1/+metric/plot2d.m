function plot2d(classifier, minvals, maxvals)

n=128;
tx = linspace(minvals(1), maxvals(1), n);
ty = linspace(minvals(2), maxvals(2), n);
[X, Y] = meshgrid(tx, ty);
Y = [ reshape(X,[],1), reshape(Y,[],1) ];
f = classifier.predict( Y );
[~,f] = max(f, [], 2);

contourf(tx,ty, reshape(f, n, n) );

end

