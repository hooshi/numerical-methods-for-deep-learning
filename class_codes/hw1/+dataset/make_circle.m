function [Y,lb] = make_circle(n_samples, is_circle_present)
% function [Y,lb] = make_circle()

if(nargin==0) run_min_example(); return; end

center = [0.5,0.5; 0 , 0; 1, 1];
Y = rand(n_samples,2);
C1 = vecnorm( Y - center(1,:) , 2, 2);
C2 = vecnorm( Y - center(2,:) , 2, 2);
C3 = vecnorm( Y - center(3,:) , 2, 2);
r1 = 0.2;
r2 = 0.3;
r3 = 0.3;

lb = zeros(size(Y,1), 1);

if(is_circle_present(1))
        lb = lb | (C1<r1);
end
if(is_circle_present(2))
        lb = lb | (C2<r2);
end
if(is_circle_present(3))
        lb = lb | (C3<r3);
end

end

function run_min_example()
[Y,lb] = dataset.make_circle(600, [false, true, false]);
figure();
hold off, plot(Y(lb==0,1), Y(lb==0,2), 'ro');
hold on, plot(Y(lb==1,1), Y(lb==1,2), 'go');
end


