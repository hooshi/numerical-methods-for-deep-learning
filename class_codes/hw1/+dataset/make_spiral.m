function [Y,lb] = make_spiral(n_samples)
% function [Y,lb] = make_spiral(n_samples)
%  
data = importdata('+dataset/spiral.txt');
if(nargin==0) n_samples = size(data,1); end

assert( n_samples <= size(data,1) );


Y = data(1:n_samples, 1:2);
lb = uint32(data(1:n_samples, 3));

end

