function [Y,lb] = make_linear(n_samples)
% function [Y,lb] = make_linear()

a = 3; b = 2;
Y = randn(n_samples,2);
C = a*Y(:,1) + b*Y(:,2) + 1;
lb = C>0;

end

