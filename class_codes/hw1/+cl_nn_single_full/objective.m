function [E, dE] = objective(x, Y, C, params)
% [E, dE] = objective(x, Y, C, params)

if( nargin == 0 ) 
    run_min_example('relu');
    run_min_example('tanh');
    return; 
end

% Get the matrices
[K, bk, W, bw] = cl_nn_single_full.unwrap_vars(x, params);

% Get activation
switch params.activation
    case "srelu"
        sigma_f = @(l) cl_nn_single_full.smooth_relu(l);
        dsigma_f = @(l) smooth_relu_der(l);
    case "relu"
        sigma_f = @(l) max(l, 0);
        dsigma_f = @(l) l > 0;        
    case "tanh"        
        sigma_f = @(l) tanh(l);
        pow2_f = @(l) l.*l;
        dsigma_f = @(l) 1-pow2_f(tanh(l));
end

% Get Z and handles to its jacobian
[Z, ~, ~, dZdKt_f, dZdbt_f] = cl_nn_single_full.full_layer(K, bk, Y, sigma_f, dsigma_f);
    
% Get sofmax and its derivative
S = Z*W + bw;
[E, dEdS] = cl_nn_single_full.softmax(S, C);

if( nargout >= 2)
% Get derivatives for K

if( ~params.silence_k )
    dEdZ = dEdS * W';
    dEdK = dZdKt_f( dEdZ );
    dEdbk = dZdbt_f( dEdZ );
else
    dEdK = zeros( size( K ) );
    dEdbk = zeros( size( bk ) );
end

% Get the derivatives for W
dEdW = Z' * dEdS;
dEdbw = sum(dEdS, 1);

% Get the jacobian
dE = cl_nn_single_full.wrap_vars(dEdK,dEdbk, dEdW, dEdbw, params);
end

end

function dR = smooth_relu_der(Y)
[~,dR] = cl_nn_single_full.smooth_relu(Y);
end

%%%=================================================
%%%            Test function
%%%=================================================
function run_min_example(activation_name)

fprintf("================= TESTING %s ================ \n", activation_name);

% Fix random seed
rng(214);

n =  100;
d1 = 3;
d2 = 8;
c =4;

Y = randn(n, d1);
C = max( randn(n,c), 0.1);
C = C./sum(C,2);

params.n_d1 = d1;
params.n_d2 = d2;
params.n_c    = c;
params.activation = activation_name;
params.silence_k = false;
[K, bk, W, bw] = cl_nn_single_full.unwrap_vars([], params);
x = cl_nn_single_full.wrap_vars(K, bk, W, bw, params);
x = randn( size(x) );

[E0,dE] = cl_nn_single_full.objective(x, Y, C, params);

%%======================= Test the derivative
h = 1;
n_attempt = 20;
dx = randn(size(x)); 
lw = 13;

fprintf('%*s %*s %*s %*s \n', ...
    lw, "constant", lw, "ratio", lw, "linear", lw, "ratio");
    
for i=1:n_attempt
    Ee =  cl_nn_single_full.objective(x+h*dx, Y, C, params);
    El = E0 +  dE(:)'*h*dx;
    
    t  = max( max( abs(E0-Ee)) );
    t1 = max( max( abs(El-Ee)) );
    %
    if(i > 1)
        fprintf('%*.2e %*.2f %*.2e %*.2f\n', ...
            lw, t, lw, told/t,  lw, t1, lw, t1old/t1);
    end
    
    % Half the finite-difference step
    told = t;
    t1old = t1;
    h = h/2;
end

end