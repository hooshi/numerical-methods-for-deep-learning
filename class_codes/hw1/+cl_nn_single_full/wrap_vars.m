function x = wrap_vars(K, bk, W, bw, params)
% x = wrap_vars(K, bk, W, bw, params)

x = [K(:); bk(:); W(:); bw(:)];

end

