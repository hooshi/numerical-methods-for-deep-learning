function [Z, dZdK, dZdb, dZdKt, dZdbt] = full_layer(K, bk, Y, sigma, dsigma)
% function [Z, dZdK, dZdb, dZdKt, dZdbt] = full_layer(K, bk, Y, sigma, dsigma)

% test code
if(nargin==0) run_min_example(); return; end

L = Y*K + bk;
Zp = dsigma(L);
Z = sigma(L);

dZdK = @(V) Zp .* (Y * V);
dZdKt = @(U) Y' * ( Zp .* U);

dZdb = @(V) Zp .* V; % Zp .* repmat(V)
dZdbt = @(U) sum( Zp .* U, 1 );  

end

function run_min_example() 
rng(9423797);

n =  100;
d1 = 3;
d2 = 8;

sigma = @(x) tanh(x);
dsigma =  @(x) (1 - tanh(x) .* tanh(x) );
K = randn(d1,d2);
bk = randn(1,d2);
Y = randn(n,d1);

[Z0, dZdK, dZdb, dZdKt, dZdbt] = cl_nn_single_full.full_layer(K, bk, Y, sigma, dsigma);

%%======================= Test the derivative
h = 1;
n_attempt = 20;

dK = randn(size(K));  %zeros(size(K));
db = randn(size(bk)); %zeros(size(bk));
lw = 13;

fprintf('%*s %*s %*s %*s \n', ...
    lw, "constant", lw, "ratio", lw, "linear", lw, "ratio");
    
for i=1:n_attempt
    Ze =  cl_nn_single_full.full_layer(K+h*dK, bk+h*db, Y, sigma, dsigma);
    Zl = Z0 +  dZdK(h*dK) + dZdb(h*db);
    
    t  = max( max( abs(Z0-Ze)) );
    t1 = max( max( abs(Zl-Ze)) );
    %
    if(i > 1)
        fprintf('%*.2e %*.2f %*.2e %*.2f\n', ...
            lw, t, lw, told/t,  lw, t1, lw, t1old/t1);
    end
    
    % Half the finite-difference step
    told = t;
    t1old = t1;
    h = h/2;
end


%%============ Test the transpose
fprintf('%*s %*s\n', lw, "JK err", lw, "Jb err");
for i=1:n_attempt
    dK = randn(size(K)); 
    db = randn(size(bk));
    dZ = randn(size(Z0));
    
    vec = @(x) x(:);
    err1 = abs(dZ(:)' * vec( dZdK(dK) ) - dK(:)' * vec( dZdKt(dZ) ) );
    err2 = abs(dZ(:)' * vec( dZdb(db) ) - db(:)' * vec( dZdbt(dZ) ) );
    
    fprintf('%*.2e %*.2f \n', lw,  err1, lw, err2 );
end

end