function dp = default_params()
% dp = default_params()

dp.n_c = -1;                 % User must specify this
dp.n_d1 = -1;                % User must specify this
dp.n_d2 = -1;                % User must specify this
dp.activation = "relu"; %% tanh, sigmoid
dp.silence_k  = false;

dp.lambda_K = 0;
dp.lambda_bk = 0;
dp.lambda_W = 0;
dp.lambda_bw = 0;

end

