function [K, bk, W, bw] = unwrap_vars(x, params)
% [K, bk, W, bw] = unwrap_vars(x, params)

% test code
if(nargin == 0) run_min_example(); return; end

% find dimensions
n_K = [params.n_d1, params.n_d2];
n_bk = [1, params.n_d2]; % add b to every row.
n_W = [params.n_d2,  params.n_c];
n_bw = [1,  params.n_c]; % add b to every row

% initialize x as zero if [] is passed
if(isempty(x))
    x = zeros(prod(n_K)+prod(n_bk)+prod(n_W)+prod(n_bw), 1);
end

% Extract each variable
from = 1;
to = 0;

to = to + prod(n_K);
K = reshape(x(from:to), n_K(1), n_K(2) ); 
from= from + prod(n_K);

to = to + prod(n_bk);
bk = reshape(x(from:to), n_bk(1), n_bk(2) ); 
from= from + prod(n_bk);

to = to + prod(n_W);
W = reshape(x(from:to), n_W(1), n_W(2) ); 
from= from + prod(n_W);

to = to + prod(n_bw);
bw = reshape(x(from:to), n_bw(1), n_bw(2) ); 
% from= from + prod(n_bw);


end

function run_min_example()
p = cl_nn_single_full.default_params();
p.n_c = 3;
p.n_d1 =4;
p.n_d2 = 7;
[K, bk, W, bw] = cl_nn_single_full.unwrap_vars([], p);

K = randn(size(K))
bk = randn(size(bk))
W = randn(size(W))
bw = randn(size(bw))

x = cl_nn_single_full.wrap_vars(K, bk, W, bw, p);

[K_, bk_, W_, bw_] = cl_nn_single_full.unwrap_vars(x, p);

assert( norm(K-K_) == 0);
assert( norm(bk_-bk) == 0);
assert( norm(W_-W) == 0);
assert( norm(bw_-bw) == 0);
end