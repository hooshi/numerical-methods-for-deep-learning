function[E,dE, d2E] = softmax(S, C)
% [E,dE] = softmax(S, C)
% 
% Finds E(S,C) and its gradient

% Minimum example
if (nargin == 0)   run_min_example();  return; end

% Find number of samples
n = size(C, 1);
assert(all(size(C) == size(S))); % same size

% Scale the linear model output
s = max(S,[],2);
S = S-s;

% The cross entropy
expS = exp(S);
sum_expS   = sum(expS,2);

% Normalized objective function
if( nargout >=1 )
    E = -C(:)'*S(:) +  sum(log(sum_expS));
    E = E/ n;
end

% Normalized gradient
if (nargout >= 2)
    expS_div_sum_expS = expS ./ sum_expS;
    dE  = -C + expS_div_sum_expS;
    dE  = dE / n;
end


% Normalized Hessian, only the SPD term
    function HV = fn_d2E(V)
        HV1 =  expS_div_sum_expS .* V / n;
        HV2 = - ( expS.* ( (1./sum_expS.^2) .* sum(expS.*V,2) ) ) /n;
        HV = HV1 + HV2;
    end
d2E  = @fn_d2E;

end 

% --------------------------------------------------------------------
%                          run_min_example
% --------------------------------------------------------------------

function run_min_example()

% Fix random seed
rng(214);

nex = 100;
Y = hilb(500)*255;
Y = Y(1:nex,:);
Y = [Y, ones(size(Y,1),1)];
C = max( randn(nex,3), 0.1);
C = C./sum(C,2);
W = hilb(501);
W = W(:,1:3);
S = Y*W;

[E0,dE, d2E] = cl_nn_single_full.softmax(S,C);

n_attempts = 20;
lw = 13;
h = 1;
rho = zeros(n_attempts,3);
dS = randn(size(S));
fprintf('%*s %*s  %*s \n', lw, "E", lw, "Taylor(1)", lw, "Taylor(2)")

for i=1:n_attempts
    E1 = cl_nn_single_full.softmax(S+h*dS,C);

    vec = @(x) x(:);
    El = E0 + h*dE(:)'*dS(:);
    Eq = El + h^2/2*dS(:)'*vec( d2E(dS) );
    
    t  = abs(E1 - E0);
    t1 = abs(E1 - El);
    t2 = abs(E1 - Eq);
    %
    rho(i,1) = t;
    rho(i,2) = t1;
    rho(i,3) = t2;
    fprintf('%*.2e %*.2e %*.2e \n', lw, t, lw, t1, lw, t2);
    
    % Half the finite-difference step
    h = h/2;
end

ratios = rho(1:end-1,:) ./ rho(2:end,:);
fprintf('%*s %*s %*s\n', lw, "E", lw, "Taylor(1)",  lw, "Taylor(2)")
for i=1:(n_attempts-1)
    fprintf('%*.2f %*.2f %*.2f \n', lw, ratios(i,1), lw, ratios(i,2), lw, ratios(i,3))
end

end