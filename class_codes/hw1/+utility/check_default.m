function param_out = check_default(param, field, value)
% function Utility_check_default(param, field, value)

if (~isfield(param, field)) 
    param = setfield(param, field, value);
    fprintf("Setting %s to %s \n", field, ...
        matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(value));
end

param_out = param;

end

