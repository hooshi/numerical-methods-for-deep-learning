function out = LabelTools(labels)
% function out = LabelTools(labels)

if(nargin == 0) run_min_example(); return; end

% Get the labels
unique_old_labels = sort( unique( labels ) );
n_labels = numel( unique_old_labels );


% Renumber labels
    function out = new_to_old_labels(in)
        out =  unique_old_labels(in);
    end

% Renumber labels
    function out = old_to_new_labels(in)
        out = zeros( size(in) );
        for i=1:length(in)
            id = utility.binary_search( unique_old_labels, n_labels, in(i) );
            assert( id ~= -1);
            out(i) = id;
        end 
    end

% Convert labels to probability matrix
    function C = old_labels_to_proba_matrix( lb_old )
        n = length(lb_old);
        lb_new = old_to_new_labels(lb_old);
        C = full(sparse(1:n, lb_new,ones(n,1),n,n_labels));
    end

out = struct;
out.new_to_old_labels = @new_to_old_labels;
out.old_to_new_labels = @old_to_new_labels;
out.old_labels_to_proba_matrix = @old_labels_to_proba_matrix;
out.n_labels = n_labels;
end

function run_min_example()
labels = [-1 12 453245 32 99 34 66 23 1 1 23 66 34 99 -1 32]
lt = utility.LabelTools(labels)

lt.n_labels
l1 = lt.new_to_old_labels( [1 3 4 6 9] )
l2 = lt.old_to_new_labels( l1 )
C  = lt.old_labels_to_proba_matrix( l1 )

end