% Linear classification of MNIST

clear
close all
rng(0)

%% setting all the path
% setting all the path
run('utility.init_paths.m')
addpath( DIR_MNIST )

%% Upload the data and set it for training
Ytest = loadMNISTImages('mnist/t10k-images-idx3-ubyte')';
lbtest = loadMNISTLabels('mnist/t10k-labels-idx1-ubyte');

Y = loadMNISTImages('mnist/train-images-idx3-ubyte')';
lb = loadMNISTLabels('mnist/train-labels-idx1-ubyte');

%% Classifier

%%%%%% Transformation
d_ratio = 3;
activation_type = "tanh";
% tr_nrm = transformation.Normalizer(Y, false);
%
tr_rand =  transformation.RandomLayer(Y, d_ratio, activation_type, true);
% tr = transformation.Glue(tr_nrm, tr_rand);
%
% tr = tr_nrm;
%
% tr = transformation.None();

params.tr = tr_rand;

params.alpha = 1e-2;
params.regularization = "identity";


%%%%% LAPLACIAN
% params.tr = transformation.None();
% params.alpha = 1e-5;
% params.regularization = "laplacian";

%%%% OPTIMIZATION
params.optimizer_iters = 6;
params.random_guess = false;
params.optimizer='newtoncg';
params.verbosity=1;

rg = cl_linear.LogisticRegression(Y, lb, params);
% rg2 = cl_linear.LogisticRegression(Y2, lb2, params);
C = rg.Cobs;
Ctest = rg.label_tools.old_labels_to_proba_matrix(lbtest);

%% Test

fprintf('Testing    Acc %3.2g\n', metric.accuracy(C, rg.predict(Y)) );
fprintf('Validation Acc %3.2g\n', metric.accuracy(Ctest, rg.predict(Ytest)) )

%% Visualize W
W = reshape(rg.weights,[],10);

figure(1);
for i=1:9
    w = W(1:end-1,i);
    subplot(3,3,i)
    imagesc(reshape(w,28,28));
    colorbar;
end
