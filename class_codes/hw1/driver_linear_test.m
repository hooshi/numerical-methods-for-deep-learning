% Classification of MNIST using a single layer fully connected neural
% network
%
% 
% Objective function is 
% logistic( sigma(YK + bk)W + bw, C)

clear
close all
rng(0);

%% Parameters
% OPTIMIZATION_METHOD = "GDMINIMIZE";
% OPTIMIZATION_METHOD = "GDOURS";         % does not work well
OPTIMIZATION_METHOD = "GDNEWTON";  
MAX_ITERS = 10000;
FEATURE_RATIO = 300;

ALPHA      = 1e-7;

ACTIVATION = "tanh";
% ACTIVATION = "relu";

% relu does not work very well.
% DATASET = "LINEAR";
DATASET = "SPIRAL";
% DATASET = "CIRCLES";

filename = sprintf('linear_test_history_%s_%s_%s.txt', DATASET, ACTIVATION, OPTIMIZATION_METHOD);
filename2 = sprintf('linear_test_history_%s_%s_%s.svg', DATASET, ACTIVATION, OPTIMIZATION_METHOD);
historyfile = fopen(filename, 'wb');

fprintf(historyfile, "# OPTIMIZATION_METHOD = %s \n", OPTIMIZATION_METHOD);
fprintf(historyfile, "# ALPHA = %e \n", ALPHA);
fprintf(historyfile, "# ACTIVATION = %s \n", ACTIVATION);
fprintf(historyfile, "# MAX_ITERS = %d \n", MAX_ITERS);
fprintf(historyfile, "# FEATURE_RATIO = %d \n", FEATURE_RATIO);

%% Get data
switch DATASET
    case "LINEAR"
        [Y, labels] = dataset.make_linear(1000);
    case "SPIRAL"
        [Y, labels] = dataset.make_spiral();
    case "CIRCLES"
        [Y, labels] = dataset.make_circle(1000, [true, true, true]);
end

label_tools = utility.LabelTools(labels);
c = label_tools.n_labels;
Cobs = label_tools.old_labels_to_proba_matrix( labels );

tr_nrm = transformation.Normalizer(Y,true);
tr_rnd = transformation.RandomLayer(Y, FEATURE_RATIO, ACTIVATION, true);
tr = transformation.Glue( tr_nrm, tr_rnd );
 

params.verbosity = 1;
params.alpha =ALPHA;
params.tr = tr;
params.regularization = "identity";

switch OPTIMIZATION_METHOD
    case "GDNEWTON"
        params.random_guess =false;
        params.optimizer = 'newtoncg';
        params.optimizer_iters = MAX_ITERS;
        params.optimizier_diag = 1000;
    case "GDOURS"
        params.random_guess =false;
        params.optimizer = 'gd';
        params.historyfile = historyfile;
        params.optimizer_iters = MAX_ITERS;
    case "GDMINIMIZE"
        params.random_guess =false;
        params.optimizer = 'gd_minimize';
        params.optimizer_iters = MAX_ITERS;
end

params
cls = cl_linear.LogisticRegression(Y, labels, params);


% return
% Plot the stuff
Yploty = Y; % params.tr.tr(Y);
hold off; metric.plot2d( cls, min(Yploty, [], 1), max(Yploty, [], 1));
hold on; plot( Yploty(labels == 0,1), Yploty(labels == 0,2), 'go');
hold on ; plot( Yploty(labels == 1,1), Yploty(labels == 1,2), 'ro');
saveas(gcf(),filename2);

